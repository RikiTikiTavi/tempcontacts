package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arrayadapters.DialogAdapter;
import com.datamodel.DialogItem;
import com.tempcontacts.R;


/* dialog for group editing*/
public class GroupDialog extends ListActivity{
	private ListView edits_list;

	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.basic_listview);
        ArrayList<DialogItem> arr = new ArrayList<DialogItem>();
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_edit), R.drawable.edit));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_group_clean), R.drawable.clean));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_group_delete), R.drawable.remove));
        DialogAdapter adapter =  new DialogAdapter(this, arr);
        setListAdapter(adapter);
        edits_list = getListView();
        edits_list.setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
						if (arg2 == 0)
						{
							setResult(1);
						}
						else if (arg2 == 1)
						{
							setResult(2);
						}
						else if (arg2 == 2)
						{
							setResult(3);
						}
						finish();
			}});
	    
    }
}
