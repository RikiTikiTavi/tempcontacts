package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.arrayadapters.FilterDialogAdapter;
import com.datamodel.FilterDialogItem;
import com.datamodel.GroupItem;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
/* dialog in contacts fragment for filtering by group*/
public class FilterContactDialog extends ListActivity{
	
private AdapterView<ListAdapter> filter_list;
FrequentlyUsedMethods faq;
ArrayList<GroupItem> groups;
private TextView dialog_header;
private Button btnDialogOk;
private Button btnDialogCancel;
@Override
protected void onCreate(Bundle savedInstanceState){
    super.onCreate(savedInstanceState);
    faq  = new FrequentlyUsedMethods(this);
    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

    //Remove notification bar
    setContentView(R.layout.fiter_dialog_listview);
    dialog_header =(TextView)findViewById(R.id.dialog_header);
    dialog_header.setText(this.getResources().getString(R.string.dialog_contact_filter_name));
    
    btnDialogOk = (Button)findViewById(R.id.btnDialogOk);
    btnDialogCancel = (Button)findViewById(R.id.btnDialogCancel);
    
    btnDialogCancel.setText(this.getResources().getString(R.string.dialog_cancel));
    btnDialogOk.setText(this.getResources().getString(R.string.dialog_ok));
    btnDialogOk.setOnClickListener(new Button.OnClickListener() {  
        public void onClick(View v)
            {
        	
        		finish();
        		
            }
         });
    btnDialogCancel.setOnClickListener(new Button.OnClickListener() {  
        public void onClick(View v)
            {
	        	setResult(0);	
	        	finish();
            }
         });
    groups = faq.getDisplayGroups(this);
		    ArrayList<FilterDialogItem> arr = new ArrayList<FilterDialogItem>();
		    
		    if (!groups.isEmpty())
		    {
		    	for (GroupItem group: groups)
		    	{
		    		arr.add(new FilterDialogItem(group.getName(), group.getIcon(), false));
		    	}
		    }
			FilterDialogAdapter adapter =  new FilterDialogAdapter(this, arr);
		     setListAdapter(adapter);
		     filter_list = getListView();
		     ((AbsListView) filter_list).setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		     filter_list.setOnItemClickListener(new OnItemClickListener()
				  {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
							long arg3) {
								setResult(groups.get(arg2).getIcon());
//								dialog_selector = (RadioButton)arg1.findViewById(R.id.dialog_radioBtn);
//								dialog_selector.setChecked(true);
					}});
	}
}
