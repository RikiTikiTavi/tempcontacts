package com.customdialogs;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.NumberPicker;
import android.widget.NumberPicker.Formatter;
import android.widget.TextView;

import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.GroupEditing;
import com.tempcontacts.R;
/* dialog for setting contact saving terms */
public class TermsPickerDialog extends Activity {
	private TextView lastWeek;
	private TextView currWeek;
	private TextView nextWeek;
	private Button week_minus;
	private Button week_plus;
	int counter =0 ;
	private Button week_btn_cancel;
	private Button week_btn_save;
	FrequentlyUsedMethods faq;
	NumberPicker np;
	PickerFormatter formatter;
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
	    this.setContentView(R.layout.number_picker);
	    np = (NumberPicker) findViewById(R.id.np);
	    np.setMaxValue(100);
	    np.setWrapSelectorWheel(true);
	    android.view.WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = LayoutParams.MATCH_PARENT;
        params.horizontalMargin = 40;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
	    faq = new FrequentlyUsedMethods(this);

	    week_minus = (Button)findViewById(R.id.week_minus);
	    week_plus= (Button)findViewById(R.id.week_plus);
//	    
	    np = (NumberPicker) findViewById(R.id.np);
	    week_btn_cancel = (Button)findViewById(R.id.week_btn_cancel);
	    week_btn_save = (Button)findViewById(R.id.week_btn_save);

	    np.setMinValue(0);
	    np.setMaxValue(6);

	    String [] values  = this.getResources().getStringArray(R.array.termspicker_terms);
	    np.setDisplayedValues(values);

	    week_minus.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
					--counter;
					np.setValue(counter);
				}
	    });
	    
	    week_plus.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				++counter;
				np.setValue(counter);
				}
	    });
	    week_btn_cancel.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				finish();
			}
	    });
	    week_btn_save.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
					
				if(getIntent().getExtras()!=null)
				{
					
					if ((ContactItem)getIntent().getExtras().getParcelable("new_contact")!=null)
					{
							ContactItem item = (ContactItem)getIntent().getExtras().getParcelable("new_contact");
							GroupItem group_item = (GroupItem)getIntent().getExtras().getParcelable("new_contact_group");
							
							item.setDays(getDaysVal());
							item.setContactGroup(group_item);
							if (group_item!=null)
							{
								group_item.setContact(item);
								group_item.setCount(group_item.getCount());
								faq.updateGroupCount(group_item);
							}
							try {
								Log.i("contact before saving" ,  item.toString());
							faq.saveContact(item);
							
							}
							catch(Exception e)
							{
								 Log.e("MYAPP", "exception", e);
								e.printStackTrace();
							}
							setResult(1);
							finish();
					}
					else if ((ContactItem)getIntent().getExtras().getParcelable("contact_editing")!=null)
					{
						ContactItem item = (ContactItem)getIntent().getExtras().getParcelable("contact_editing");
						item.setDays(getDaysVal());
						Intent a =new Intent();
						a.putExtra("contact_editing_time_change", item);
						
						setResult(2,a);
						
						finish();
					}
				
				}
				else 
				{
					Log.i("termsPicker", "getIntent is null");
					Intent a =new Intent(TermsPickerDialog.this, GroupEditing.class);
					a.putExtra("group_days", getDaysVal());
					setResult(1,a);
					
					finish();
				}
			
			}
	    });
	    
	}
	private int getDaysVal()
	{
		
		int days = 0;
		if (np.getValue()==0)
			days = 7;
		else if(np.getValue()==1)
			days =14;
		else if(np.getValue()==2)
			days =30;
		else if(np.getValue()==3)
			days =91;
		else if(np.getValue()==4)
			days =183;
		else if(np.getValue()==5)
			days =365;
		else if(np.getValue()==6)
			days =1000;
		
		return days;
		
	}
	public class PickerFormatter implements Formatter 
	{

		private String one;
		private String two;
		private String three;
		
		private String value;

		public PickerFormatter(String single, String multiple,String multiple2) {
			one = single;
			two = multiple;
			three = multiple2;
		}

		@Override
		public String format(int num) {
			if (num == 1) {
				value= num + " " + one;
		    }
		    if (num < 5) {
		    	value=  num + " " + two;
		    }
		    else
		    	value = num + " " + three;
		    return value;
		}
		
		public String getFormatValue()
		{
			return this.value;
		}
	}
	
	private void checkCountNull()
	{
		if (counter ==0)
	    {
			week_minus.setEnabled(false);
	    }
		else 
		   	week_minus.setEnabled(true);
	}
	@Override
	protected void onResume() {
	    super.onResume();
	    np.setFormatter(formatter);
	}
	
}
