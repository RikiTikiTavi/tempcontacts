package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.arrayadapters.DialogAdapter;
import com.datamodel.DialogItem;
import com.tempcontacts.R;
/* dialog for calling or sending sms to contact in reminder */
public class EventDialog extends ListActivity{
	private ListView edits_list;
	String number;
	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.basic_listview);
        
        ArrayList<DialogItem> arr = new ArrayList<DialogItem>();
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_call), R.drawable.call));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_sms), R.drawable.sms));
		 
        DialogAdapter adapter =  new DialogAdapter(this, arr);
        setListAdapter(adapter);
        
        if (getIntent().getExtras().getString("event_number")!=null)
        {
        	number = getIntent().getExtras().getString("event_number");
        }
        
        edits_list = getListView();
        edits_list.setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				
				if (arg2 == 0)
				 {
			    	 // calling
					 Intent callIntent = new Intent(Intent.ACTION_CALL);
				        callIntent.setData(Uri.parse("tel:"+ number));
				        startActivity(callIntent);
				 }
				 else if(arg2 == 1)
				 {
					 
					 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
		                        + number)));
				 }
						finish();
			}});
	    
    }
}