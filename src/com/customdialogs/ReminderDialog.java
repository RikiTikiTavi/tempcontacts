package com.customdialogs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.arrayadapters.GroupContactAdapter;
import com.datamodel.ContactItem;
import com.datamodel.ReminderItem;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
import com.tempcontacts.ReminderActivity;
/* dialog for saved reminder editing*/
public class ReminderDialog extends Activity{
	private ListView edits_list;
ContactItem cont_info;
ContactItem contact;
GroupContactAdapter groupContactAdapter;
FrequentlyUsedMethods faq;
ReminderItem reminder;
private TextView reminder_note;
private ImageButton reminder_dialog_edit;
private ImageButton reminder_dialog_remove;
private ImageButton reminder_dialog_close;
private TextView reminder_dialog_number;
	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        android.view.WindowManager.LayoutParams params = getWindow().getAttributes();
        //Remove notification bar
        setContentView(R.layout.reminder_dialog);
        getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        faq = new FrequentlyUsedMethods(this);
        
        reminder_note = (TextView)findViewById(R.id.reminder_note);
        reminder_dialog_number = (TextView)findViewById(R.id.reminder_dialog_number);
        reminder_dialog_edit = (ImageButton)findViewById(R.id.reminder_dialog_edit);
        reminder_dialog_remove = (ImageButton)findViewById(R.id.reminder_dialog_remove);
        reminder_dialog_close = (ImageButton)findViewById(R.id.reminder_dialog_close);
        
        
        if (getIntent().getExtras()!=null)
        {
        	reminder= getIntent().getExtras().getParcelable("reminder_edit");
        	if (reminder.getInfo()!=null )
        		reminder_note.setText(reminder.getInfo());
        	reminder_dialog_number.setText(reminder.getNumber());
        	
        }
        
        reminder_dialog_number.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				
				Intent a=  new Intent(ReminderDialog.this, EventDialog.class);
				a.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				a.putExtra("event_number", reminder_dialog_number.getText().toString());
				startActivity(a);
//					finish();
				}
	    });
        
        reminder_dialog_close.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
					finish();
				}
	    });
        
        reminder_dialog_edit.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
					Intent a = new Intent(ReminderDialog.this, ReminderActivity.class);
					a.putExtra("reminder_update", reminder);
					startActivity(a);
				}
	    });
        
        reminder_dialog_remove.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				Log.i(" ocnlick to remove reminder", "i'm going to kill reminder");
				faq.deleteReminder(reminder);
				Intent back_reminder = new Intent();
				setResult(1);
				finish();
				}
	    });
	 }
}
