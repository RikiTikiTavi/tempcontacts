package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arrayadapters.DialogAdapter;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.datamodel.DialogItem;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
/* dialog for archive item in ArchiveActivity */
public class ArchiveDialog extends ListActivity{
	private ListView edits_list;
	FrequentlyUsedMethods faq;
	 DialogAdapter adapter;
	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.basic_listview);
        faq = new FrequentlyUsedMethods(this);
        ArrayList<DialogItem> arr = new ArrayList<DialogItem>();
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_call), R.drawable.call));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_sms), R.drawable.sms));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_calendar), R.drawable.calendar)); 
		 adapter  =  new DialogAdapter(this, arr);
        setListAdapter(adapter);
        edits_list = getListView();
        edits_list.setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
					setResult (adapter.getItem(arg2).getIcon());
						finish();
			}});
	    
    }
}
