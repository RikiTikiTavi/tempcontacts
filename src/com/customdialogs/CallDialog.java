package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arrayadapters.DialogAdapter;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.datamodel.DialogItem;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
/* dialog for call item in CallFragment */
public class CallDialog extends ListActivity{
	private ListView edits_list;
	CallItem call;
	FrequentlyUsedMethods faq;
	 DialogAdapter adapter;
	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.basic_listview);
        faq = new FrequentlyUsedMethods(this);
        if (getIntent().getExtras()!=null)
        {
        	call = getIntent().getExtras().getParcelable("call_dialog");
        }
        ArrayList<DialogItem> arr = new ArrayList<DialogItem>();
        DialogItem autosave  = new DialogItem(this.getResources().getString(R.string.dialog_autosave), R.drawable.quickly_save);
        arr.add(autosave);
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_call), R.drawable.call));
		 DialogItem save  = new DialogItem(this.getResources().getString(R.string.dialog_save), R.drawable.save);
		 arr.add(save);
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_calendar), R.drawable.calendar)); 
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_bufer), R.drawable.copy));
		 for (ContactItem a : faq.getDisplayContacts(this))
		 {
			 if (call.getNumber().equalsIgnoreCase(a.getNumber()))
				 {
				 	arr.add(new DialogItem(this.getResources().getString(R.string.dialog_archive), R.drawable.archive));
				 	arr.add(new DialogItem(this.getResources().getString(R.string.dialog_sms), R.drawable.sms));
				 	arr.remove(autosave);
				 	arr.remove(save);
				 }
		 }
		 adapter  =  new DialogAdapter(this, arr);
        setListAdapter(adapter);
        edits_list = getListView();
        edits_list.setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
					setResult (adapter.getItem(arg2).getIcon());
						finish();
			}});
	    
    }
}
