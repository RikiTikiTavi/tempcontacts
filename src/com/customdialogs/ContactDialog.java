package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arrayadapters.DialogAdapter;
import com.datamodel.DialogItem;
import com.tempcontacts.R;
/* dialog for contact item in ContactFragment */
public class ContactDialog extends ListActivity{
	private ListView edits_list;

	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.basic_listview);
        ArrayList<DialogItem> arr = new ArrayList<DialogItem>();
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_call), R.drawable.call));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_sms), R.drawable.sms));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_info), R.drawable.info));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_calendar), R.drawable.calendar));
		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_edit), R.drawable.edit));
		 if (getIntent().getExtras()!=null)
		 {
			 if (getIntent().getExtras().getBoolean("group_contact_choose"))
				 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_delete_contact_from_group), R.drawable.remove));
		 }
         else
             arr.add(new DialogItem(this.getResources().getString(R.string.dialog_archive), R.drawable.archive));
        final DialogAdapter adapter =  new DialogAdapter(this, arr);
        setListAdapter(adapter);
        edits_list = getListView();
        edits_list.setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
                setResult(adapter.getItem(arg2).getIcon());
						finish();
			}});
	    
    }
}