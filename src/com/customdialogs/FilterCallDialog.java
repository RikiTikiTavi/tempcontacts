package com.customdialogs;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.arrayadapters.FilterDialogAdapter;
import com.datamodel.FilterDialogItem;
import com.tempcontacts.R;
/* dialog in contacts fragment for filtering by incoming, outgoing, missed calls*/
public class FilterCallDialog extends ListActivity implements View.OnClickListener{
	
private ListView filter_list;
private TextView dialog_header;
private Button btnDialogOk;
private Button btnDialogCancel;
private RadioButton dialog_selector;

@Override
protected void onCreate(Bundle savedInstanceState)
	{ 
	    super.onCreate(savedInstanceState);
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	
	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.fiter_dialog_listview);
	    dialog_header =(TextView)findViewById(R.id.dialog_header);
	    btnDialogOk = (Button)findViewById(R.id.btnDialogOk);
	    btnDialogCancel = (Button)findViewById(R.id.btnDialogCancel);
	    
	    btnDialogCancel.setText(this.getResources().getString(R.string.dialog_cancel));
	    btnDialogOk.setText(this.getResources().getString(R.string.dialog_ok));
	    
	    
	    btnDialogOk.setOnClickListener(new Button.OnClickListener() {  
	        public void onClick(View v)
	            {
	        		finish();
	            }
	         });
	    btnDialogCancel.setOnClickListener(new Button.OnClickListener() {  
	        public void onClick(View v)
	            {
		        	setResult(0);	
		        	finish();
	            }
	         });
	   
	    
	    dialog_header.setText(this.getResources().getString(R.string.dialog_call_filter_name));
			     ArrayList<FilterDialogItem> arr = new ArrayList<FilterDialogItem>();
				 arr.add(new FilterDialogItem(this.getResources().getString(R.string.dialog_incoming), 0, false));
				 arr.add(new FilterDialogItem(this.getResources().getString(R.string.dialog_outcoming), 0, false));
				 arr.add(new FilterDialogItem(this.getResources().getString(R.string.dialog_missed), 0, false));
				 arr.add(new FilterDialogItem(this.getResources().getString(R.string.dialog_all_calls), 0, false));
				 FilterDialogAdapter adapter =  new FilterDialogAdapter(this, arr);
			     setListAdapter(adapter);
			     filter_list = getListView();
			     filter_list.setOnItemClickListener(new OnItemClickListener()
					  {
						@Override
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
								long arg3) {
									if (arg2 == 0)
									{
										setResult(1);
									}
									else if (arg2 == 1)
									{
										setResult(2);
									}
									else if (arg2 == 2)
									{
										setResult(3);
									}
									else if (arg2 == 3)
									{
										setResult(4);
									}
									
						}});
			     
			     
	}

		@Override
		public void onClick(View v) {
			Toast.makeText(FilterCallDialog.this, "Accept", Toast.LENGTH_LONG).show();
		}
}
