package com.customdialogs;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arrayadapters.DialogAdapter;
import com.arrayadapters.GroupContactAdapter;
import com.datamodel.ContactItem;
import com.datamodel.DialogItem;
import com.datamodel.GroupItem;
import com.fragments.ContactsArchiveFragment;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.library.DatabaseHandler;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.CalendarViewActivity;
import com.tempcontacts.ContactSaveActivity;
import com.tempcontacts.R;
/* dialog for contact information view*/
public class InformationDialog extends ListActivity{
	private ListView edits_list;
ContactItem cont_info;
ContactItem contact;
GroupContactAdapter groupContactAdapter;
FrequentlyUsedMethods faq;
	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.basic_listview);
        faq = new FrequentlyUsedMethods(this);
      
       if (getIntent().getExtras()!=null)
       {
    	   if ( getIntent().getExtras().getParcelable("contact_info")!=null)
    	   {	
    		   cont_info = getIntent().getExtras().getParcelable("contact_info");
    	   		Log.i("received contact for information", cont_info.toString());
    	   	 ArrayList<DialogItem> arr = new ArrayList<DialogItem>();
    		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_info_name)+cont_info.getName(),0));
    		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_info_number)+cont_info.getNumber(),0));
    		 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_info_days_save)+Integer.toString(cont_info.getDays()),0));
    		 
    		 if (cont_info.getContactGroup()!=null)
    			 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_info_group)+cont_info.getContactGroup().getName(),0));
    		 if (cont_info.getContact_info()!=null)
    			 arr.add(new DialogItem(this.getResources().getString(R.string.dialog_info_additional)+cont_info.getContact_info(),0));
            DialogAdapter adapter =  new DialogAdapter(this, arr);
            setListAdapter(adapter);
            getListView().setOnItemClickListener(new OnItemClickListener()
    		  {
    			@Override
    			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
    					long arg3) {
    						finish();
    			}});
            
    	   }
    	   else if (getIntent().getExtras().getParcelable("group_contact_list")!=null)
    	   {
    		   GroupItem a = getIntent().getExtras().getParcelable("group_contact_list");
    		   DatabaseHandler db = new DatabaseHandler(this);
   			Dao<GroupItem,Integer> daoGroup = null;
   			Dao<ContactItem,Integer> daoContact = null;
   			GroupItem newGroup = null;
   			 try {
   				 daoGroup = db.getGroupDao();
   				 UpdateBuilder<GroupItem, Integer> updateBuilder = daoGroup.updateBuilder();
   				 newGroup = daoGroup.queryForId(a.getId());
   				 ForeignCollection<ContactItem> m =  newGroup.getContactCollection() ;
   				 if (m.size()!=0)
   				 {
   					 ArrayList<ContactItem> b =new ArrayList<ContactItem>(m);
   					groupContactAdapter  =  new GroupContactAdapter(this, b);
	   	            setListAdapter(groupContactAdapter);
	   	            getListView().setOnItemClickListener(new OnItemClickListener()
	    		  {
	    			

					@Override
	    			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
	    					long arg3) {
	    				contact = groupContactAdapter.getItem(arg2);
	    				 Intent i = new Intent(InformationDialog.this, ContactDialog.class);
	    				 i.putExtra("group_contact_choose", true);
	 					startActivityForResult(i,1);
	    			}});
   	            }
   				 
   			 }
   			 catch(Exception e)
   			 {
   				 Log.i(" saveGroupContacts database problem","fuck it is cause problem");
   				 e.printStackTrace();
   			 }
    	   }
    	  
       }
       
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		Log.i("groups frag", Integer.toString(resultCode));
		 if (requestCode==1)
		 {
		     if (resultCode == R.drawable.call)
			 {
//				 Toast.makeText(getActivity(), contact.toString(), Toast.LENGTH_SHORT).show();
//				 contact.setDays(0);
//				 adapter.notifyDataSetChanged();
		    	 
//		    	 Toast.makeText(getActivity(), "I'm going to call", Toast.LENGTH_SHORT).show();
		    	 
		    	 // calling
				 Intent callIntent = new Intent(Intent.ACTION_CALL);
			        callIntent.setData(Uri.parse("tel:"+ contact.getNumber()));
			        startActivity(callIntent);
			 }
			 else if(resultCode==R.drawable.sms)
			 {
				 
				 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
	                        + contact.getNumber())));
			 }
			 else if(resultCode == R.drawable.info)
			 {

           		Intent a = new Intent(InformationDialog.this, InformationDialog.class);
           		a.putExtra("contact_info", contact);
           		startActivity(a);
           		
			 }
			 else if(resultCode == R.drawable.archive )
			 {
				 FragmentManager fragmentManager =  this.getFragmentManager();
		 	        FragmentTransaction fragmentTransaction = fragmentManager
		 	                .beginTransaction();
		 	        Fragment newFragment = new ContactsArchiveFragment();
		 	        fragmentTransaction.replace(android.R.id.content, newFragment, "Tag B");
		 	        fragmentTransaction.commit();
			 }
			 else if(resultCode == R.drawable.calendar )
			 {
				 Intent i =  new Intent(InformationDialog.this, CalendarViewActivity.class);
				startActivity(i);
			 }
			 else if(resultCode ==  R.drawable.edit  )
			 {
				 Intent i =  new Intent(InformationDialog.this, ContactSaveActivity.class);
				 i.putExtra("contact_edit", contact);
				startActivity(i);
			 } 
			 else if(resultCode == R.drawable.remove )
			 {
				 
				 faq.removeContactFromGroup(contact.getContactGroup(), contact);
				 groupContactAdapter.remove(contact);
				 if (groupContactAdapter.isEmpty())
					 finish();

			 }
		 }
	 }
}
