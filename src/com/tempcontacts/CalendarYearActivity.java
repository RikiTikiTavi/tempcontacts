package com.tempcontacts;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.library.FrequentlyUsedMethods;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ParseException;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
/*  Activity for choosing year for calendar*/
public class CalendarYearActivity extends Activity implements OnClickListener{
	
	
	private ImageView prevMonth;
	private TextView currentMonth;
	private View nextMonth;
	private GridView calendarView;
	private GridCellAdapter adapter;
	String year;
	int currYear =0;
	private final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
	
	private Date parsedDate;
	private Date newDate;
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			//CalendarView cal = new CalendarView(this);
			
			 getActionBar().setDisplayHomeAsUpEnabled(true);
		        getActionBar().setDisplayShowHomeEnabled(false);
		        getActionBar().setDisplayShowTitleEnabled(true);
		        getActionBar().setTitle(R.string.calendar);
		        getActionBar().setDisplayUseLogoEnabled(false);
			setContentView(R.layout.calendar_hour_view);
			
			prevMonth = (ImageView) this.findViewById(R.id.prevMonth);
			prevMonth.setOnClickListener(this);

			currentMonth = (TextView) this.findViewById(R.id.currentMonth);
			
			if (getIntent().getExtras()!=null)
			{
				year = getIntent().getExtras().getString("curr_year");
				try {
					parsedDate = dateTimeFormatter.parse(year);
				}
				catch(Exception e)
				{e.printStackTrace();}
				currYear = FrequentlyUsedMethods.DateToCalendar(parsedDate).get(Calendar.YEAR);
				currentMonth.setText(Integer.toString(currYear));
				
			}
			nextMonth = (ImageView) this.findViewById(R.id.nextMonth);
			nextMonth.setOnClickListener(this);

			calendarView = (GridView) this.findViewById(R.id.calendar);
			calendarView.setNumColumns(3);

			// Initialised
			adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, parsedDate.getYear());
			adapter.notifyDataSetChanged();
			calendarView.setAdapter(adapter);
		}
	
	
	public class GridCellAdapter extends BaseAdapter implements OnClickListener
	{
		private static final String tag = "GridCellAdapter";
		private final Context _context;

		private final List<String> list;
		private final String[] months = getResources().getStringArray(R.array.calendar_months);
		
		private int currentDayOfMonth;
		private int currentWeekDay;
		private Button gridcell;
		private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");

		// Days in Current Month
		public GridCellAdapter(Context context, int textViewResourceId, int year)
			{
				super();
				this._context = context;
				this.list = new ArrayList<String>();
				Calendar calendar = Calendar.getInstance();
				setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
				setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
			}
		public String getItem(int position)
			{
				return  Arrays.asList(months).get(position);
			}

		@Override
		public int getCount()
			{
				return  Arrays.asList(months).size();
			}

		@Override
		public long getItemId(int position)
			{
				return position;
			}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
			{
				View row = convertView;
				if (row == null)
					{
						LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
					}

				// Get a reference to the Day gridcell
				gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
				gridcell.setOnClickListener(this);


				String year = Arrays.asList(months).get(position);

				gridcell.setText(year);
				gridcell.setTag(position);
				
				DisplayMetrics metrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(metrics);
				int width = metrics.widthPixels;
				int height = metrics.heightPixels;
				gridcell.setMinimumHeight(height/5);
				gridcell.setMinimumWidth(width/3);

				return row;
			}
		@Override
		public void onClick(View view)
			{
				String date_month_year = (view.getTag().toString());

				try
					{
					Calendar cal = Calendar.getInstance();
					cal.clear();
					Log.i("year date", date_month_year);
					cal.set(Calendar.MONTH, Integer.parseInt(date_month_year));
					cal.set(Calendar.YEAR, currYear);
					cal.set(Calendar.DAY_OF_MONTH, FrequentlyUsedMethods.DateToCalendar(parsedDate).get(Calendar.DAY_OF_MONTH));
					newDate = cal.getTime();
					
					Intent a = new Intent();
					a.putExtra("choosed_year", newDate.toString());
					Toast.makeText(getApplicationContext(), newDate.toString(), Toast.LENGTH_SHORT).show();
					setResult(1, a);
					finish();
					
					}
				catch (ParseException e)
					{
						e.printStackTrace();
					}
			}

		public int getCurrentDayOfMonth()
			{
				return currentDayOfMonth;
			}

		private void setCurrentDayOfMonth(int currentDayOfMonth)
			{
				this.currentDayOfMonth = currentDayOfMonth;
			}
		public void setCurrentWeekDay(int currentWeekDay)
			{
				this.currentWeekDay = currentWeekDay;
			}
		public int getCurrentWeekDay()
			{
				return currentWeekDay;
			}
	}


	@Override
	public void onClick(View v) {
		if (v == prevMonth)
		{
			currYear--;
			setGridCellAdapterToDate(currYear);
		}
	if (v == nextMonth)
		{
			currYear++;
			setGridCellAdapterToDate(currYear);
		}
		
		
	}
	@TargetApi(Build.VERSION_CODES.CUPCAKE)
	private void setGridCellAdapterToDate(int year)
		{
			currentMonth.setText(Integer.toString(year));
		}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) {
        case android.R.id.home:
        	finish();
                    return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
