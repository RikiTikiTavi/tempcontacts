package com.tempcontacts;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.asynctaskbase.ITaskLoaderListener;
import com.asynctasks.CallsInitialize;
import com.asynctasks.ContactsInitialize;
import com.asynctasks.DownloadMoreCalls;
import com.crashlytics.android.Crashlytics;
import com.datamodel.CallItem;
import com.fragments.CallsFragment;
import com.fragments.ContactsArchiveFragment;
import com.fragments.ContactsFragment;
import com.fragments.GroupsFragment;
import com.library.FrequentlyUsedMethods;
import com.library.IFragmentsListener;
import com.testflightapp.lib.TestFlight;

import java.util.ArrayList;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
/*  main Activity for tabs setting and operations across fragments handling*/
public class CallsActivity extends FragmentActivity implements ContactsFragment.TaskCallbacks, IFragmentsListener , ITaskLoaderListener

 {
	static Fragment currFragment ;
	FrequentlyUsedMethods faq;
	 private boolean isVisible = false;
	 Handler handler  = new Handler();
     private boolean isDownloaded = false;

     @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        Crashlytics.start(this);
		final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.tab_background)));
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        Tab tabA = actionBar.newTab();
        tabA.setTabListener(new TabListener<CallsFragment>(this, "Tag A", CallsFragment.class));
        tabA.setIcon(R.drawable.icon_call);
        actionBar.addTab(tabA);
        
        Tab tabB = actionBar.newTab();
        tabB.setIcon(R.drawable.icon_phone_book);
        tabB.setTabListener(new TabListener<ContactsFragment>(this, "Tag B", ContactsFragment.class));
        actionBar.addTab(tabB);
        
        Tab tabC = actionBar.newTab();
        tabC.setIcon(R.drawable.icon_group);
        tabC.setTabListener(new TabListener<GroupsFragment>(this, "Tag C", GroupsFragment.class));
        actionBar.addTab(tabC);
        
        if (savedInstanceState != null) {
            int savedIndex = savedInstanceState.getInt("SAVED_INDEX");
            getActionBar().setSelectedNavigationItem(savedIndex);
        }
        actionBar.setIcon(null);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        faq = new FrequentlyUsedMethods(this);
        CallsInitialize.execute(this, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       faq.clearCallLog();
    }

    public static class TabListener<T extends Fragment>
    implements ActionBar.TabListener{
		    
				       private final Activity myActivity;
				       private final String myTag;
				       private final Class<T> myClass;
				       private static String currTag; 
					private Fragment mFragment;
				
				       public TabListener(Activity activity, String tag, Class<T> cls) {
				           myActivity = activity;
				           myTag = tag;
				           myClass = cls;
				           currTag  = tag;
				       }
				
					@Override
					public void onTabReselected(Tab tab, FragmentTransaction ft) {
					}
				
					@Override
					public void onTabSelected(Tab tab, FragmentTransaction ft) {
						
						 Fragment preInitializedFragment = (Fragment) myActivity.getFragmentManager().findFragmentByTag(myTag);
						 
						
	                        if (preInitializedFragment == null) {
		                            mFragment = (Fragment) Fragment.instantiate(myActivity, myClass.getName());
		                            ft.add(android.R.id.content, mFragment, myTag);
	                        } else {
	                            ft.attach(preInitializedFragment);
	                        }
	                        
					}
				
					@Override
					public void onTabUnselected(Tab tab, FragmentTransaction ft) {
						
						  Fragment preInitializedFragment = myActivity.getFragmentManager().findFragmentByTag(myTag);
						  if (preInitializedFragment != null) {
							  if (preInitializedFragment instanceof ContactsArchiveFragment)
							  {
								  ft.detach(mFragment);
							  }
							  else 
								  ft.detach(preInitializedFragment);
					        } else if (mFragment != null) {
					            ft.detach(mFragment);
					        }
					}
					public static String getCurrTag()
					{
						return currTag;
					}
		    }
    
    @Override
    public void onBackPressed() {
    	Log.i("callsAct onBack", "OnBack btn");
    		if (getActionBar().getSelectedTab().getPosition()==1)
    		{
    			Log.i("callsAct onBack", "position==1");
	    		if ( getFragmentManager().findFragmentByTag("Tag B") instanceof ContactsArchiveFragment)
	    	  	{
	    			Log.i("contactsArchive back btn", "its contacts Archive");
		    	 FragmentManager fragmentManager = getFragmentManager();
		    	 fragmentManager.popBackStack();
	    	  	}
	    		else 
	    			finish();
    		}
    		else
    		{
    			finish();
    		}
    }
    
    
    
    
    @Override
  		public void onResume() {
  			super.onResume();	
  			isVisible = true;
            Fragment currentFragment = getFragmentManager().findFragmentById(android.R.id.content);
            if (currentFragment instanceof CallsFragment)
            {
                if (!CallsFragment.filtering)
                 ((CallsFragment) currentFragment).updateCallList();
            }
  		}
    
    @Override
	public void onPause() {
    	super.onPause();
    	isVisible = false;
    }
    
    
    @Override
		public void onStop() {
			super.onStop();	
			
		}
	@Override
	public void onPreExecute() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onCancelled() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onPostExecute() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void fragmentsEventsListener(int flag) {
		if (flag==1)
		{
			Fragment currentFragment = getFragmentManager().findFragmentById(android.R.id.content);
			if (currentFragment instanceof ContactsFragment)
			{
				((ContactsFragment)currentFragment).updateContactList();
			}
		}
		// contacts import
		if (flag==5)
		{
			ContactsInitialize.execute(this, this);
		}
        // get more calls in call log
        if (flag==7)
        {
           
            ArrayList<CallItem> toDownload = new ArrayList<CallItem>();
            Toast.makeText(this, this.getResources().getString(R.string.download_toast), Toast.LENGTH_SHORT).show();
            toDownload = faq.downloadMoreCalls();
            if(!toDownload.isEmpty())
            {
                Fragment currentFragment = getFragmentManager().findFragmentById(android.R.id.content);
                ((CallsFragment)currentFragment).updateCallListDownload();
            }
        }

	}
	@Override
	public void onLoadFinished(Object data) {
		if (data instanceof String)
		{
			Fragment currentFragment = getFragmentManager().findFragmentById(android.R.id.content);
			if (((String)data).equalsIgnoreCase("contacts_initialized"))
			{
				if (currentFragment instanceof ContactsFragment)
				{
					((ContactsFragment)currentFragment).updateContactList();
				}
			}
			else if (((String)data).equalsIgnoreCase("calls_initialized"))
			{
                TestFlight.log("call Log initialize was successful. going to update UI");
				if (currentFragment instanceof CallsFragment)
				{
					((CallsFragment)currentFragment).updateCallList();
				}
			}
            else if(((String)data).equalsIgnoreCase("calls_downloaded"))
            {
                ((CallsFragment)currentFragment).updateCallListDownload();
            }
		}
		
	}

	@Override
	public void onCancelLoad() {
		// TODO Auto-generated method stub
		
	}

 }
