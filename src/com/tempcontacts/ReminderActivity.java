package com.tempcontacts;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.datamodel.ReminderItem;
import com.library.Constants;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;

@SuppressLint("SimpleDateFormat")
/*  Activity for creating reminders*/
public class ReminderActivity extends Activity{
	private EditText reminder_name;
	private EditText reminder_number;
	private EditText reminder_info;
	FrequentlyUsedMethods faq;
	
	private final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
	String date ;
	private ImageView pasteBtn;
	private ImageView reminder_contact_choose;
	ReminderItem remind_update;
	
	ContactItem received_contact;
	
	Calendar import_calendar = Calendar.getInstance();
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setTitle(R.string.calendar);
        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setCustomView(R.layout.reminder_bar);
        
        getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        this.setContentView(R.layout.reminder);
        
        reminder_name = (EditText)findViewById(R.id.reminder_name);
        reminder_number= (EditText)findViewById(R.id.reminder_number);
        reminder_info= (EditText)findViewById(R.id.reminder_info);
        reminder_contact_choose = (ImageView)findViewById(R.id.reminder_contact_choose);
        faq = new FrequentlyUsedMethods(this);
        
        pasteBtn = (ImageView)findViewById(R.id.pasteBtn);
        pasteBtn.setOnClickListener(new OnClickListener()
        {

				@Override
				public void onClick(View v)
				{
					
					if (SharedPrefs.getInstance().getSharedPrefs()!=null)
					{
						
						 if (!SharedPrefs.getInstance().getSharedPrefs().getString(Constants.BUFFER_NUMBER,"").equalsIgnoreCase(""))
							 reminder_number.setText(SharedPrefs.getInstance().getSharedPrefs().getString(Constants.BUFFER_NUMBER,""));
						
					}
					
				}
        });
        reminder_contact_choose.setOnClickListener(new OnClickListener()
        {

				@Override
				public void onClick(View v) {
					Intent a  = new Intent(ReminderActivity.this, ContactsChooseReminderActivity.class);
					startActivityForResult(a,1 );
				}
        });
        
        long milisecs = 0 ;
        if(getIntent().getExtras()!=null)
		{
        	if (getIntent().getExtras().getParcelable("reminder_update")!=null)
        		{
        			remind_update = getIntent().getExtras().getParcelable("reminder_update");
        			if (remind_update.getMention_name()!=null)
        				reminder_name.setText(remind_update.getMention_name());
        			if (remind_update.getNumber()!=null)
        				reminder_number.setText(remind_update.getNumber());
        			if (remind_update.getInfo()!=null)
        				reminder_info.setText(remind_update.getInfo());
        		}
        	milisecs = getIntent().getExtras().getLong("choosed_date_time_milisecs");		
		}
        
		Date date = new Date(milisecs);
		import_calendar.clear();
		import_calendar.setTime(date);
    }   
	
	
	
	 @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        case R.id.reminder_save:
	            if (reminder_name.getText().toString().length()<1)
	            	Toast.makeText(getApplicationContext(), this.getResources().getString(R.string.reminder_event_name_toast), Toast.LENGTH_SHORT).show();
	            else if (reminder_number.getText().toString().length()<1)
	            	Toast.makeText(getApplicationContext(), this.getResources().getString(R.string.reminder_number_toast), Toast.LENGTH_SHORT).show();
	            else if (!faq.numberValidate(reminder_number.getText().toString()))
	            {
	            	Toast.makeText(getApplicationContext(), this.getResources().getString(R.string.reminder_correct_number_format_toast), Toast.LENGTH_SHORT).show();
	            }
	            else
	            {
	            	if(getIntent().getExtras()!=null)
	            		{
	            		
	            		if (remind_update!=null)
	            		{
	            			if (!reminder_name.getText().toString().equalsIgnoreCase(""))
	            				remind_update.setMention_name(reminder_name.getText().toString());
	            			if (!reminder_number.getText().toString().equalsIgnoreCase(""))
	            				remind_update.setNumber(reminder_number.getText().toString());
	            			if (!reminder_info.getText().toString().equalsIgnoreCase(""))
	            				remind_update.setInfo(reminder_info.getText().toString());
	            			faq.updateReminder(remind_update);
	            			finish();
	            		}
	            		else {
			    					int icon = 0;
			    					int calendar_icon = 0;
			    					if (received_contact!=null)
			    					{
			    						if (received_contact.getContactGroup()!= null)
			    							{
			    							received_contact.getContactGroup();
											GroupItem.setContext(getApplicationContext());
			    								icon = faq.returnContactIcon(received_contact.getContactGroup().getIcon());
			    								
			    								String path = getResources().getResourceName(received_contact.getIcon())+"_ev";
			    								calendar_icon = getResources().getIdentifier(path , "drawable", getPackageName());
			    							}
			    						else 
			    							{
			    								icon =  R.drawable.user_not_group;
			    								calendar_icon = R.drawable.user_not_group_ev;
			    							}
			    					}
			    					else 
			    						{
				    						icon =  R.drawable.user_not_group;
											calendar_icon = R.drawable.user_not_group_ev;
			    						}
			    					ReminderItem remind = new ReminderItem(reminder_number.getText().toString(),
			    							reminder_name.getText().toString(),
			    							reminder_info.getText().toString(),import_calendar.getTime(),icon, calendar_icon);
			    					
			    					faq.sendNotification(remind);
			    					
			    					faq.saveReminder(remind);
			    					setResult(1);
			    					finish();
		            		
		            		}
	            		}
	            }
	        	
	            return true;
	        case R.id.reminder_close:
	        	finish();
	        	   return true;
	        case android.R.id.home:
	        	finish();
	        		return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     MenuInflater inflater = getMenuInflater();
	     inflater.inflate(R.menu.reminder_menu, menu);
	     return true;
	 }
	 public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		Log.i("groups frag", Integer.toString(resultCode));
		 if (requestCode==1)
		 { 
			 if (resultCode == 1)
			 {
				 if (data.getExtras()!=null)
				 {
					 received_contact  = (ContactItem)data.getExtras().getParcelable("contact_remind");
					 reminder_number.setText(received_contact.getNumber());
					 reminder_name.setText(received_contact.getName());
				 }
			 }
			
		 }
	 }
}
