package com.tempcontacts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ParseException;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.customdialogs.ReminderDialog;
import com.datamodel.ReminderItem;
import com.library.FrequentlyUsedMethods;
/*  Activity for choosing hours for calendar*/
public class CalendarHoursViewActivity extends Activity implements OnClickListener{
	 
	private GridCellAdapter adapter;
	private GridView calendarView;
	private Calendar _calendar;
	private int month;
	private int year;
	
	private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
	private final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd-MMM-yyyy-hh:mm");
	private final SimpleDateFormat customDateTimeFormatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
	String date ;
	private ImageView prevMonth;
	private TextView currentMonth;
	private ImageView nextMonth;
	Date parsedDate = null;
	FrequentlyUsedMethods faq;
	private Window window;
	private final DateFormat dateFormatterRussian = DateFormat.getDateInstance(DateFormat.LONG, new Locale("ru","RU"));
	private int day;
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			//CalendarView cal = new CalendarView(this);
			
			   getActionBar().setDisplayHomeAsUpEnabled(true);
		        getActionBar().setDisplayShowHomeEnabled(false);
		        getActionBar().setDisplayShowTitleEnabled(true);
		        getActionBar().setTitle(R.string.calendar);
		        getActionBar().setDisplayUseLogoEnabled(false);
		        window = getWindow();
		        window.addFlags(android.view.WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		        window.addFlags(android.view.WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
			setContentView(R.layout.calendar_hour_view);
			calendarView = (GridView) this.findViewById(R.id.calendar);
			faq = new FrequentlyUsedMethods(this);
			String currDate = null;

			if (getIntent().getExtras().getString("choosed_date")!=null)
			{
				currDate = getIntent().getExtras().getString("choosed_date");
			}
			
			try {
				parsedDate = dateFormatter.parse(currDate);
			}
			catch(Exception e)
			{e.printStackTrace();}

			_calendar = FrequentlyUsedMethods.DateToCalendar(parsedDate);
			
			
			month = _calendar.get(Calendar.MONTH) + 1;
			year = _calendar.get(Calendar.YEAR);
			day  = _calendar.get(Calendar.DAY_OF_MONTH);
			
			
			prevMonth = (ImageView) this.findViewById(R.id.prevMonth);
			prevMonth.setOnClickListener(this);

			currentMonth = (TextView) this.findViewById(R.id.currentMonth);

			nextMonth = (ImageView) this.findViewById(R.id.nextMonth);
			nextMonth.setOnClickListener(this);
			
			
			adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month, year );
			adapter.notifyDataSetChanged();
			calendarView.setAdapter(adapter);
			
			setGridCellAdapterToDate(month,year, day);
			currentMonth.setOnClickListener(new View.OnClickListener() {

				  @Override
				  public void onClick(View v) {
					  Intent m = new Intent(CalendarHoursViewActivity.this, CalendarYearActivity.class);
					  m.putExtra("curr_year", parsedDate.toString());
					  startActivityForResult(m,0);
				  }
				});
		}
	public class GridCellAdapter extends BaseAdapter implements OnClickListener
	{
		private static final String tag = "GridCellAdapter";
		private final Context _context;

		private final List<String> list;
		
		private final List<String> hours_list;
		
		private static final int DAY_OFFSET = 1;
		
		
		private final String[] months =  getResources().getStringArray(R.array.calendar_months);
		private final int[] daysOfMonth =getResources().getIntArray(R.array.calendar_days);
		private int daysInMonth;
		private int currentDayOfMonth;
		private int currentWeekDay;
		private Button gridcell;
		private TextView num_events_per_day;
		private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");

		// Days in Current Month
		public GridCellAdapter(Context context, int textViewResourceId, int month, int year)
			{
				super();
				this._context = context;
				this.list = new ArrayList<String>();
				this.hours_list = new ArrayList<String>();
				Calendar calendar = FrequentlyUsedMethods.DateToCalendar(parsedDate);
				setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH)+1);
				setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
				printMonth(month, year);

			}
		private String getMonthAsString(int i)
			{
				return months[i];
			}

		private int getNumberOfDaysOfMonth(int i)
			{
				return daysOfMonth[i];
			}

		public String getItem(int position)
			{
				return hours_list.get(position);
			}

		@Override
		public int getCount()
			{
				return hours_list.size();
			}

		/**
		 * Prints Month
		 * 
		 * @param mm
		 * @param yy
		 */
		private void printMonth(int mm, int yy)
			{
				// The number of days to leave blank at
				// the start of this month.
				int trailingSpaces = 0;
				int daysInPrevMonth = 0;
				int prevMonth = 0;
				int prevYear = 0;
				int nextMonth = 0;
				int nextYear = 0;

				int currentMonth = mm - 1;
				String currentMonthName = getMonthAsString(currentMonth);
				daysInMonth = getNumberOfDaysOfMonth(currentMonth);


				// Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
				GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);

				if (currentMonth == 11)
					{
						prevMonth = currentMonth - 1;
						daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
						nextMonth = 0;
						prevYear = yy;
						nextYear = yy + 1;
					}
				else if (currentMonth == 0)
					{
						prevMonth = 11;
						prevYear = yy - 1;
						nextYear = yy;
						daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
						nextMonth = 1;
					}
				else
					{
						prevMonth = currentMonth - 1;
						nextMonth = currentMonth + 1;
						nextYear = yy;
						prevYear = yy;
						daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
					}

				// Compute how much to leave before before the first day of the
				// month.
				// getDay() returns 0 for Sunday.
				int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
				trailingSpaces = currentWeekDay;


				if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1)
					{
						++daysInMonth;
					}

				// Trailing Month days
				for (int i = 0; i < trailingSpaces; i++)
					{
						list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
					}

				// Current Month Days
				for (int i = 1; i <= daysInMonth; i++)
					{
						if (i == getCurrentDayOfMonth())
							{
								list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
							}
						else
							{
							
								list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
								
							}
					}

				// Leading Month days
				for (int i = 0; i < list.size() % 7; i++)
					{
						list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
					}
				for (int a = 1; a< 25; a++)
				{
					hours_list.add(Integer.toString(a));
				}
			}

		@Override
		public long getItemId(int position)
			{
				return position;
			}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
			{
				View row = convertView;
				if (row == null)
					{
						LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
						row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
					}
 
				// Get a reference to the Day gridcell
				DisplayMetrics metrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(metrics);
				int width = metrics.widthPixels;
				int height = metrics.heightPixels;
				
				
				gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
				gridcell.setOnClickListener(this);
				gridcell.setMinimumHeight(height/7);
				gridcell.setMinimumWidth(width/4);
				// ACCOUNT FOR SPACING

				String[] day_color = list.get(position).split("-");
				String theday = day_color[0];
				String themonth = day_color[2];
				String theyear = day_color[3];
				String hour = hours_list.get(position);


				// Set the Day GridCell
				gridcell.setText(hour+":00");
				
				gridcell.setTag(hour);

				if (day_color[1].equals("GREY"))
					{
						gridcell.setTextColor(Color.LTGRAY);
					}
				if (day_color[1].equals("WHITE"))
					{
						gridcell.setTextColor(Color.BLACK);
					}
				if (day_color[1].equals("BLUE"))
					{
						gridcell.setTextColor(getResources().getColor(R.color.static_text_color));
					}
				
				ArrayList<ReminderItem> reminders = faq.getDisplayReminders(CalendarHoursViewActivity.this);
				for (ReminderItem remind : reminders)
				{
					if (faq.getParsedDateDay(remind.getDate()).equals(faq.getParsedDateDay(parsedDate)))
					{
						if (Integer.parseInt(gridcell.getTag().toString())== FrequentlyUsedMethods.DateToCalendar(remind.getDate()).get(Calendar.HOUR_OF_DAY))
						  {
							  gridcell.setCompoundDrawablesWithIntrinsicBounds(0, 0,0,remind.getCalendar_icon());
							  gridcell.setTag( reminders.indexOf(remind));
						  }
						}
				}
				return row;
			}
		@Override
		public void onClick(View view)
			{
			 ReminderItem rem = null;
			 String date_month_year = null;
				if (view.getTag() instanceof Integer)
					{
						rem = faq.getDisplayReminders(CalendarHoursViewActivity.this).get(Integer.parseInt(view.getTag().toString()));
					}
				else
					{
						date_month_year = (String) view.getTag();
					}

				if (rem==null)
				{
				try
					{
						
						try
						{
							Intent k = new Intent(CalendarHoursViewActivity.this, ReminderActivity.class);
							_calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(date_month_year));
							parsedDate = _calendar.getTime();
							k.putExtra("choosed_date_time", parsedDate.toString());
							k.putExtra("choosed_date_time_milisecs", 	_calendar.getTimeInMillis());
							
							startActivityForResult(k,1);
	
						}
					catch (ParseException e)
						{
							e.printStackTrace();
						}
						
					}
				catch (ParseException e)
					{
						e.printStackTrace();
					}
				}
				else 
				{
					Intent a  = new Intent(CalendarHoursViewActivity.this, ReminderDialog.class);
					a.putExtra("reminder_edit", rem);
					startActivityForResult(a,1);

					
				}

			}

		public int getCurrentDayOfMonth()
			{
				return currentDayOfMonth;
			}

		private void setCurrentDayOfMonth(int currentDayOfMonth)
			{
				this.currentDayOfMonth = currentDayOfMonth;
			}
		public void setCurrentWeekDay(int currentWeekDay)
			{
				this.currentWeekDay = currentWeekDay;
			}
		public int getCurrentWeekDay()
			{
				return currentWeekDay;
			}
	}
	@TargetApi(Build.VERSION_CODES.CUPCAKE)
	private void setGridCellAdapterToDate(int month, int year, int day)
		{
			_calendar.set(year, month-1, day);
			currentMonth.setText(dateFormatterRussian.format(_calendar.getTime()));
		}
	@Override
	public void onClick(View v) {
		if (v == prevMonth)
		{
			if (month <= 1)
				{
					month = 12;
					year--;
				}
			else
				{
				if (day<1)
				{
					month--;
					day = adapter.getNumberOfDaysOfMonth(month);
				}
				else 
					day--;
					
				}
			setGridCellAdapterToDate(month, year, day);
		}
	if (v == nextMonth)
		{
			if (month > 11)
				{
					month = 1;
					year++;
				}
			else
				{
				if (day>adapter.getNumberOfDaysOfMonth(month))
				{
					day=1;
					month++;
				}
				else
					day++;
					
				}
			setGridCellAdapterToDate(month, year, day);
		}

		
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		// return from CalendarYearActivity
		if (requestCode==0)
		{
			 if (resultCode == 1)
			 {
			 try{
				 if(data.getExtras()!=null)
				 	parsedDate = customDateTimeFormatter.parse(data.getExtras().getString("choosed_year"));
				 	_calendar.clear();
				 	_calendar = FrequentlyUsedMethods.DateToCalendar(parsedDate);
				 	setGridCellAdapterToDate(_calendar.get(Calendar.MONTH) +1, _calendar.get(Calendar.YEAR), _calendar.get(Calendar.DAY_OF_MONTH));
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
			 }
		}
		// return from RemindAcvtivity
		if (requestCode==1)
		{
					if (resultCode ==1 )
					{
						updateLayout();
					}
		}
	 }
	private void updateLayout()
	{
		_calendar = FrequentlyUsedMethods.DateToCalendar(parsedDate);
		month = _calendar.get(Calendar.MONTH) + 1;
		year = _calendar.get(Calendar.YEAR);
		adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month, year );
		adapter.notifyDataSetChanged();
		calendarView.setAdapter(adapter);
	}
	@Override
	protected void onResume() {
	    
		updateLayout();
		super.onResume();
	    
		
	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) {
        case android.R.id.home:
        	finish();
                    return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
