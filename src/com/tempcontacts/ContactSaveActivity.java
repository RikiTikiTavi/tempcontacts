package com.tempcontacts;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.arrayadapters.GroupChooseAdapter;
import com.customdialogs.TermsPickerDialog;
import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.library.Constants;
import com.library.DatabaseHandler;
import com.library.FrequentlyUsedMethods;
import com.library.IFragmentsListener;

public class ContactSaveActivity extends ListActivity implements IFragmentsListener{
	FrequentlyUsedMethods faq;
	private GroupChooseAdapter adapter;
	private EditText new_contact_number;
	private EditText new_contact_info;
	private EditText new_contact_name;
	
	GroupItem choosed_group;
	ContactItem contact;
	String parcedNumber;
	ContactItem contact_edit;
	IFragmentsListener listener;
	@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        faq = new FrequentlyUsedMethods(this);
        setContentView(R.layout.contact_save);
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setTitle(R.string.contact_save_title);
        
        new_contact_name = (EditText)findViewById(R.id.new_contact_name);
        new_contact_info = (EditText)findViewById(R.id.new_contact_info);
        new_contact_number = (EditText)findViewById(R.id.new_contact_number);
        
        if(getIntent().getExtras()!=null)
        {
        	if (getIntent().getExtras().getString(Constants.NEW_CONTACT_NUMBER)!=null)
        	{
        		parcedNumber = getIntent().getExtras().getString(Constants.NEW_CONTACT_NUMBER);
        		new_contact_number.setText(parcedNumber);
        	}
        	else if (getIntent().getExtras().getParcelable("contact_edit")!=null)
        	{
        		contact_edit= getIntent().getExtras().getParcelable("contact_edit");
        		new_contact_name.setText(contact_edit.getName());
    	    	new_contact_number.setText(contact_edit.getNumber());
    	    	new_contact_info.setText(contact_edit.getContact_info());
        	}
        	
        }
        new_contact_name.setSelection(0);
        new_contact_number.setSelection(0);
        new_contact_info.setSelection(0);
        adapter   = new GroupChooseAdapter(this,
        		R.layout.contact_save, faq.getDisplayGroups(this));
	    ListView contancts_list = getListView();
	    LayoutInflater inflater = getLayoutInflater();
	    ViewGroup header = (ViewGroup)inflater.inflate(R.layout.new_contact_groups_header, null);
	    contancts_list.addHeaderView(header,null,false);
	    header.setClickable(false);
	    header.setFocusable(false);
	    header.setFocusableInTouchMode(false);
	    contancts_list.setAdapter(adapter);
	    contancts_list.setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if (!adapter.getItems().isEmpty()){
					Log.i("clicked item", arg1.getClass().toString());
						final CheckBox checkBox = (CheckBox)arg1
									.findViewById(R.id.groupCheckbox);
					if(adapter.getChecks().get(arg2-1).intValue()==0)
						{
							checkBox.setChecked(true);
							adapter.getChecks().set(arg2-1, 1);
						}
						else 
						{
							checkBox.setChecked(false);
							adapter.getChecks().set(arg2-1, 0);
						}
					choosed_group =	 adapter.getItem(arg2-1);

				
				}
			}});
	    
	}
	
	 @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        case R.id.group_save:
	        	if (new_contact_name.getText().toString().length()<1)
	        		Toast.makeText(getApplicationContext(), this.getResources().getString(R.string.contact_choose_name), Toast.LENGTH_SHORT).show();
	        	else if (new_contact_number.getText().toString().length()<1)
	        		Toast.makeText(getApplicationContext(),this.getResources().getString(R.string.contact_choose_number), Toast.LENGTH_SHORT).show();
	        	else if (!faq.numberValidate(new_contact_number.getText().toString()))
	        		Toast.makeText(getApplicationContext(), this.getResources().getString(R.string.contact_correct_number_format_toast), Toast.LENGTH_SHORT).show();
	        	else 
	        	{
	        		if (contact_edit !=null)
	        		{
	        			
	        			Intent i = new Intent(this, TermsPickerDialog.class);
	        			i.putExtra("contact_editing", contact_edit);
	        			startActivityForResult(i,1);
	        		}
	        		else 
	        		{
	        			Intent i = new Intent(this, TermsPickerDialog.class);
	        			ContactItem new_cont  = new ContactItem(new_contact_name.getText().toString(),new_contact_number.getText().toString() , 0, 0);
	        			new_cont.setContact_info(new_contact_info.getText().toString());
	        			i.putExtra("new_contact", new_cont);
	        			i.putExtra("new_contact_group", choosed_group);
	        			startActivityForResult(i,1);
	        		}
	        	}
	            return true;
	        case android.R.id.home:
	        	finish();
	        	 return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
	 public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		if (requestCode==1)
		{
		 
			if (resultCode == 1)
			 {
				 setResult(1);
				 finish();
				
			 }
			if (resultCode==2)
			{
				if(data.getExtras().getParcelable("contact_editing_time_change")!=null)
				{
				
					if (contact_edit !=null)
	        		{
	        			DatabaseHandler db = new DatabaseHandler(getApplicationContext());
	        			Dao<ContactItem,Integer> daoContact = null;
	        			Log.i("received concncnc", contact_edit.toString());
	        			try {
	        				daoContact=	db.getContactDao();
	        				QueryBuilder<ContactItem,Integer> uQb = daoContact.queryBuilder();
	        				uQb.where().eq(Constants.CONTACT_NUMBER, contact_edit.getNumber());
	        				QueryBuilder<ContactItem,Integer> urQb = daoContact.queryBuilder();
	        				urQb.where().eq(Constants.CONTACT_NUMBER,  contact_edit.getNumber());
	        				List<ContactItem> results = urQb.query();
	        				ContactItem a = results.get(0);
	        			a.setName(new_contact_name.getText().toString());
	        			a.setNumber(new_contact_number.getText().toString());
	        			a.setDays(((ContactItem)data.getExtras().getParcelable("contact_editing_time_change")).getDays());
	        			a.setContact_info(new_contact_info.getText().toString());
	        			if (choosed_group != null)
	        			{
	        				a.setContactGroup(choosed_group);
	        				a.setIcon(faq.returnContactIcon(choosed_group.getIcon()));
	        			}
	        			daoContact.update(a);
	        			}catch(Exception e)
	        			{
	        				e.printStackTrace();
	        			}
	        			setResult(1);
	        			finish();
	        		}
				}
				
			}
		
		
		}
	 }
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     MenuInflater inflater = getMenuInflater();
	     inflater.inflate(R.menu.new_group_menu, menu);
	     return true;
	 }

	@Override
	public void fragmentsEventsListener(int flag) {
		// TODO Auto-generated method stub
		
	}
	
}
