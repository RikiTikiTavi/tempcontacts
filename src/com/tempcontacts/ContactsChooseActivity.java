package com.tempcontacts;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ListView;

import com.arrayadapters.ContactArchiveAdapter;
import com.datamodel.ContactItem;
import com.datamodel.ContactItemParcelable;
import com.datamodel.GroupItem;
import com.library.FrequentlyUsedMethods;
/*  Activity for choosing contacts for saving*/
public class ContactsChooseActivity extends ListActivity{
	private FrequentlyUsedMethods faq;
	private ContactArchiveAdapter adapter;
	private ListView contancts_list;
	ArrayList<ContactItem> contactsToSave = new ArrayList<ContactItem>();
	private GroupItem group_contacts_add; 
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			this.setContentView(R.layout.contacts_fragment);
			  faq = new FrequentlyUsedMethods(this);
			  
			   adapter = new ContactArchiveAdapter(this,
				       R.layout.call_item, faq.getDisplayContacts(this));
			
			   getActionBar().setDisplayHomeAsUpEnabled(true);
		        getActionBar().setDisplayShowHomeEnabled(false);
		        getActionBar().setDisplayShowTitleEnabled(true);
		        getActionBar().setTitle(R.string.contact_save_title);   
		        
		   
		    contancts_list = getListView();
		    contancts_list.setAdapter(adapter);
		    if (getIntent().getExtras()!=null)
	    	{
	    		group_contacts_add  = (GroupItem)getIntent().getExtras().getParcelable("group_contacts_add");
	    		for(ContactItem a:  faq.getDisplayContacts(this))
	    		{
	    			if ( a.getContactGroup()!=null)
	    			{
		    			if (a.getContactGroup().getName().equalsIgnoreCase(group_contacts_add.getName()))
		    				{
		    					adapter.getItem(adapter.getPosition(a)).setFlag(true);
		    					adapter.notifyDataSetChanged();
		    				}
		    			else 
		    			{
		    				adapter.getItem(adapter.getPosition(a)).setFlag(false);
	    					adapter.notifyDataSetChanged();
		    			}
	    			}
	    		}
	    	}
		    
		    contancts_list.setOnItemClickListener(new OnItemClickListener()
			  {
				private ContactItem choosed_group;

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					
					Log.i("clicked item", adapter.getItem(arg2).toString());
						final CheckBox checkBox = (CheckBox)arg1
									.findViewById(R.id.contactCheckbox);
						
						if(adapter.getChecks().get(arg2).intValue()==0)
						{
							checkBox.setChecked(true);
							adapter.getChecks().set(arg2, 1);
							contactsToSave.add( adapter.getItem(arg2));
						}
						else 
						{
							checkBox.setChecked(false);
							adapter.getChecks().set(arg2, 0);
							contactsToSave.remove(adapter.getItem(arg2));
						}
					
					choosed_group =	 adapter.getItem(arg2);
				}});
		}
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     MenuInflater inflater = getMenuInflater();
	     inflater.inflate(R.menu.new_group_menu, menu);
	     return true;
	 }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) {
        case R.id.group_save:
        	if (!contactsToSave.isEmpty())
        	{
        		
        		Intent intent = new Intent(ContactsChooseActivity.this, GroupEditing.class);
        		ContactItemParcelable contactParcelableContact = new ContactItemParcelable();
    			contactParcelableContact.setList(contactsToSave);
    			Bundle b = new Bundle();
    			b.putParcelableArrayList("contacts_to_new_group",  contactParcelableContact.getList());
    			intent.putExtras(b);
        		if (group_contacts_add != null)
        			{
        			    intent.putExtra("group_contacts_add",group_contacts_add);
        			    faq.saveGroupContacts(group_contacts_add, contactsToSave);
        				setResult(1,intent);
        			}
        		else 
        		{
        				setResult(2,intent);
        		}
        	}
            finish();
            return true;
        case android.R.id.home:
        	finish();
        	 return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
