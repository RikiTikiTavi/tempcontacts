package com.tempcontacts;

import java.sql.SQLException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.arrayadapters.ImageAdapter;
import com.customdialogs.TermsPickerDialog;
import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.library.Constants;
import com.library.ContentRepository;
import com.library.DatabaseHandler;
import com.library.FrequentlyUsedMethods;


/*  Activity for group saving and editing*/
public class GroupEditing extends Activity{
	private ListAdapter imageAdapter;
	private Button btn_add_group;
	private EditText group_name_edittext;
	 GroupItem old_group_item;
	 GroupItem new_group_item;
	 private Integer new_choosedItem;
	 FrequentlyUsedMethods faq;
	private ContentRepository _contactRepo;
	
	 ArrayList<ContactItem> exportedContactList  = new  ArrayList<ContactItem> () ;
	private TextView group_terms_edittext;
	int group_days = 0 ;
	private TextView contacts_count_textView;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
	imageAdapter = new ImageAdapter(this);
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setTitle(R.string.groups);
        getActionBar().setDisplayUseLogoEnabled(false);
        this.setContentView(R.layout.new_group);
        faq = new FrequentlyUsedMethods(this);
        GridView gridview = (GridView) findViewById(R.id.groupIconsGridView);
        gridview.setAdapter(imageAdapter);
        
        gridview.setOnItemClickListener(new OnItemClickListener()
        {

		public void onItemClick(AdapterView<?> parent, View v,
            int position, long id) {
        	  Log.i("image adapter item",imageAdapter.getItem(position).toString());
        	  new_choosedItem = (Integer)imageAdapter.getItem(position);
        	  Log.i("group val",new_choosedItem.toString()) ;
          		}
        });
        group_name_edittext = (EditText) findViewById(R.id.group_name_edittext);
        btn_add_group = (Button) findViewById(R.id.btn_add_group);
        group_terms_edittext = (TextView) findViewById(R.id.group_terms_edittext);
        contacts_count_textView = (TextView)findViewById(R.id.contacts_count_textView);
        if (getIntent().getExtras()!=null)
        old_group_item  = (GroupItem)getIntent().getExtras().getParcelable("group_editing");
        if (old_group_item!=null)
        {
        	group_name_edittext.setText(old_group_item.getName());
        	//if(old_group_item.getDays()!=0)
        		group_terms_edittext.setText(Integer.toString(old_group_item.getDays()));
        	//if (faq.getGroupContactsListSize(old_group_item) !=0)
        		contacts_count_textView.setText(Integer.toString(faq.getGroupContactsListSize(old_group_item)));
        	
        Log.i("group contacts list", Integer.toString(faq.getGroupContactsListSize(old_group_item)));
		}
        btn_add_group.setOnClickListener(new OnClickListener(){

			private ContentRepository _contactRepo;

			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(GroupEditing.this, ContactsChooseActivity.class);
				if (old_group_item !=null)
				{
					i.putExtra("group_contacts_add", old_group_item);
					startActivityForResult(i, 1);
				}
				else
				{
					startActivityForResult(i, 0);
				}
			}
        	
        });
        group_terms_edittext.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Intent a = new Intent(GroupEditing.this, TermsPickerDialog.class);
				startActivityForResult(a,2);
			}
        });
    }
	 public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		 if (requestCode ==0)
		 {
				 if (resultCode == 0)
				 {
					 finish();
				 }
				 else if(resultCode == 1)
				 {
					 if (data !=null)
					 {
						 Bundle b = data.getExtras();
						 exportedContactList=  b.getParcelableArrayList("contacts_to_new_group"); 

					 }

					 
				}
		 }
		 else if(requestCode==1)
		 {
			 if (resultCode == 0)
			 {
			 }
			 if (resultCode == 1)
			 {
				 if (data !=null)
				 {
					 Bundle b = data.getExtras();
					 exportedContactList=  b.getParcelableArrayList("contacts_to_new_group"); 
					 if (!exportedContactList.isEmpty())
					 {
					 contacts_count_textView.setText(Integer.toString(Integer.parseInt(contacts_count_textView.getText().toString())+exportedContactList.size()));
					 }
					 old_group_item = data.getExtras().getParcelable("group_contacts_add");
					 faq.saveGroupContacts(old_group_item, exportedContactList);
					 
				 }
				
				 //finish();
			 }
			 if (resultCode == 2)
			 {
				 if (data !=null)
				 {
					 Bundle b = data.getExtras();
					 exportedContactList=  b.getParcelableArrayList("contacts_to_new_group"); 
					 if (!exportedContactList.isEmpty())
					 {
					 contacts_count_textView.setText(Integer.toString(Integer.parseInt(contacts_count_textView.getText().toString())+exportedContactList.size()));
					 }
					 
				 }
			 }
			
			 
			 
		 }
		 else if(requestCode==2)
		 {
			 if (resultCode ==1)
			 {
				 group_days = data.getExtras().getInt("group_days");
				 group_terms_edittext.setText(Integer.toString(group_days));
			 }
			 
		 }
	 }
	 
	 	@Override
	    public boolean onOptionsItemSelected(MenuItem item) 
	    {
	        switch (item.getItemId()) {
	        case R.id.group_save:
	        	if (old_group_item != null)
	        	{
	        	if (new_choosedItem!=null)
	        	new_group_item = new GroupItem(old_group_item.getId(),group_name_edittext.getText().toString(),new_choosedItem,old_group_item.getCount() );
	        	else 
	        		new_group_item = new GroupItem(old_group_item.getId(),group_name_edittext.getText().toString(),old_group_item.getIcon(),old_group_item.getCount() );
	        	if (group_days != 0)
	        		{
	        			new_group_item.setDays(group_days);
	        			faq.updateGroupDays(new_group_item);
	        		}
	        	if (!old_group_item.getName().equalsIgnoreCase(new_group_item.getName()))
	        		faq.updateGroupName(new_group_item);
	        	if (old_group_item.getIcon()!=new_group_item.getIcon())
	        		faq.updateGroupIcon(new_group_item);
	        	setResult(5);
	        	finish();
	        	}
	        	else 
	        	{
					if (group_name_edittext.getText().toString().equals(""))
						Toast.makeText(getApplicationContext(), this.getResources().getString(R.string.group_choose_name), Toast.LENGTH_LONG).show();
					else if (new_choosedItem == null)
						Toast.makeText(getApplicationContext(),this.getResources().getString(R.string.group_choose_icon), Toast.LENGTH_LONG).show();
					else 
						{
						GroupItem newGroup = new GroupItem(group_name_edittext.getText().toString(),new_choosedItem, 0,group_days );
						_contactRepo=new ContentRepository(GroupEditing.this.getContentResolver(),GroupEditing.this);
						faq.saveNonExistingGroupContacts(newGroup, exportedContactList);
						finish();
						}
	        	}
	            
	            return true;
	        case android.R.id.home:
	        	finish();
	                    return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     MenuInflater inflater = getMenuInflater();
	     inflater.inflate(R.menu.new_group_menu, menu);
	     return true;
	 }
}
