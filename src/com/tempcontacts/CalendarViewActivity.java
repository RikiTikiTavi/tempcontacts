package com.tempcontacts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ParseException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.customdialogs.ReminderDialog;
import com.datamodel.ReminderItem;
import com.library.Constants;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;

/*  main Activity for calendar*/
public class CalendarViewActivity extends Activity implements OnClickListener
	{
		private static final String tag = "SimpleCalendarViewActivity";

		private ImageView calendarToJournalButton;
		private Button selectedDayMonthYearButton;
		private TextView currentMonth;
		private ImageView prevMonth;
		private ImageView nextMonth;
		private GridView calendarView;
		private GridCellAdapter adapter;
		private Calendar _calendar;
		private int month, year;
		private final DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("ru","RU"));
		private static final String dateTemplate = "MMMM yyyy";
		FrequentlyUsedMethods faq;
		Date parsedDate;
		
		ArrayList<String> daysOfMonth;
		/** Called when the activity is first created. */
		@Override
		public void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
				setContentView(R.layout.calendar_view);
				   getActionBar().setDisplayHomeAsUpEnabled(true);
			        getActionBar().setDisplayShowHomeEnabled(false);
			        getActionBar().setDisplayShowTitleEnabled(true);
			        getActionBar().setTitle(R.string.calendar);
			        getActionBar().setDisplayUseLogoEnabled(false);
			        _calendar = Calendar.getInstance(Locale.ROOT);
				month = _calendar.get(Calendar.MONTH) + 1;
				year = _calendar.get(Calendar.YEAR);
				faq = new FrequentlyUsedMethods(this);

				prevMonth = (ImageView) this.findViewById(R.id.prevMonth);
				prevMonth.setOnClickListener(this);

				currentMonth = (TextView) this.findViewById(R.id.currentMonth);
				  SimpleDateFormat sdf = new SimpleDateFormat(dateTemplate, new Locale("ru", "RU"));
			

				nextMonth = (ImageView) this.findViewById(R.id.nextMonth);
				nextMonth.setOnClickListener(this);

				calendarView = (GridView) this.findViewById(R.id.calendar);

				// Initialised
				adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month, year);
				adapter.notifyDataSetChanged();
				calendarView.setAdapter(adapter);
				setGridCellAdapterToDate(month, year);
			}
		
		private void findMonthEvents()
		{
			ArrayList<ReminderItem> reminders = faq.getDisplayReminders(this);
			for (ReminderItem remind : reminders)
			{
				if (faq.getParsedDateMonth(remind.getDate()).equals(faq.getParsedDateMonth(parsedDate)))
				{
					
					
					
					final int size = calendarView.getCount();
					for(int i = 0; i < size; i++) 
					{
					  View gridChild =  calendarView.getChildAt(i);
					  Button gridcell = (Button) gridChild.findViewById(R.id.calendar_day_gridcell);
					  if (Integer.parseInt(gridcell.getTag().toString())== FrequentlyUsedMethods.DateToCalendar(remind.getDate()).get(Calendar.HOUR))
						  gridcell.setBackgroundResource(R.drawable.user_red_group);
					}

				}

			}
		}

		/**
		 * 
		 * @param month
		 * @param year
		 */
		@TargetApi(Build.VERSION_CODES.CUPCAKE)
		private void setGridCellAdapterToDate(int month, int year)
			{
				adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month, year);

				_calendar.clear();
				_calendar.set(Calendar.YEAR, year);
				_calendar.set(Calendar.MONTH, month);
				currentMonth.setText(adapter.getMonthAsString(month-1)+" "+Integer.toString(year));;
				adapter.notifyDataSetChanged();
				calendarView.setAdapter(adapter);
			}

		@Override
		public void onClick(View v)
			{
				if (v == prevMonth)
					{
						if (month <= 1)
							{
								month = 12;
								year--;
							}
						else
							{
								month--;
							}
						setGridCellAdapterToDate(month, year);
					}
				if (v == nextMonth)
					{
						if (month > 11)
							{
								month = 1;
								year++;
							}
						else
							{
								month++;
							}
						setGridCellAdapterToDate(month, year);
					}

			}

		@Override
		public void onDestroy()
			{
				Log.d(tag, "Destroying View ...");
				super.onDestroy();
			}

		// Inner Class
		public class GridCellAdapter extends BaseAdapter implements OnClickListener
			{
				private static final String tag = "GridCellAdapter";
				private final Context _context;

				private final List<String> list;
				private static final int DAY_OFFSET = 1;
				
				private final String[] weekdays = getResources().getStringArray(R.array.calendar_weekdays);
				private final String[] months = getResources().getStringArray(R.array.calendar_months);
				
				private final int[] daysOfMonth =getResources().getIntArray(R.array.calendar_days);
				private final int month, year;
				private int daysInMonth, prevMonthDays;
				private int currentDayOfMonth;
				private int currentWeekDay;
				private Button gridcell;
				private TextView num_events_per_day;
				Calendar calendar = Calendar.getInstance();
				
				ReminderItem reminder_click;
				private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy",new Locale("ru", "RU"));
				// Days in Current Month
				public GridCellAdapter(Context context, int textViewResourceId, int month, int year)
					{
						super();
						this._context = context;
						this.list = new ArrayList<String>();
						this.month = month;
						this.year = year;

						Log.d(tag, "==> Passed in Date FOR Month: " + month + " " + "Year: " + year);
						Calendar calendar = Calendar.getInstance();
						setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
						setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));

						// Print Month
						printMonth(month, year);

						// Find Number of Events
					}
				public String getMonthAsString(int i)
					{
						return months[i];
					}

				private String getWeekDayAsString(int i)
					{
						return weekdays[i];
					}

				private int getNumberOfDaysOfMonth(int i)
					{
						return daysOfMonth[i];
					}

				public String getItem(int position)
					{
						return list.get(position);
					}
				public ArrayList<String> getCalendarList()
				{
					return (ArrayList<String>) this.list;
				}
				@Override
				public int getCount()
					{
						return list.size();
					}
				public int[] getDaysOfMonth()
				{
					return this.daysOfMonth;
				}
				
				public boolean holidayCheck(int month, int day)
				{
					boolean result = false;
					
					
					return result;
				}
				/**
				 * Prints Month
				 * 
				 * @param mm
				 * @param yy
				 */
				private void printMonth(int mm, int yy)
					{
					
					
						// The number of days to leave blank at
						// the start of this month.
						int trailingSpaces = 0;
						int leadSpaces = 0;
						int daysInPrevMonth = 0;
						int prevMonth = 0;
						int prevYear = 0;
						int nextMonth = 0;
						int nextYear = 0;

						int currentMonth = mm - 1;
						String currentMonthName = getMonthAsString(currentMonth);
						daysInMonth = getNumberOfDaysOfMonth(currentMonth);

						// Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
						GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 0);

						if (currentMonth == 11)
							{
								prevMonth = currentMonth - 1;
								daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
								nextMonth = 0;
								prevYear = yy;
								nextYear = yy + 1;
							}
						else if (currentMonth == 0)
							{
								prevMonth = 11;
								prevYear = yy - 1;
								nextYear = yy;
								daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
								nextMonth = 1;
							}
						else
							{
								prevMonth = currentMonth - 1;
								nextMonth = currentMonth + 1;
								nextYear = yy;
								prevYear = yy;
								daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
							}

						// Compute how much to leave before before the first day of the
						// month.
						// getDay() returns 0 for Sunday.
						int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
						
						trailingSpaces = currentWeekDay;


						if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1)
							{
								++daysInMonth;
							}

						// Trailing Month days
						for (int i = 0; i < trailingSpaces; i++)
							{
								Log.d(tag, "PREV MONTH:= " + prevMonth + " => " + (prevMonth) + " " + String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i));
								list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" +(prevMonth+1) + "-" + prevYear);
							}

						// Current Month Days
						for (int i = 1; i <= daysInMonth; i++)
							{
								if (i == getCurrentDayOfMonth())
									{
										list.add(String.valueOf(i) + "-BLUE" + "-" + (currentMonth+1) + "-" + yy);
									}
								else
									{
										list.add(String.valueOf(i) + "-WHITE" + "-" + (currentMonth+1) + "-" + yy);
									}
							}

						// Leading Month days
						for (int i = 0; i < list.size() % 7; i++)
							{
								list.add(String.valueOf(i + 1) + "-GREY" + "-" + (nextMonth+1) + "-" + nextYear);
							}
						
					}


				@Override
				public long getItemId(int position)
					{
						return position;
					}

				@SuppressWarnings("deprecation")
				@SuppressLint("NewApi")
				@Override
				public View getView(int position, View convertView, ViewGroup parent)
					{
						View row = convertView;
						if (row == null)
							{
								LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								row = inflater.inflate(R.layout.calendar_day_gridcell, parent, false);
							}

						// Get a reference to the Day gridcell
						gridcell = (Button) row.findViewById(R.id.calendar_day_gridcell);
						gridcell.setOnClickListener(this);

						// ACCOUNT FOR SPACING

						String[] day_color = list.get(position).split("-");
						
						String theday = day_color[0];
						String themonth = day_color[2];
						String theyear = day_color[3];
						
						
						Calendar calendar = Calendar.getInstance();
						gridcell.setText(theday);
						gridcell.setTag(theday + "-" + themonth + "-" + theyear);
						
						// events checking
						ArrayList<ReminderItem> reminders = faq.getDisplayReminders(CalendarViewActivity.this);
						
                        // weekends color changing
                        Calendar c = Calendar.getInstance();
                        calendar.set(year, Integer.parseInt(themonth)-1, Integer.parseInt(theday));
                        if (faq.getDateMMddYY(c.getTime()).equalsIgnoreCase(faq.getDateMMddYY(calendar.getTime())))
                            gridcell.setBackgroundColor(Color.rgb(188, 210, 238));
						if ( (position-6)%7==0  | (position-5)%7==0)
							{
								gridcell.setBackgroundColor(Color.rgb(250, 250, 210));
								 GradientDrawable drawable = new GradientDrawable();
								    drawable.setShape(GradientDrawable.RECTANGLE);
								    drawable.setStroke(2, Color.rgb(176, 209, 232));
								    drawable.setColor(Color.rgb(250, 250, 210));
								    int sdk = android.os.Build.VERSION.SDK_INT;
								    if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
								    	 gridcell.setBackgroundDrawable(drawable);
								    } else {
								    	 gridcell.setBackground(drawable);
								    }
								   
							}
						
						Log.d(tag, "Setting GridCell " + theday + "-" + themonth + "-" + theyear);

						if (day_color[1].equals("GREY"))
							{
								gridcell.setTextColor(Color.LTGRAY);
							}
						if (day_color[1].equals("WHITE"))
							{
							gridcell.setTextColor(Color.BLACK);
								
							}
						if (day_color[1].equals("BLUE"))
							{
								gridcell.setTextColor(getResources().getColor(R.color.static_text_color));
							}
						
					// reminder events searching and setting color
						try
						{
							if (!reminders.isEmpty())
							{

								for (ReminderItem reminder: reminders)
								{

									calendar.set(year, Integer.parseInt(themonth)-1, Integer.parseInt(theday));
									if (faq.getDateMMddYY(reminder.getDate()).equalsIgnoreCase(faq.getDateMMddYY(calendar.getTime())))
									{
										reminder_click = reminder;
										gridcell.setBackgroundColor(Color.rgb(164, 211, 238));
									}
								}
							}
						}
						catch(Exception e)
						{e.printStackTrace();}
						return row;
					}
				@Override
				public void onClick(View view)
					{
					Log.i("onclick calndar view",  view.getTag().toString());
					 ReminderItem rem = null;
					 String date_month_year = null;
								date_month_year = (String) view.getTag();
						try
							{
								
								try {
									Log.i(" before parsing date", date_month_year);
									parsedDate = dateFormatter.parse(date_month_year);
									Intent i = new Intent();
									i.setClass(CalendarViewActivity.this, CalendarHoursViewActivity.class);
									i.putExtra("choosed_date", date_month_year);
									startActivity(i);
								} catch (java.text.ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}
						catch (ParseException e)
							{
								e.printStackTrace();
							}

					}

				public int getCurrentDayOfMonth()
					{
						return currentDayOfMonth;
					}

				private void setCurrentDayOfMonth(int currentDayOfMonth)
					{
						this.currentDayOfMonth = currentDayOfMonth;
					}
				public void setCurrentWeekDay(int currentWeekDay)
					{
						this.currentWeekDay = currentWeekDay;
					}
				public int getCurrentWeekDay()
					{
						return currentWeekDay;
					}
			}
		@Override
		protected void onResume() {
		    super.onResume();
		    adapter.notifyDataSetChanged();
		    calendarView.setAdapter(adapter);
		}
		
		@Override
	    public boolean onOptionsItemSelected(MenuItem item) 
	    {
	        switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	                    return true;
	        default:
	            return super.onOptionsItemSelected(item);
	        }
	    }
		
		public void onActivityResult(int requestCode, int resultCode, Intent data) 
		 {
			// return from ArchiveDialog
			if (requestCode==1)
			{
				if (resultCode ==1 )
				{
					
					adapter = new GridCellAdapter(getApplicationContext(), R.id.calendar_day_gridcell, month, year);
					adapter.notifyDataSetChanged();
					calendarView.setAdapter(adapter);
				}
				
			}
		 }
		

}
