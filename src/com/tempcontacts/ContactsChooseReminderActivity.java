package com.tempcontacts;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.arrayadapters.ContactChooseRemindAdapter;
import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.library.FrequentlyUsedMethods;

/*  Activity for choosing contacts for reminder creation*/
public class ContactsChooseReminderActivity extends ListActivity{
	private FrequentlyUsedMethods faq;
	private ContactChooseRemindAdapter adapter;
	private ListView contancts_list;
	ArrayList<ContactItem> contactsToSave = new ArrayList<ContactItem>();
	private GroupItem group_contacts_add; 
	ContactItem choosed_contact;
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			this.setContentView(R.layout.contacts_fragment);
			  faq = new FrequentlyUsedMethods(this);
			   adapter = new ContactChooseRemindAdapter(this,
				       R.layout.contact_choose_reminder_item, faq.getDisplayContacts(this));
			
			   getActionBar().setDisplayHomeAsUpEnabled(true);
		        getActionBar().setDisplayShowHomeEnabled(false);
		        getActionBar().setDisplayShowTitleEnabled(true);
		        getActionBar().setTitle(R.string.contact_save_title);   
		        
		    if (getIntent().getExtras()!=null)
		    	group_contacts_add  = (GroupItem)getIntent().getExtras().getParcelable("group_contacts_add");
		    contancts_list = getListView();
		    contancts_list.setAdapter(adapter);
		      contancts_list.setOnItemClickListener(new OnItemClickListener()
			  {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					
					choosed_contact  = adapter.getItem(arg2);
					
					
					
				}});
		}
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     MenuInflater inflater = getMenuInflater();
	     inflater.inflate(R.menu.new_group_menu, menu);
	     return true;
	 }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) {
        case R.id.group_save:
        	if (choosed_contact != null)
        	{
        		Intent a = new Intent();
				a.putExtra("contact_remind", choosed_contact);
				setResult(1,a);
        	}
            finish();
            return true;
        case android.R.id.home:
        	finish();
        	 return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
