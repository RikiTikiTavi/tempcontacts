package com.tempcontacts;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.ListActivity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.arrayadapters.ArchiveAdapter;
import com.customdialogs.ArchiveDialog;
import com.datamodel.ArchiveItem;
import com.datamodel.ContactItem;
import com.library.Constants;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;
/*  Activity for archive items list*/
public class ArchiveActivity extends ListActivity{
	private FrequentlyUsedMethods faq;
	private ArchiveAdapter adapter;
	private ListView contancts_list;
	ArchiveItem choosed_archived_item;
	ArrayList<ArchiveItem> contactsToSave = new ArrayList<ArchiveItem>();
	private RelativeLayout selectAllArchive;
	@Override
	public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);

			   getActionBar().setDisplayHomeAsUpEnabled(true);
		        getActionBar().setDisplayShowHomeEnabled(false);
		        getActionBar().setDisplayShowTitleEnabled(true);
		        getActionBar().setTitle(R.string.archive);
		        getActionBar().setDisplayUseLogoEnabled(false);
			this.setContentView(R.layout.archive_listview);
			  faq = new FrequentlyUsedMethods(this);
			   adapter = new ArchiveAdapter(this,
				       R.layout.call_item, faq.getDisplayArchiveItems(this));
			   
			   selectAllArchive = (RelativeLayout)findViewById(R.id.selectAllArchive);  
			   
		    contancts_list = getListView();
		    contancts_list.setAdapter(adapter);
		    contancts_list.setOnItemClickListener(new OnItemClickListener()
			  {
				private ArchiveItem choosed_group;
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					
					Log.i("clicked item", adapter.getItem(arg2).toString());
						final CheckBox checkBox = (CheckBox)arg1
									.findViewById(R.id.contactCheckbox);
						
						if(adapter.getChecks().get(arg2).intValue()==0)
						{
						
							checkBox.setChecked(true);
							adapter.getChecks().set(arg2, 1);
							adapter.getItem(arg2).setFlag(true);
							contactsToSave.add( adapter.getItem(arg2));
							
						}
						else 
						{
							Log.i("im going to discheck item", adapter.getItem(arg2).toString());
							checkBox.setChecked(false);
							adapter.getItem(arg2).setFlag(false);
							adapter.getChecks().set(arg2, 0);
							contactsToSave.remove(adapter.getItem(arg2));
						}
					
					choosed_group =	 adapter.getItem(arg2);
				}});
		    contancts_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

	            public boolean onItemLongClick(AdapterView<?> arg0, View v,
	                    int index, long arg3) {
	            	choosed_archived_item = adapter.getItem(index);
	            	Intent y = new Intent(ArchiveActivity.this, ArchiveDialog.class);
	            	startActivityForResult(y,1);
	            	return true;
	            }
		    }); 
		    
		    selectAllArchive.setOnClickListener(new OnClickListener() {  
		        public void onClick(View v)
	            {
		        	CheckBox checkbox = (CheckBox)v.findViewById(R.id.selectAllArchive_chb);
		        	if (!checkbox.isChecked())
		        	{
		        		checkbox.setChecked(true);
		               for (ArchiveItem a : adapter.getItems())
		               {
							a.setFlag(true);
							contactsToSave.add( a);
							adapter.notifyDataSetChanged();
		               }
		        	}
		        	else 
		        	{
		        		checkbox.setChecked(false);
		        		
		        		 for (ArchiveItem a : adapter.getItems())
			               {
								a.setFlag(false);
								contactsToSave.remove( a);
								adapter.notifyDataSetChanged();
			               }
		        	}
	            }
	         });
		}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		if (requestCode==1)
		{
				if(resultCode == R.drawable.calendar )
				 {
					 ClipboardManager clipboard = (ClipboardManager)this.
					            getSystemService(Context.CLIPBOARD_SERVICE);
					 	this.getContentResolver();
					    ClipData clip = ClipData.newPlainText("number",choosed_archived_item.getNumber());
					    clipboard.setPrimaryClip(clip);
					 SharedPrefs.getInstance().editSharePrefs().putString(Constants.BUFFER_NUMBER, choosed_archived_item.getNumber()).commit();
					 Intent i =  new Intent(this, CalendarViewActivity.class);
	 				startActivity(i);
				 
				 }
				 else if(resultCode==R.drawable.call)
				 {
					 Intent callIntent = new Intent(Intent.ACTION_CALL);
	 		        callIntent.setData(Uri.parse("tel:"+ choosed_archived_item.getNumber()));
	 		        startActivity(callIntent);
				 }
				 else if(resultCode==R.drawable.sms)
				 {
					 
					 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
		                        + choosed_archived_item.getNumber())));
				 }
		}
	 }
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	     MenuInflater inflater = getMenuInflater();
	     inflater.inflate(R.menu.archive_menu, menu);
	     return true;
	 }
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) {
        case R.id.archive_itemDelete:
			   	 faq.removeArchiveList(contactsToSave);
			   	adapter.getItems().removeAll(contactsToSave);
			   	adapter = new ArchiveAdapter(this,
					       R.layout.call_item, faq.getDisplayArchiveItems(this));
			    contancts_list = getListView();
			    contancts_list.setAdapter(adapter);
        	return true;
        case android.R.id.home:
        	finish();
                    return true;
        case R.id.archive_restore:     
        	if (contactsToSave.size()!=0)
        	{
        		faq.restoreContactsFromArchive(contactsToSave);
        		setResult(1);
        		finish();
        	}
        	   return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
