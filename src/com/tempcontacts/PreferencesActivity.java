package com.tempcontacts;


import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.MenuItem;
/*  Activity for preferences*/
public class PreferencesActivity extends Activity{
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayShowTitleEnabled(true);
        getActionBar().setTitle(R.string.preferences);
        getActionBar().setDisplayUseLogoEnabled(false);
        this.setContentView(R.layout.preferences);
    }   
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        switch (item.getItemId()) {
        case android.R.id.home:
        	finish();
                    return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}
