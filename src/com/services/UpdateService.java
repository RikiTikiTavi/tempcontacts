package com.services;

import java.sql.SQLException;
import java.util.ArrayList;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.datamodel.ArchiveItem;
import com.datamodel.ContactItem;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.library.Constants;
import com.library.ContentRepository;
import com.library.DatabaseHandler;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;
/*  service for days of archives and temprorary contacts updating*/
public class UpdateService extends IntentService {
	FrequentlyUsedMethods faq;
	Context context;
	Boolean isRunning = false;
	private ContentRepository _contentRepo;
    public UpdateService() {
        super("UpdateService");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
    	faq = new FrequentlyUsedMethods(this);
    	_contentRepo = new ContentRepository(this.getContentResolver(), this);
    	this.context = this;
//    	if (!isRunning)
    		archiveDaysDeduct();
        return START_REDELIVER_INTENT;
    }
    @Override
    protected void onHandleIntent(Intent intent) {
    }
    
    private void archiveDaysDeduct() 
    {
    	isRunning = true;

    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
		Dao<ContactItem,Integer> daoContact = null;
		Dao<ArchiveItem,Integer> daoArchive = null;
		String yourDate = faq.getCurrentDate();
		

		try
		{
			if (SharedPrefs.getInstance().getSharedPrefs()!=null)
			{
				// checking current date and which was saved previously. If it is, all contacts and archives items days are decremented
				if (!SharedPrefs.getInstance().getSharedPrefs().getString(Constants.CURRENT_DATE, "").equalsIgnoreCase(yourDate))
				{		
					
					ArrayList<ContactItem> contacts = faq.getDisplayContacts(this);
			    	ArrayList<ArchiveItem> archiveItems = faq.getDisplayArchiveItems(this);
					 try {
						 daoContact = db.getContactDao();
						 daoArchive = db.getArchiveDao();
					 }
					 catch(Exception e)
					 {
						 e.printStackTrace();
					 }
					 UpdateBuilder<ContactItem, Integer> updateBuilder = daoContact.updateBuilder();
					 DeleteBuilder<ContactItem, Integer> daoContactDelete = daoContact.deleteBuilder();
					 
					 
					 UpdateBuilder<ArchiveItem, Integer> archUpdateBuilder = daoArchive.updateBuilder();
					 DeleteBuilder<ArchiveItem, Integer> archDaoContactDelete = daoArchive.deleteBuilder();
					 Log.i("contacts size in update service", Integer.toString(contacts.size()));
			    	for (ContactItem a : contacts)
			    	{
                        // if days of contact is 0, it should be moved to archive
                        if ( a.getDays() == 1000)
                        {

                        }
                        else
                        {

                            if (a.getDays()==0)
                            {
                                try {

                                    ArchiveItem toArchive = new ArchiveItem(a.getName(), a.getNumber(),7);
                                    faq.saveArchiveItem(toArchive);
                                    daoContactDelete.where().eq("id", a.getId());
                                    daoContact.delete(daoContactDelete.prepare());

                                }
                                catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            }
                            else
                            {
                                try {
                                    Log.i("contacts to update days", a.toString());
                                    a.setDays(a.getDays()-1);
                                    daoContact.update(a);
                                } catch (SQLException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                            }
                        }
			    	}
			    	for (ArchiveItem arch : archiveItems)
			    	{
                        // if archive item days is 0, then remove it permanently
			    		if (arch.getDays()==0)
			    		{
			    			try {
			    				
			    				archDaoContactDelete.where().eq("id", arch.getId());
			    				daoArchive.delete(archDaoContactDelete.prepare());
								
							} 
			    			catch (SQLException e) {
								e.printStackTrace();
							}
			    		}
			    		else 
			    		{
				    		try {
				    			arch.setDays(arch.getDays()-1);
				    			daoArchive.update(arch);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			    		}
			    	}

                    // setting date to current
			    	SharedPrefs.getInstance().getSharedPrefs().edit().putString(Constants.CURRENT_DATE, yourDate).commit();
			    	stopSelf();
			    
			    }
			}
		}
		catch(Exception e)
		{e.printStackTrace();}
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

}