package com.services;

import com.library.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {
	
    private static final String DEBUG_TAG = "AlarmReceiver";
    public static String ACTION_ALARM = "com.alarammanager.alaram";
    @Override
    public void onReceive(Context context, Intent intent) {
    	
    	Intent downloader = new Intent(context, UpdateService.class);
    	downloader.setAction(Constants.UPDATE_SERVICE);
        context.startService(downloader);
    }
}