package com.services;

import java.util.Date;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.tempcontacts.CalendarViewActivity;
import com.tempcontacts.R;

public class ReminderService extends IntentService {
    private static final int NOTIF_ID = 1;

    public ReminderService(){
        super("ReminderService");
    }

    @Override
      protected void onHandleIntent(Intent intent) {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        long when = System.currentTimeMillis();         // notification time

        NotificationCompat.Builder mBuilder =
        	    new NotificationCompat.Builder(this)
        	    .setSmallIcon(R.drawable.user_not_group)
        	    .setContentTitle(this.getResources().getString(R.string.remind_title))
        	    .setContentText(intent.getExtras().getString("reminder_notification"))
        	    .setWhen(intent.getExtras().getLong("reminder_when"))
        	    .setDefaults(Notification.DEFAULT_ALL)
        	    .setAutoCancel(true);
        Date fate = new Date(intent.getExtras().getLong("reminder_when"));
        Intent notificationIntent = new Intent(this, CalendarViewActivity.class);
        PendingIntent pendingIntent =
                TaskStackBuilder.create(this)
                        // add all of DetailsActivity's parents to the stack,
                        // followed by DetailsActivity itself
                        .addNextIntentWithParentStack(notificationIntent)
                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);
        nm.notify(NOTIF_ID, mBuilder.build());
    }

}