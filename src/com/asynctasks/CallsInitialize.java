package com.asynctasks;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.asynctaskbase.AbstractTaskLoader;
import com.asynctaskbase.ITaskLoaderListener;
import com.asynctaskbase.TaskProgressDialogFragment;
import com.datamodel.CallItem;
import com.j256.ormlite.dao.Dao;
import com.library.DatabaseHandler;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;
import com.tempcontacts.R;
import com.testflightapp.lib.TestFlight;

/** * Calls Log Initialize AsyncTask*/
public class CallsInitialize extends AbstractTaskLoader{
	Context context;
	private static Bitmap bp;
	private boolean errorFlag  = false;
	private FrequentlyUsedMethods faq;
	public CallsInitialize(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
	public static void execute(FragmentActivity fa,	ITaskLoaderListener taskLoaderListener) {

		CallsInitialize loader = new CallsInitialize(fa);
		
		new TaskProgressDialogFragment.Builder(fa, loader, fa.getResources().getString(R.string.download))
				.setCancelable(true)
				.setTaskLoaderListener(taskLoaderListener)
				.show();
	}

	@Override
	public Bundle getArguments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setArguments(Bundle args) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object  loadInBackground() {
        TestFlight.log("logging while CallInitializing Async");
        TestFlight.passCheckpoint("CallInitializing Async CHECKPOINT");
		faq = new FrequentlyUsedMethods(context);
		
		ArrayList<CallItem> contactsList = new ArrayList<CallItem>();
    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
		 try {
			Dao<CallItem,Integer> daoSubject = db.getCallDao();
			contactsList = (ArrayList<CallItem>) daoSubject.queryForAll();
		 }
		 catch(Exception e)
		 {e.printStackTrace();}
		 if (contactsList.isEmpty())
			 faq.initializeCallLog();
		
		return "calls_initialized";
		
		
	 }
	
	@Override 
	 protected void onStopLoading() {
		
		
	    }
}
