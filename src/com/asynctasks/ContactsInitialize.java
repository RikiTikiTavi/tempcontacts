package com.asynctasks;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.asynctaskbase.AbstractTaskLoader;
import com.asynctaskbase.ITaskLoaderListener;
import com.asynctaskbase.TaskProgressDialogFragment;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;
import com.tempcontacts.R;
import com.testflightapp.lib.TestFlight;

/** * Contacts Initialize AsyncTask*/
public class ContactsInitialize extends AbstractTaskLoader{
	Context context;
	private static Bitmap bp;
	private boolean errorFlag  = false;
	private FrequentlyUsedMethods faq;
	public ContactsInitialize(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
	public static void execute(FragmentActivity fa,	ITaskLoaderListener taskLoaderListener) {

		ContactsInitialize loader = new ContactsInitialize(fa);
		
		new TaskProgressDialogFragment.Builder(fa, loader, fa.getResources().getString(R.string.download))
				.setCancelable(true)
				.setTaskLoaderListener(taskLoaderListener)
				.show();
	}

	@Override
	public Bundle getArguments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setArguments(Bundle args) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object  loadInBackground() {
        TestFlight.log("logging while ContactsInitialize Async");
		faq = new FrequentlyUsedMethods(context);
		faq.importContacts();
		
		
		return "contacts_initialized";
		
		
	 }
	
	@Override 
	 protected void onStopLoading() {
		
		
	    }
}
