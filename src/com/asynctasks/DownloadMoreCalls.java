package com.asynctasks;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.asynctaskbase.AbstractTaskLoader;
import com.asynctaskbase.ITaskLoaderListener;
import com.asynctaskbase.TaskProgressDialogFragment;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
import com.testflightapp.lib.TestFlight;

/** * AsyncTask for downloading more calls*/
public class DownloadMoreCalls extends AbstractTaskLoader{
	Context context;
	private static Bitmap bp;
	private boolean errorFlag  = false;
	private FrequentlyUsedMethods faq;
	public DownloadMoreCalls(Context context) {
		super(context);
		this.context = context;
        faq = new FrequentlyUsedMethods(context);
		// TODO Auto-generated constructor stub
	}
	public static void execute(FragmentActivity fa,	ITaskLoaderListener taskLoaderListener) {

		DownloadMoreCalls loader = new DownloadMoreCalls(fa);
		
		new TaskProgressDialogFragment.Builder(fa, loader, fa.getResources().getString(R.string.download))
				.setCancelable(true)
				.setTaskLoaderListener(taskLoaderListener)
				.show();
	}

	@Override
	public Bundle getArguments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setArguments(Bundle args) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public Object  loadInBackground() {
        TestFlight.log("logging while ContactsInitialize Async");

		faq.downloadMoreCalls();
		
		
		return "calls_downloaded";
		
		
	 }
	
	@Override 
	 protected void onStopLoading() {
		    this.cancelLoad();
		
	    }
}
