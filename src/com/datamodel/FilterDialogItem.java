package com.datamodel;
/* class for filter dialogs item (uses in call and contact fragments)*/
public class FilterDialogItem {
	private String text;
	private int icon;
	private boolean check;
	public String getText() {
		return text;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public FilterDialogItem(String text, int icon, boolean check) {
		super();
		this.text = text;
		this.icon = icon;
		this.check = check;
	}
	
	
}
