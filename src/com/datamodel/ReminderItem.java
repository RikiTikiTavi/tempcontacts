package com.datamodel;

import java.util.Calendar;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.library.Constants;

@DatabaseTable(tableName = "reminders")
public class ReminderItem implements  Parcelable{
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.REMINDER_NUMBER)
	private String number;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.REMINDER_NAME)
	private String mention_name;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.REMINDER_DESCRIPTION)
	private String info;
	@DatabaseField(dataType = DataType.DATE, columnName = Constants.REMINDER_DATE)
	private Date date;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.REMINDER_ICON)
	private int icon;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.REMINDER_CALENDAR_ICON)
	private int calendar_icon;
	
	Calendar calendar ;
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public ReminderItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReminderItem(String number, String mention_name,
			String info, Date date, int icon, int calendar_icon) {
		super();
		this.number = number;
		this.mention_name = mention_name;
		this.info = info;
		this.date = date;
		this.icon = icon;
		this.calendar_icon = calendar_icon;
		calendar = Calendar.getInstance();     
	}
	public ReminderItem(int id,String number, String mention_name,
			String info, Date date, int icon, int calendar_icon) {
		super();
		this.id = id;
		this.number = number;
		this.mention_name = mention_name;
		this.info = info;
		this.date = date;
		this.icon = icon;
		this.calendar_icon = calendar_icon;
		calendar = Calendar.getInstance();     
	}
	public ReminderItem(Parcel in) {
		super();
		readFromParcel(in);
	}
	public Calendar getCalendar() {
		calendar.setTime(date);
		return calendar;
	}
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
	public int getCalendar_icon() {
		return calendar_icon;
	}
	public void setCalendar_icon(int calendar_icon) {
		this.calendar_icon = calendar_icon;
	}
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "ReminderItem [number=" + number + ", mention_name="
				+ mention_name + ", icon=" + Integer.toString(icon) + ", date=" + date + "]";
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getMention_name() {
		return mention_name;
	}
	public void setMention_name(String mention_name) {
		this.mention_name = mention_name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public static final Parcelable.Creator<ReminderItem> CREATOR = new Parcelable.Creator<ReminderItem>() {
        public ReminderItem createFromParcel(Parcel in) {
            return new ReminderItem(in);
        }
        public ReminderItem[] newArray(int size) {

            return new ReminderItem[size];
        }
    };
	private void readFromParcel(Parcel in) { 
		id = in.readInt();
		number = in.readString();
		mention_name = in.readString();
		info = in.readString();
		date = (Date) in.readSerializable();
		icon = in.readInt();
		calendar_icon = in.readInt();
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(number);
		dest.writeString(mention_name);
		dest.writeString(info);
		dest.writeSerializable(date);
		dest.writeInt(icon);
		dest.writeInt(calendar_icon);
		
	}
	
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
