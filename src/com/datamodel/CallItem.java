package com.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.library.Constants;

@DatabaseTable(tableName = "calls")
public class CallItem implements Parcelable{
	@DatabaseField(id = true)
	private Integer id;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CALL_NAME)
	private String name;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CALL_NUMBER)
	private String number;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.CALL_ICON)
	private int icon;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CALL_DURATION)
	private String duration;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CALL_DATE)
	private String date;
	@DatabaseField(canBeNull = true, foreign = true,foreignAutoCreate = true,foreignAutoRefresh = true,columnName = Constants.CALL_CONTACT)
	private ContactItem call_contact;
	@DatabaseField(dataType = DataType.STRING,  columnName = Constants.CALL_SYSTEM_ID)
	private String call_system_id;
	
	boolean checkflag;
	CallItem()
	{
		
	}
	public ContactItem getCall_contact() {
		return call_contact;
	}
	public void setCall_contact(ContactItem call_contact) {
		this.call_contact = call_contact;
	}
	public String getCall_system_id() {
		return call_system_id;
	}
	public void setCall_system_id(String call_system_id) {
		this.call_system_id = call_system_id;
	}
	public CallItem(String id, String name, String number, int icon, String date , String duration)
	{
		this.call_system_id = id;
		this.name=name;
		this.number = number;
		this.icon = icon;
		this.date = date;
		this.duration = duration;
	}
	public CallItem(String name, String number, int icon, String date , String duration)
	{
		this.name=name;
		this.number = number;
		this.icon = icon;
		this.date = date;
		this.duration = duration;
	}
	public CallItem(String name, String number, int icon, String date )
	{
		this.name=name;
		this.number = number;
		this.icon = icon;
		this.date = date;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	
	public CallItem(Parcel in) {
		super();
		readFromParcel(in);	// TODO Auto-generated constructor stub
		
	}
	
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public String getName()
	{
		return this.name;
	}
	public String getNumber()
	{
		return this.number;
	}
	public int getIcon()
	{
		return this.icon;
	}
	public String getDays()
	{
		return this.date;
	}
	public int getId()
	{
		return this.id;
	}
	public boolean getFlag()
	{
		return this.checkflag;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public void setNumber(String number)
	{
		this.number = number;
	}
	public void setIcon(int icon)
	{
		this.icon = icon;
	}
	public void setDays(String days)
	{
		this.date = days;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CallItem))
			return false;
		CallItem other = (CallItem) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
	public void setFlag(boolean flag)
	{
		this.checkflag = flag;
	}
	
	@Override
	public String toString()
	{		
	return "{ "+"name="+name+ " " + "number"+ number + " " + "days" + date + "call_contact " +call_contact +"}";
	}
	private void readFromParcel(Parcel in) {   
		name = in.readString();
		number = in.readString();
		icon = in.readInt();
		date = in.readString();
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(number);
		dest.writeInt(icon);
		dest.writeString(date);
		
	}
	public static final Parcelable.Creator<CallItem> CREATOR = new Parcelable.Creator<CallItem>() {
        public CallItem createFromParcel(Parcel in) {
            return new CallItem(in);
        }

        public CallItem[] newArray(int size) {

            return new CallItem[size];
        }

    };
}
