package com.datamodel;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.library.Constants;

@DatabaseTable(tableName = "archive")
public class ArchiveItem {
	@DatabaseField(id = true)
	private Integer id;
	@DatabaseField(foreign=true,canBeNull = true,foreignAutoRefresh = true,columnName = Constants.ARCHIVE_CONTACT)
	private ContactItem contact;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.ARCHIVE_NAME)
	private String name;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.ARCHIVE_NUMBER)
	private String number;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.ARCHIVE_DAYS)
	private int days;
	private boolean flag;
	public ArchiveItem(ContactItem contact)
	{
		this.contact = contact;
		this.name = contact.getName();
		this.number = contact.getNumber();
		this.days = contact.getDays();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public boolean getFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contact == null) ? 0 : contact.hashCode());
		result = prime * result + days;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ArchiveItem))
			return false;
		ArchiveItem other = (ArchiveItem) obj;
		if (contact == null) {
			if (other.contact != null)
				return false;
		} else if (!contact.equals(other.contact))
			return false;
		if (days != other.days)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
	public ArchiveItem(){}
	public ContactItem getContact() {
		return contact;
	}
	public void setContact(ContactItem contact) {
		this.contact = contact;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArchiveItem(String name, String number, int days) {
		super();
		this.name = name;
		this.number = number;
		this.days = days;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	
	

}
