package com.datamodel;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.library.Constants;

@DatabaseTable(tableName = "contacts")
public class ContactItem implements Parcelable{
	private static final long serialVersionUID = 1L;
	@DatabaseField(generatedId = true)
	private Integer id;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CONTACT_NAME)
	private String name;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CONTACT_NUMBER)
	private String number;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.CONTACT_ICON)
	private int icon;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.CONTACT_DAYS)
	private int days;
	@DatabaseField(foreign=true,canBeNull = true,foreignAutoRefresh = true,columnName = Constants.CONTACT_GROUP)
	private GroupItem group;
	@ForeignCollectionField(columnName = Constants.CONTACT_GROUP_LIST, eager = true)
	private ForeignCollection<GroupItem> groups;
	@DatabaseField(dataType = DataType.BOOLEAN, columnName = Constants.CONTACT_CHECKFLAG)
	boolean checkflag;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.CONTACT_INFO)
	private String contact_info;
	
	public String getContact_info() {
		return contact_info;
	}
	public void setContact_info(String contact_info) {
		this.contact_info = contact_info;
	}
	public Integer getId() {
		return id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ContactItem))
			return false;
		ContactItem other = (ContactItem) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
	public void setId(Integer id) {
		this.id = id;
	}



	
	
	ContactItem()
	{
	}
	public ContactItem(String name, String number, int icon, int days)
	{
		this.name=name;
		this.number = number;
		this.icon = icon;
		this.days = days;
//		this.group = group;
	}
	
	public ContactItem(Parcel in) {
		super();
		readFromParcel(in);
	}
	public String getName()
	{
		return this.name;
	}
	public String getNumber()
	{
		return this.number;
	}
	public int getIcon()
	{
		return this.icon;
	}
	public int getDays()
	{
		return this.days;
	}
	public boolean getFlag()
	{
		return this.checkflag;
	}
	public GroupItem getContactGroup()
	{
		return this.group;
	}
	
	public ForeignCollection<GroupItem> getContactGroupCollection()
	{
		return this.groups;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setNumber(String number)
	{
		this.number = number;
	}
	
	public void setIcon(int icon)
	{
		this.icon = icon;
	}
	
	public void setDays(int days)
	{
		this.days = days;
	}
	
	public void setFlag(boolean flag)
	{
		this.checkflag = flag;
	}
	
	public void setContactGroup(GroupItem group)
	{
		this.group = group;
	}
	
	public void setContactGroupCollection(ForeignCollection<GroupItem> groups)
	{
		this.groups = groups;
	}
		
	
	@Override
	public String toString()
	{		
		String retStr;
		if (group==null)
			retStr ="{ "+"name="+name+ " " + "number"+ number + " " + "days" + days +"}"; 
		else 
			retStr ="{ "+"name="+name+ " " + "number"+ number + " " + "days" + days + "group"+ group.toString()
			+"info"+contact_info+"}";
		return retStr;	
	}
	  public void readFromParcel(Parcel in) {
		  	this.name=in.readString();
			this.number = in.readString();
			this.icon = in.readInt();
			this.days = in.readInt();
			this.group = in.readParcelable(GroupItem.class.getClassLoader());
			this.contact_info =  in.readString();

        }
	  public static final Parcelable.Creator<ContactItem> CREATOR = new Parcelable.Creator<ContactItem>() {
          public ContactItem createFromParcel(Parcel in) {
              return new ContactItem(in);
          }

          public ContactItem[] newArray(int size) {

              return new ContactItem[size];
          }

      };
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(number);
		dest.writeInt(icon);
		dest.writeInt(days);
		dest.writeParcelable(group, flags);
		dest.writeString(contact_info);
	}
	
	
	
}
