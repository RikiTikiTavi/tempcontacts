package com.datamodel;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;
/* item for parcing arrayLists of contactItem */
public class ContactItemParcelable implements Parcelable {
    final private static String LOG_TAG = "ParcelableProducts";
    public ArrayList<ContactItem> prod;

    public ContactItemParcelable() {
        prod = new ArrayList<ContactItem>();
     }

    public void setList(ArrayList<ContactItem> _prod){
        prod = _prod;
    }

    public ArrayList<ContactItem> getList() {
        return prod;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeList(prod);
    }
    public static final Creator<ContactItemParcelable> CREATOR = new Creator<ContactItemParcelable>() {
        public ContactItemParcelable createFromParcel(Parcel in) {
          return new ContactItemParcelable(in);
        }

        public ContactItemParcelable[] newArray(int size) {
          return new ContactItemParcelable[size];
        }
      };

      private ContactItemParcelable(Parcel parcel) {
        prod  = new ArrayList<ContactItem>();
        parcel.readTypedList(prod,ContactItem.CREATOR);
      }
}