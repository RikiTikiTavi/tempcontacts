package com.datamodel;

import java.io.Serializable;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.library.Constants;
 
@DatabaseTable(tableName = "groups")
public class GroupItem implements  Parcelable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6706547075075641174L;
	private static Context context;
	
	  public static void setContext(Context mcontext) {
	        if (context == null)
	            context = mcontext;
	    }
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public GroupItem(String name, int icon, int count) {
		super();
		this.name = name;
		this.icon = icon;
		this.count = count;
	}
	
	public GroupItem(String name, int icon, int count, int days) {
		super();
		this.name = name;
		this.icon = icon;
		this.count = count;
		this.days = days;
	}
	
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public GroupItem(int id, String name, int icon, int count) {
		super();
		this.id = id;
		this.name = name;
		this.icon = icon;
		this.count = count;
	}
	
	public GroupItem() {
		
		super();
	}
	public GroupItem(Parcel in) {
		super();
		readFromParcel(in);
	}
	public int getIcon() {
		return icon;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupItem other = (GroupItem) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public int getCount() {
		if (count==0)
			return 0;
		else
		return count;
	}
	public void setContact(ContactItem contact) {
		this.contact = contact;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public ContactItem getContact() {
		return contact;
	}
	
	public void setContactCollection(ForeignCollection<ContactItem> contacts) {
		this.contacts = contacts;
	}
	public ForeignCollection<ContactItem> getContactCollection() {
		return contacts;
	}
	@Override
	public String toString() {
		return "GroupItem [id=" + id + ", name=" + name + ", icon=" + icon
			//	+ ", count=" + count +"foreign size" +  contacts.size()
				+  "]";
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getResourceName()
	{
		return context.getResources().getResourceEntryName(icon);
	}
	@DatabaseField(generatedId = true)
	private Integer id;
	@DatabaseField(dataType = DataType.STRING, columnName = Constants.GROUP_NAME)
	private String name;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.GROUP_ICON)
	private int icon;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.GROUP_COUNT)
	private int count;
	@DatabaseField(foreign=true,canBeNull = true,foreignAutoRefresh = true, columnName = Constants.GROUP_CONTACT)
    private ContactItem contact;
	@ForeignCollectionField(eager = false, columnName=Constants.GROUP_CONTACTS_LIST)
	private ForeignCollection<ContactItem> contacts;
	@DatabaseField(dataType = DataType.INTEGER, columnName = Constants.GROUP_DAYS)
    private int days;
	
	
	public static final Parcelable.Creator<GroupItem> CREATOR = new Parcelable.Creator<GroupItem>() {
        public GroupItem createFromParcel(Parcel in) {
            return new GroupItem(in);
        }

        public GroupItem[] newArray(int size) {

            return new GroupItem[size];
        }

    };
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	private void readFromParcel(Parcel in) {   
		id = in.readInt();
		name = in.readString();
		icon = in.readInt();
		count = in.readInt();
		days = in.readInt();
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeInt(icon);
		dest.writeInt(count);
		dest.writeInt(days);
		
	}
	public int returnContactIcon()
	{
		return context.getResources().getIdentifier(context.getResources().getResourceEntryName(icon).split("_group")[0] , "drawable", context.getPackageName());
	}
	
	
}
