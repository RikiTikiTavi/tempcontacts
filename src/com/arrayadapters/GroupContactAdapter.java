package com.arrayadapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.datamodel.ContactItem;
import com.tempcontacts.R;
/* adapter for all contacts of choosed group displaying */
public class GroupContactAdapter extends ArrayAdapter<ContactItem> {
	private final Context context;
	private final  List<ContactItem> values;

	public GroupContactAdapter(Context context, List<ContactItem> values) {
	    super(context, R.layout.basic_listview, values);
	    this.context = context;
	    this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.dialog_row, parent, false);
	    ContactItem item = values.get(position);
	    TextView textView = (TextView) rowView.findViewById(R.id.dialog_text);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.dialog_image);
	    textView.setText(item.getName());
	    imageView.setBackgroundResource(item.getIcon());


	    return rowView;
	}
	 
}
