package com.arrayadapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.customitems.CustomTextView;
import com.datamodel.GroupItem;
import com.singletones.GroupsViewMapSingletone;
import com.tempcontacts.R;

public class GroupAdapter extends ArrayAdapter<GroupItem> {
	private final Context context;
	private final  List<GroupItem> values;
	 Map<GroupItem, View> contactViewMap = GroupsViewMapSingletone.getInstance().getOrderViewMap();
	private ContactHolder holder;
	private View view;
	private ArrayList<Integer> checks= new ArrayList<Integer>();;
	private int layoutResourceId;
	 public GroupAdapter(Context context,  int layoutResourceId,List<GroupItem> contacts) {
	       super(context, layoutResourceId, contacts);
	       this.layoutResourceId = layoutResourceId;
	       this.context = context;
	       this.values = contacts;
	   }
	  @Override
	   public GroupItem getItem(int arg0) {
	       return values.get(arg0);
	   }

	   @Override
	   public long getItemId(int arg0) {
	       return arg0;
	   }
	   @Override
	   public View getView(final int position, View convertView, ViewGroup parent) {
	       View row = convertView;
	       view = row;
	       checks.add(position, 0);
	       if(row == null)
	       {
	       	LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	           row = inflater.inflate(R.layout.group_item, parent, false);
	           holder = new ContactHolder();
	           holder.contactIcon = (ImageView)row.findViewById(R.id.contactIcon);
	           holder.contactName = (CustomTextView)row.findViewById(R.id.contactName);
	           holder.contactDays = (CustomTextView)row.findViewById(R.id.contactDays);
	           row.setTag(holder);
	       }
	       else
	       {
	           holder = (ContactHolder)row.getTag();
	       }
	       GroupItem contact = values.get(position);
	       try{
	    	   
		    	holder.contactIcon.setBackgroundResource(contact.getIcon());
		        holder.contactName.setText(contact.getName());
           	    holder.contactDays.setText(Integer.toString(contact.getContactCollection().size()));
	       }
	       catch(Exception e)
	       {
	    	   e.printStackTrace();
	       }
	       contactViewMap.put(contact,row);
	       return row;
	   }
	   public Map<GroupItem, View> getViewContactBindItems() {
	       return contactViewMap;
	   }
	   public List<GroupItem> getItems() {
	       return values;
	   }
	   public ArrayList<Integer> getChecks()
	   {
		   return this.checks;
	   }
	   
	   static class ContactHolder
	   {
	   	ImageView contactIcon;
	   	CustomTextView contactName;
	   	CustomTextView contactDays;
	   }

	}