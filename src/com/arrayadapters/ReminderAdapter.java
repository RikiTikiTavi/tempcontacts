package com.arrayadapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.customitems.CustomTextView;
import com.datamodel.ReminderItem;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
/* adapter for reminder items*/
public class ReminderAdapter extends ArrayAdapter<ReminderItem>{
	ArrayList<ReminderItem> reminders;
	private int layoutResourceId;
	private Context context;
	private View view;
	private ContactHolder holder;
	FrequentlyUsedMethods faq;
	public ReminderAdapter(Context context, int textViewResourceId, List<ReminderItem> objects) {
		super(context, textViewResourceId, objects);
	       this.layoutResourceId = textViewResourceId;
	       this.context = context;
	       this.reminders = (ArrayList<ReminderItem>) objects;
	       faq = new FrequentlyUsedMethods(context);
	} 
	@Override
	   public View getView(int position, View convertView, ViewGroup parent) {
	       View row = convertView;
	       view = row;
	       if(row == null)
	       {
	       	   LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	           row = inflater.inflate(R.layout.reminder_item, parent, false);
	           holder = new ContactHolder();
	           holder.contactIcon = (ImageView)row.findViewById(R.id.contactIcon);
	           holder.contactName = (CustomTextView)row.findViewById(R.id.contactName);
	           holder.contactNumber = (CustomTextView)row.findViewById(R.id.contactNumber);
	           holder.contactDays = (CustomTextView)row.findViewById(R.id.contactDays);
	           row.setTag(holder);
	       }
	       else
	       {
	           holder = (ContactHolder)row.getTag();
	       }
	       
	       ReminderItem contact = reminders.get(position);
	       try
	       {
			        holder.contactName.setText(contact.getNumber());
			       	holder.contactNumber.setText(faq.getReminderFormatDate(contact.getDate()));
			        holder.contactIcon.setBackgroundResource(contact.getIcon());
	       }
	       catch(Exception e)
	       {
	       	e.printStackTrace();
	       	
	       }
	       return row;
	   }
	  static class ContactHolder
	   {
	   	ImageView contactIcon;
	   	CustomTextView contactName;
	   	CustomTextView contactNumber;
	   	CustomTextView contactDays;
	   }
}
