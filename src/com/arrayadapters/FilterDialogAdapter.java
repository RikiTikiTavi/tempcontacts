package com.arrayadapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.datamodel.FilterDialogItem;
import com.tempcontacts.R;
/* adapter for setting filter dialogs */
public class FilterDialogAdapter extends ArrayAdapter<FilterDialogItem> {
	private final Context context;
	private final  List<FilterDialogItem> values;

	public FilterDialogAdapter(Context context, List<FilterDialogItem> values) {
	    super(context, R.layout.fiter_dialog_listview, values);
	    this.context = context;
	    this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.filter_dialog_row, parent, false);
	    
	    FilterDialogItem item = values.get(position);
	    
	    final FiltertHolder viewHolder = new FiltertHolder();
	    viewHolder.textView = (TextView) rowView.findViewById(R.id.dialog_text);
	    viewHolder.imageView= (ImageView) rowView.findViewById(R.id.dialog_image);
//	    viewHolder.radioBtn = (RadioButton)  rowView.findViewById(R.id.dialog_radioBtn);
	    rowView.setTag(viewHolder);
	    
	    
//	    viewHolder.radioBtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView,
//					boolean isChecked) {
//				
//				viewHolder.radioBtn.setChecked(true);
//	                for(int i=0;i<values.size();i++){
//	                	Log.i("checkable states", Boolean.toString(values.get(i).isCheck()));
//	                    if(!values.get(i).isCheck()){
//	                    	values.get(i).setCheck(false); // more implement here or may be this work
//	                    }
//	                    else 
//	                    {
//	                    	values.get(i).setCheck(true);
//	                    }
//	                }
//			}
//        });
//	    viewHolder.radioBtn.setChecked(item.isCheck());
	    viewHolder.textView.setText(item.getText());
	    viewHolder.imageView.setBackgroundResource(item.getIcon());


	    return rowView;
	}
	   static class FiltertHolder
	   {
		   
		   TextView textView ;
		    ImageView imageView ;
//		    RadioButton radioBtn;
	   }
}
