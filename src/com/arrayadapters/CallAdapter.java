package com.arrayadapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.customitems.CustomTextView;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.singletones.ContactsViewMapSingletone;
import com.tempcontacts.R;

public class CallAdapter extends ArrayAdapter<CallItem> implements Filterable{
	View view;
	ContactHolder holder = null;
	 int color = 0;
   
   Context context; 
   int layoutResourceId;    
   public static int rowHeight = 0;
	private ViewSwitcher switcher;
	 List<CallItem> contacts = null;
    Map<ContactItem, View> contactViewMap = ContactsViewMapSingletone.getInstance().getOrderViewMap();
	private List<CallItem> filtredContacts;
	private Filter filter;
   public CallAdapter(Context context,  int layoutResourceId,List<CallItem> contacts) {
       super(context, layoutResourceId, contacts);
       this.layoutResourceId = layoutResourceId;
       this.context = context;
       this.contacts = contacts;
       this.filtredContacts = new ArrayList<CallItem>();
       this.filtredContacts.addAll(contacts);
   }
   @Override
   public CallItem getItem(int arg0) {
       return contacts.get(arg0);
   }

   @Override
   public long getItemId(int arg0) {
       return arg0;
   }
   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
       View row = convertView;
       view = row;
       if(row == null)
       {
       	LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           row = inflater.inflate(R.layout.call_item, parent, false);
           holder = new ContactHolder();
           holder.contactIcon = (ImageView)row.findViewById(R.id.contactIcon);
           holder.contactName = (CustomTextView)row.findViewById(R.id.contactName);
           holder.contactNumber = (CustomTextView)row.findViewById(R.id.contactNumber);
           holder.contactDays = (CustomTextView)row.findViewById(R.id.contactDays);
           row.setTag(holder);
       }
       else
       {
           holder = (ContactHolder)row.getTag();
       }
       
       CallItem contact = contacts.get(position);
       try
       {
    	   
//    	   if (contact.getIcon() == 1)
//	    		{
//    		   		(R.drawable.incoming_call);
//    		   		contact.setIcon(R.drawable.incoming_call);
//	    		}
//	    	else if (contact.getIcon() == 2)
//	    		{
//	    			holder.contactIcon.setBackgroundResource(R.drawable.outgoing_call);
//	    			contact.setIcon(R.drawable.outgoing_call);
//	    		}
//	    	else if (contact.getIcon() == 3)
//	    		{
//	    			holder.contactIcon.setBackgroundResource(R.drawable.misst_call);
//	    			contact.setIcon(R.drawable.icon_call);
//	    		}
    	   holder.contactIcon.setBackgroundResource(contact.getIcon());
           if (contact.getCall_contact() != null)
           {
               Log.i("call adapter call contact", contact.toString() + " "+contact.getCall_contact().toString());
                  holder.contactName.setText(contact.getCall_contact().getName());
           }
            else if(contact.getName() != null)
                 holder.contactName.setText(contact.getName());
           else
               holder.contactName.setText(contact.getNumber());


	        if (contact.getDuration()!=null)
	        	holder.contactNumber.setText(contact.getDays() + " " + contact.getDuration());
	        else
	        	holder.contactNumber.setText(contact.getDays());
       }
       catch(Exception e)
       {
    	   		e.printStackTrace();
       }
       
       
       return row;
   }
   @Override
   public Filter getFilter()
   {
       if (filter == null)
           filter = new CallFilter();

       return filter;
   }
   public List<CallItem> getItems() {
       return filtredContacts;
   }
    public void swapItems(List<CallItem > items)
    {
        this.filtredContacts = items;
        notifyDataSetChanged();
    }
    public void setItems(List<CallItem > items) {
        this.filtredContacts= items;
    }

   static class ContactHolder
   {
   	ImageView contactIcon;
   	CustomTextView contactName;
   	CustomTextView contactNumber;
   	CustomTextView contactDays;
   }
   
   @SuppressLint("DefaultLocale")
	private class CallFilter extends Filter
	   {
	           @Override
	           protected FilterResults performFiltering(CharSequence constraint)
	           {   
	        	   
	        	   Log.i(" filtered value", constraint.toString());
	               FilterResults results = new FilterResults();
	               String prefix = constraint.toString().toLowerCase();
	
	               if (prefix == null || prefix.length() == 0)
	               {
	                   results.values = contacts;
	                   results.count = contacts.size();
	               }
	               else
	               {
	                   final ArrayList<CallItem> list =  new ArrayList<CallItem>();
	                   synchronized (this)
	                   {
	                	   list.addAll(contacts);
	                   }
	                   filtredContacts.clear();
	                   int count = list.size();
	                   for (int i=0; i<count; i++)
	                   {
	                       final CallItem pkmn = list.get(i);
	                       if(pkmn.getName().toLowerCase(Locale.getDefault()).contains(constraint))
	                       {
	                    	   Log.i("found pref",pkmn.toString() );
	                    	   filtredContacts.add(pkmn);
	                       }
	                   }
	                   Log.i("prefix",prefix);
	                   for(int i = 0, l = filtredContacts.size(); i < l; i++)
		               {
	                	   Log.i("filtered list",filtredContacts.get(i).toString());
		               }
	                   results.values = filtredContacts;
	                   results.count = filtredContacts.size();
	               }
	               return results;
	           }
	
	           @SuppressWarnings("unchecked")
	           @Override
	           protected void publishResults(CharSequence constraint, FilterResults results) {
	        	   filtredContacts = (ArrayList<CallItem>)results.values;
	               notifyDataSetChanged();
	               clear();
	               for(int i = 0, l = filtredContacts.size(); i < l; i++)
	               {  
	            	   
	            	   add(filtredContacts.get(i));
	            	  
	               }
	               notifyDataSetChanged();
	       }
	
	}

}