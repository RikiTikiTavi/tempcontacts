package com.arrayadapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ViewSwitcher;

import com.customitems.CustomTextView;
import com.datamodel.ArchiveItem;
import com.singletones.ArchiveViewMapSingletone;
import com.tempcontacts.R;

public class ArchiveAdapter extends ArrayAdapter<ArchiveItem>{
	View view;
	ContactHolder holder = null;
	 int color = 0;
	 private ArrayList<Integer> checks = new ArrayList<Integer>();
   Context context; 
   int layoutResourceId;    
   public static int rowHeight = 0;
	private ViewSwitcher switcher;
	List<ArchiveItem> contacts = null;
    Map<ArchiveItem, View> contactViewMap = ArchiveViewMapSingletone.getInstance().getOrderViewMap();
   public ArchiveAdapter(Context context,  int layoutResourceId,List<ArchiveItem> contacts) 
   {
       super(context, layoutResourceId, contacts);
       this.layoutResourceId = layoutResourceId;
       this.context = context;
       this.contacts = contacts;
   }
   @Override
   public ArchiveItem getItem(int arg0) {
   	
       return contacts.get(arg0);
   }

   @Override
   public long getItemId(int arg0) {
       return arg0;
   }
   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
       View row = convertView;
       view = row;
       checks.add(0);
       if(row == null)
       {
       	LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           row = inflater.inflate(R.layout.archive_item, parent, false);
           holder = new ContactHolder();
           holder.contactName = (CustomTextView)row.findViewById(R.id.contactName);
           holder.contactNumber = (CustomTextView)row.findViewById(R.id.contactNumber);
           holder.contactDays = (CustomTextView)row.findViewById(R.id.contactDays);
           holder.checkBox = (CheckBox)row.findViewById(R.id.contactCheckbox);
           row.setTag(holder);
       }
       else
       {
           holder = (ContactHolder)row.getTag();
       }
       
       ArchiveItem contact = contacts.get(position);
       try{
    	   
	        holder.contactName.setText(contact.getName());
	       	holder.contactNumber.setText(contact.getNumber());
	    	holder.contactDays.setText(Integer.toString(contact.getDays()));
	    	holder.checkBox.setChecked(contact.getFlag());
       }
       catch(Exception e)
       {
       	e.printStackTrace();
       	
       }
       contactViewMap.put(contact, row);
       return row;
   }
   public Map<ArchiveItem, View> getViewContactBindItems() {
       return contactViewMap;
   }
   
   static class ContactHolder
   {
   	CustomTextView contactName;
   	CustomTextView contactNumber;
   	CustomTextView contactDays;
   	CheckBox checkBox;

   }
   
   public List<ArchiveItem> getItems() {
       return contacts;
   }
   public ArrayList<Integer> getChecks()
   {
	   return this.checks;
   }
}