package com.arrayadapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.datamodel.DialogItem;
import com.tempcontacts.R;
/* adapter for setting dialogs */
public class DialogAdapter extends ArrayAdapter<DialogItem> {
	private final Context context;
	private final  List<DialogItem> values;

	public DialogAdapter(Context context, List<DialogItem> values) {
	    super(context, R.layout.basic_listview, values);
	    this.context = context;
	    this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.dialog_row, parent, false);
	    DialogItem item = values.get(position);
	    TextView textView = (TextView) rowView.findViewById(R.id.dialog_text);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.dialog_image);
	    textView.setText(item.getText());
	    imageView.setBackgroundResource(item.getIcon());


	    return rowView;
	}
	 
}
