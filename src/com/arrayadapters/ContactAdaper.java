package com.arrayadapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Loader;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.customitems.CustomTextView;
import com.datamodel.ContactItem;
import com.library.FrequentlyUsedMethods;
import com.singletones.ContactsViewMapSingletone;
import com.tempcontacts.R;

public class ContactAdaper  extends ArrayAdapter<ContactItem>  implements Filterable{
	View view;
	ContactHolder holder = null;
	 int color = 0;
   
   Context context; 
   int layoutResourceId;    
   public static int rowHeight = 0;
	private ViewSwitcher switcher;
	List<ContactItem> contacts = null;
	List<ContactItem> filtredContacts = null;
	FrequentlyUsedMethods faq;
	SparseArray<String> labels;
    Map<ContactItem, View> contactViewMap = ContactsViewMapSingletone.getInstance().getOrderViewMap();
	private Filter filter;
   public ContactAdaper(Context context,  int layoutResourceId,List<ContactItem> contacts, SparseArray<String> labels) {
       super(context, layoutResourceId, contacts);
       this.layoutResourceId = layoutResourceId;
       this.context = context;
       this.contacts = new ArrayList<ContactItem>();
       this.contacts.addAll(contacts);
       this.filtredContacts = new ArrayList<ContactItem>();
       this.filtredContacts.addAll(contacts);
       this.labels = labels;
       faq = new FrequentlyUsedMethods(context);
      
   }
   @Override
   public ContactItem getItem(int arg0) {
   	
       return contacts.get(arg0);
   }

   @Override
   public long getItemId(int arg0) {
       return arg0;
   }
   @Override
   public Filter getFilter()
   {
       if (filter == null)
           filter = new ContactFilter();

       return filter;
   }
   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
       View row = convertView;
       view = row;
       if(row == null)
       {
    	   
       	   LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           row = inflater.inflate(R.layout.contact_item, parent, false);
           holder = new ContactHolder();
           holder.contact_label=(TextView)row.findViewById(R.id.contact_label);
           holder.contactIcon = (ImageView)row.findViewById(R.id.contactIcon);
           holder.contactName = (CustomTextView)row.findViewById(R.id.contactName);
           holder.contactNumber = (CustomTextView)row.findViewById(R.id.contactNumber);
           holder.contactDays = (CustomTextView)row.findViewById(R.id.contactDays);
           row.setTag(holder);
           
       }
       else
       {
           holder = (ContactHolder)row.getTag();
       }
       
       ContactItem contact = filtredContacts.get(position);
       try{
	       
    	   if (contact.getContactGroup()!=null)
	        	{
    		   		contact.setIcon(faq.returnContactIcon(contact.getContactGroup().getIcon()));
    		   		holder.contactIcon.setBackgroundResource(faq.returnContactIcon(contact.getContactGroup().getIcon()));
    		   		if (contact.getContactGroup().getDays()!=0)
    		   		{
    		   			contact.setDays(contact.getContactGroup().getDays());
    		   		}
	        	}
	    	   else
	    	    {
	    		   contact.setIcon(R.drawable.user_not_group);
	    		   holder.contactIcon.setBackgroundResource(R.drawable.user_not_group);
	    	    }
	        
		        holder.contactName.setText(contact.getName());
		       	holder.contactNumber.setText(contact.getNumber());
                if (contact.getDays()==1000)
                    holder.contactDays.setText("");
                else
		       	    holder.contactDays.setText(Integer.toString(contact.getDays()));
		       	if (labels.get(position)!=null)
		       	{
		       			holder.contact_label.setVisibility(View.VISIBLE);
		       			holder.contact_label.setText(labels.get(position));
		       	}
		       	else
		       			holder.contact_label.setVisibility(View.GONE);
//    	   }
       }
       catch(Exception e)
       {
       	e.printStackTrace();
       	
       }
       contactViewMap.put(contact,row);
       return row;
   }
   public SparseArray<String> getSparseLabelsArray()
   {
	   return this.labels;
   }
   public List<ContactItem> getItems() {
       return filtredContacts;
   }
   public Map<ContactItem, View> getViewContactBindItems() {
       return contactViewMap;
   }
   static class ContactHolder
   {
	 TextView  contact_label;
   	ImageView contactIcon;
   	CustomTextView contactName;
   	CustomTextView contactNumber;
   	CustomTextView contactDays;
   }
	   @SuppressLint("DefaultLocale")
	private class ContactFilter extends Filter
	   {
	           @Override
	           protected FilterResults performFiltering(CharSequence constraint)
	           {   
	               FilterResults results = new FilterResults();
	               String prefix = constraint.toString().toLowerCase();
	
	               if (prefix == null || prefix.length() == 0)
	               {
	                   results.values = contacts;
	                   results.count = contacts.size();
	               }
	               else
	               {
	                   final ArrayList<ContactItem> list =  new ArrayList<ContactItem>();
	                   synchronized (this)
	                   {
	                	   list.addAll(contacts);
	                   }
	                   filtredContacts.clear();
	                   int count = list.size();
	                   for (int i=0; i<count; i++)
	                   {
	                       final ContactItem pkmn = list.get(i);
	                       if(pkmn.getName().toLowerCase(Locale.getDefault()).contains(constraint))
	                       {
	                    	   Log.i("found pref",pkmn.toString() );
	                    	   filtredContacts.add(pkmn);
	                       }
	                   }

	                   results.values = filtredContacts;
	                   results.count = filtredContacts.size();
	               }
	               return results;
	           }
	
	           @SuppressWarnings("unchecked")
	           @Override
	           protected void publishResults(CharSequence constraint, FilterResults results) {
	        	   filtredContacts = (ArrayList<ContactItem>)results.values;
	               notifyDataSetChanged();
	               clear();
	               for(int i = 0, l = filtredContacts.size(); i < l; i++)
	               {  
	            	   
	            	   add(filtredContacts.get(i));
	            	  
	               }
	               notifyDataSetChanged();
	       }
	
	}
	
}
