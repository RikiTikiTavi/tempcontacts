package com.arrayadapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.tempcontacts.R;

/* adapter for setting existing icons for groups*/
public class  ImageAdapter extends BaseAdapter {
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return mThumbIds[position];
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        Integer object = mThumbIds[position];
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);
        } else {
            imageView = (ImageView) convertView;
        }

        imageView.setImageResource(mThumbIds[position]);
        if (object.intValue() == 1) {
        	imageView.setBackgroundColor(Color.GREEN);
        } else {
        	imageView.setBackgroundColor(Color.TRANSPARENT);
        }
        return imageView;
    }

    // references to our images
    private Integer[] mThumbIds = {
    		 R.drawable.user_blue_group, R.drawable.user_green_group,
    		 R.drawable.user_red_group, R.drawable.user_purple_group,
           	 R.drawable.user_yellow_group,R.drawable.user_dark_blue_group,
           	 R.drawable.user_pink_group,R.drawable.user_gray_group
    };
}
