package com.arrayadapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.customitems.CustomTextView;
import com.datamodel.ContactItem;
import com.singletones.ContactsViewMapSingletone;
import com.tempcontacts.R;
/* adapter for setting contacts to archive*/
public class ContactArchiveAdapter extends ArrayAdapter<ContactItem> {
	View view;
	ContactHolder holder = null;
	 int color = 0;
   private ArrayList<Integer> checks = new ArrayList<Integer>();
   Context context; 
   int layoutResourceId;    
   public static int rowHeight = 0;
	private ViewSwitcher switcher;
	List<ContactItem> contacts = null;
    Map<ContactItem, View> contactViewMap = ContactsViewMapSingletone.getInstance().getOrderViewMap();
   public ContactArchiveAdapter(Context context,  int layoutResourceId,List<ContactItem> contacts) {
       super(context, layoutResourceId, contacts);
       this.layoutResourceId = layoutResourceId;
       this.context = context;
       this.contacts = contacts;
   }
   @Override
   public ContactItem getItem(int arg0) {
   	
       return contacts.get(arg0);
   }
   @Override
   public int getCount () {
	    return contacts.size ();
	}
   @Override
   public long getItemId(int arg0) {
       return arg0;
   }
   @Override
   public View getView(final int position, View convertView, ViewGroup parent) {
       View row = convertView;
       view = row;
       checks.add( 0);
       if(row == null)
       {
       	LayoutInflater inflater = (LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           row = inflater.inflate(R.layout.contact_archive_item, parent, false);
           holder = new ContactHolder();
           holder.contactIcon = (ImageView)row.findViewById(R.id.contactIcon);
           holder.contactName = (CustomTextView)row.findViewById(R.id.contactName);
           holder.contactNumber = (CustomTextView)row.findViewById(R.id.contactNumber);
           holder.contactDays = (CustomTextView)row.findViewById(R.id.contactDays);
           holder.checkBox= (CheckBox)row.findViewById(R.id.contactCheckbox);
           row.setTag(holder);
       }
       else
       {
           holder = (ContactHolder)row.getTag();
       }
       
       ContactItem contact = contacts.get(position);
       try{
    	   
	        holder.contactName.setText(contact.getName());
	       	holder.contactNumber.setText(contact.getNumber());
            if (contact.getDays()==1000)
                holder.contactDays.setText("");
           else
	       	holder.contactDays.setText(Integer.toString(contact.getDays()));
	       	holder.checkBox.setChecked(contact.getFlag());
       }
       catch(Exception e)
       {
       	e.printStackTrace();
       	
       }
       
       
       contactViewMap.put(contact,row);
       return row;
   }
   public Map<ContactItem, View> getViewContactBindItems() {
       return contactViewMap;
   }
   public List<ContactItem> getItems() {
       return contacts;
   }
   public ArrayList<Integer> getChecks()
   {
	  return checks;
   }
   static class ContactHolder
   {
   	ImageView contactIcon;
   	CustomTextView contactName;
   	CustomTextView contactNumber;
   	CustomTextView contactDays;
   	CheckBox checkBox;
   }


}
