package com.fragments;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.arrayadapters.CallAdapter;
import com.customdialogs.CallDialog;
import com.customdialogs.FilterCallDialog;
import com.datamodel.ArchiveItem;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.library.Constants;
import com.library.DatabaseHandler;
import com.library.FrequentlyUsedMethods;
import com.library.IFragmentsListener;
import com.singletones.SharedPrefs;
import com.tempcontacts.ArchiveActivity;
import com.tempcontacts.CalendarViewActivity;
import com.tempcontacts.ContactSaveActivity;
import com.tempcontacts.PreferencesActivity;
import com.tempcontacts.R;

public class CallsFragment extends ListFragment implements AbsListView.OnScrollListener {
	  public static final String TAG = "ContactManager";
        public static boolean filtering = false;
	    /***  USE this CODE   ******/
	    int PICK_CONTACT=1;
	    FrequentlyUsedMethods faq;
	    ArrayList<CallItem> callItems = new ArrayList<CallItem>();
	    static CallAdapter adapter;
	    ListView contancts_list;
	    CallItem call;
	    IFragmentsListener listener;
    private int lastInScreen;

    @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
		 getActivity().getActionBar().setTitle(R.string.calls);
		  View view = inflater.inflate(R.layout.basic_listview, container, false);
		  this.setHasOptionsMenu(true);
		  faq = new FrequentlyUsedMethods(getActivity());
		  return view;
	 }

	 
	 
	  public CallsFragment() {
		super();
		setRetainInstance(true);
		  setHasOptionsMenu(true);
	}


	@Override
	  public void onActivityCreated(Bundle savedInstanceState)
	  {
	    super.onActivityCreated(savedInstanceState);
	    contancts_list = getListView();
	    updateCallList();
          getListView().setOnScrollListener(this);

		  contancts_list.setOnItemClickListener(new OnItemClickListener() {

              @Override
              public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                      long arg3) {
                  call = adapter.getItem(arg2);
                  Intent i = new Intent(getActivity(), CallDialog.class);
                  i.putExtra("call_dialog", call);
                  startActivityForResult(i, 0);

              }
          });
		  
	  } 
	  
	  private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	        	updateUI(intent);   
	        }
	    };

	private boolean menuIsInflated;
	    
	  @Override
		public void onResume() {
			super.onResume();	
			getActivity().registerReceiver(broadcastReceiver, new IntentFilter(Constants.NEW_CALL));
 			setRetainInstance(true);
			setHasOptionsMenu(true);
          filtering = false;
		}
	  @Override
		public void onCreate(Bundle savedInstanceState) {
				super.onCreate(savedInstanceState);
				setRetainInstance(true);
				setHasOptionsMenu(true);
		}
	  
	  @Override
	  public void onAttach(Activity activity) {
			      super.onAttach(activity);
			      setRetainInstance(true);
			      setHasOptionsMenu(true);
			      try {
			        	
			            listener = (IFragmentsListener) activity;
			        } catch (ClassCastException e) {
			            throw new ClassCastException(activity.toString()
			                    + " must implement OnHeadlineSelectedListener");
			        }
	  }
	  
	  @Override
		public void onPause() {
			super.onPause();
			getActivity().unregisterReceiver(broadcastReceiver);
		}	
	  private void updateUI(Intent intent)
	    {
		  contancts_list.setAdapter(adapter);
	    }
	  public void onActivityResult(int requestCode, int resultCode, Intent data) 
		 {
			Log.i("groups frag", Integer.toString(resultCode));
			 if (requestCode==0)
			 { 
				 if (resultCode == R.drawable.quickly_save)
				 {
					 
					 ContactItem autoSave = new ContactItem("TempContacts"+ " "+ (call.getDays()), call.getNumber(),0,7);
					 faq.saveContact(autoSave);
					 getActivity().getActionBar().setSelectedNavigationItem(1);
					 
					 listener.fragmentsEventsListener(1);
				 
				 }
				 else if(resultCode==R.drawable.call)
				 {
					 Intent callIntent = new Intent(Intent.ACTION_CALL);
	 		        callIntent.setData(Uri.parse("tel:"+ call.getNumber()));
	 		        startActivity(callIntent);
				 }
				 else if(resultCode == R.drawable.save)
				 {
					 Intent callIntent = new Intent(getActivity(), ContactSaveActivity.class);
					 callIntent.putExtra(Constants.NEW_CONTACT_NUMBER,call.getNumber() );
	 		        startActivityForResult(callIntent,2);
				 }
				 else if(resultCode == R.drawable.calendar )
				 {
					 ClipboardManager clipboard = (ClipboardManager)getActivity().
					            getSystemService(Context.CLIPBOARD_SERVICE);
					    getActivity().getContentResolver();
					    ClipData clip = ClipData.newPlainText("number",call.getNumber());
					    clipboard.setPrimaryClip(clip);
					 SharedPrefs.getInstance().editSharePrefs().putString(Constants.BUFFER_NUMBER, call.getNumber()).commit();
					 Intent i =  new Intent(getActivity(), CalendarViewActivity.class);
	 				startActivity(i);
				 
				 }
				 else if(resultCode == R.drawable.copy)
				 {
					 
					 SharedPrefs.getInstance().editSharePrefs().putString(Constants.BUFFER_NUMBER, call.getNumber()).commit();
					 ClipboardManager clipboard = (ClipboardManager)getActivity().
					            getSystemService(Context.CLIPBOARD_SERVICE);
					    getActivity().getContentResolver();
					    ClipData clip = ClipData.newPlainText("number",call.getNumber());
					    clipboard.setPrimaryClip(clip);
				 }
				 else if(resultCode ==  R.drawable.archive )
				 {
					 ContactItem l = null;
					 for (ContactItem a : faq.getDisplayContacts(getActivity()))
					 {
						 if (call.getNumber().equalsIgnoreCase(a.getNumber()))
							 l = a;
					 }		
					ArchiveItem toArchive = new ArchiveItem(l.getName(),l.getNumber(), l.getDays());
					if (!(faq.getDisplayArchiveItems(getActivity()).contains(toArchive)))
						{
							faq.saveArchiveItem(toArchive);
						}
					faq.deleteContact(l);
					 getActivity().getActionBar().setSelectedNavigationItem(1);
				 }
				 else if(resultCode ==  R.drawable.sms )
				 {
					 
					 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
	                        + call.getNumber())));
				 
				 }
			 }
			 // call log filtering
			 if (requestCode==1)
			 {
				 filtering = true;
                 // filter by incoming calls
				 if(resultCode ==1)
				 {


                     this.filerCallLog(Constants.FILER_INCOMING);


				 }
                 // filter by outcoming calls
				 else if (resultCode==2)
				 {
                     this.filerCallLog(Constants.FILTER_OUTGOING);
				 }
                 // filter by incoming calls
				 else if(resultCode==3)
				 {
                     this.filerCallLog(Constants.FILTER_MISSED);
				 }
                 //
				 else if(resultCode==4)
				 {
                     this.filerCallLog(Constants.FILTER_ALL);
				 }
			 }
			 if (requestCode==2)
			 {
				 if (resultCode==1)
				  getActivity().getActionBar().setSelectedNavigationItem(1);
			 }
			 
		 }
	  
	  	 @Override
		 public void   onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		     // TODO Add your menu entries here
		    menu.clear();
		  	inflater.inflate(R.menu.call_menu, menu);
	    	 super.onCreateOptionsMenu(menu, inflater);
		     
		 }
		 @Override
		 public boolean onOptionsItemSelected(MenuItem item) {
		     // Handle item selection
		     switch (item.getItemId()) {
		     case R.id.calendar:
		    	 Intent i = new Intent(getActivity(), CalendarViewActivity.class);
		    	 startActivity(i);
		    	 return true;
		     case R.id.archive:
		    	 Intent q = new Intent(getActivity(), ArchiveActivity.class);
		    	 startActivity(q);
		    	 return true;
		     case R.id.preferences:
		    	 Intent s = new Intent(getActivity(), PreferencesActivity.class);
		    	 startActivity(s);
		    	 return true;
		     case R.id.filter:
		    	 Intent z = new Intent(getActivity(), FilterCallDialog.class);
		    	 startActivityForResult(z,1);
		    	 return true;
		     default:
		         return super.onOptionsItemSelected(item);
		     }
		
		 }

		public void updateCallList() {
			adapter = new CallAdapter(getActivity(),
				       R.layout.call_item, faq.getDisplayCalls(getActivity()));
		    contancts_list.setAdapter(adapter);
		}


    public void updateCallListDownload()
    {
        adapter = new CallAdapter(getActivity(),
                R.layout.call_item, faq.getDisplayCalls(getActivity()));
        contancts_list.setAdapter(adapter);
        contancts_list.setSelection(lastInScreen-5);
    }
    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount > 0)
        {
            lastInScreen = firstVisibleItem + visibleItemCount;
            if(lastInScreen == totalItemCount)
            {
                listener.fragmentsEventsListener(7);
            }
        }
    }
    public void filerCallLog(String filter)
    {
        if (filter.equalsIgnoreCase(Constants.FILER_INCOMING))
        {
//            adapter.clear();
//
//            adapter.addAll(faq.getFilteredCalls(getActivity()));
//            Toast.makeText(getActivity(), Integer.toString(adapter.getCount()),Toast.LENGTH_SHORT).show();
            adapter.clear();
            ArrayList<CallItem> filtered = new ArrayList<CallItem>();
            int a=0;
            for (CallItem call : adapter.getItems())
            {
                if (call.getIcon() == R.drawable.incoming_call)
                {
                    adapter.insert(call, a);
                    a++;
                }
                else
                {
                    filtered.add(call);
                }
            }
            adapter.addAll(filtered);
            adapter.notifyDataSetChanged();
        }
        else if(filter.equalsIgnoreCase(Constants.FILTER_OUTGOING))
        {
            adapter.clear();
            ArrayList<CallItem> filtered = new ArrayList<CallItem>();
            int a=0;
            for (CallItem call : adapter.getItems())
            {
                if (call.getIcon() == R.drawable.outgoing_call)
                {
                    adapter.insert(call, a);
                    a++;
                }
                else
                {
                    filtered.add(call);
                }
            }
            adapter.addAll(filtered);
            adapter.notifyDataSetChanged();
        }
        else if(filter.equalsIgnoreCase(Constants.FILTER_MISSED))
        {
            adapter.clear();
            ArrayList<CallItem> filtered = new ArrayList<CallItem>();
            int a=0;
            for (CallItem call : adapter.getItems())
            {
                if (call.getIcon() == R.drawable.misst_call)
                {
                    Log.i("item to filter", call.toString());
                    adapter.insert(call, a);
                    a++;
                }
                else
                {
                    filtered.add(call);
                }
            }
            adapter.addAll(filtered);
            adapter.notifyDataSetChanged();

        }
        else if(filter.equalsIgnoreCase(Constants.FILTER_ALL))
        {
            adapter = new CallAdapter(getActivity(),
                    R.layout.call_item, faq.getDisplayCalls(getActivity()));
        }
        getListView().setAdapter(adapter);
    }

}
