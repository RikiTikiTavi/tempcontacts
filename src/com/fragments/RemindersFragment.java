package com.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.arrayadapters.ReminderAdapter;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.R;
/* fragment for existing reminders setting */
public class RemindersFragment extends ListFragment{
	 private FrequentlyUsedMethods faq;
	private ReminderAdapter adapter;

	@Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
		  View view = inflater.inflate(R.layout.basic_listview, container, false);
		  faq = new FrequentlyUsedMethods(getActivity());
		  return view;
	 }
	 
	@Override
	 public void onActivityCreated(Bundle savedInstanceState) {
		    super.onActivityCreated(savedInstanceState);
		    adapter = new ReminderAdapter(getActivity(),
		    		android.R.layout.simple_list_item_multiple_choice, faq.getDisplayReminders(getActivity()));
		    ListView contancts_list = getListView();
		    contancts_list.setAdapter(adapter);
		    
		  }
	   @Override
 		public void onResume() {
 			super.onResume();	
 		   adapter = new ReminderAdapter(getActivity(),
		    		android.R.layout.simple_list_item_multiple_choice, faq.getDisplayReminders(getActivity()));
		    ListView contancts_list = getListView();
		    contancts_list.setAdapter(adapter);
 		}
}
