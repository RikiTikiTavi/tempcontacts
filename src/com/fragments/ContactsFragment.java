package com.fragments;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.arrayadapters.ContactAdaper;
import com.customdialogs.ContactDialog;
import com.customdialogs.FilterContactDialog;
import com.customdialogs.InformationDialog;
import com.datamodel.ContactItem;
import com.library.Constants;
import com.library.FrequentlyUsedMethods;
import com.library.IFragmentsListener;
import com.singletones.SharedPrefs;
import com.tempcontacts.ArchiveActivity;
import com.tempcontacts.CalendarViewActivity;
import com.tempcontacts.ContactSaveActivity;
import com.tempcontacts.PreferencesActivity;
import com.tempcontacts.R;
/* fragment for existing contacts setting */
public class ContactsFragment extends ListFragment implements IFragmentsListener{
	  public static final String TAG = "ContactManager";

	    FrequentlyUsedMethods faq;
	    private ContactItem contact;
	    ContactAdaper adapter;
	    ListView contancts_list;

		private EditText contactSearch;

		private ImageView new_contact_image;
		
		private SparseArray<String> labels;
		
		 Context thisContext;
		static ContactsFragment contactFrag;

		private boolean menuIsInflated;

		private IFragmentsListener listener;
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
		  View view = inflater.inflate(R.layout.contacts_fragment, container, false);
		  faq = new FrequentlyUsedMethods(getActivity());
		  getActivity().getActionBar().setTitle(R.string.contacts);
		
		  
		  thisContext = getActivity();
		  
		  contactSearch = (EditText)view.findViewById(R.id.contactSearch);
		  
		  new_contact_image = (ImageView)view.findViewById(R.id.new_contact_image);
		  new_contact_image.setOnClickListener(new OnClickListener(){
			 
			  
			@Override
			public void onClick(View v) {
				 Intent callIntent = new Intent(getActivity(), ContactSaveActivity.class);
 		        startActivityForResult(callIntent,2);
			}
			  
		  });
		  contactSearch.addTextChangedListener(new TextWatcher() {
			  
			  public void afterTextChanged(Editable s) {
			  }
			 
			  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			  }
			 
			  public void onTextChanged(CharSequence s, int start, int before, int count) {
			   adapter.getFilter().filter(s.toString());
			  }
			  });
		  return view;
	 }
	 
	 @Override
		public void onCreate(Bundle savedInstanceState) {
			
				super.onCreate(savedInstanceState);
				setRetainInstance(true);
				setHasOptionsMenu(true);
				 contactFrag = this;
		}
	 
	  @Override
	  public void onAttach(Activity activity) {
	      super.onAttach(activity);
	      setRetainInstance(true);
			setHasOptionsMenu(true);
			 try {
		        	
		            listener = (IFragmentsListener) activity;
		        } catch (ClassCastException e) {
		            throw new ClassCastException(activity.toString()
		                    + " must implement OnHeadlineSelectedListener");
		        }
	  }
	  
	  @Override
	  public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    
	    ArrayList<ContactItem> a = new ArrayList<ContactItem>();
	    SparseArray<String> b = new SparseArray<String>();
	    a  = faq.getDisplayContacts(getActivity());
	    b = FrequentlyUsedMethods.adapterLabels(a);
	    adapter = new ContactAdaper(getActivity(),
			       R.layout.contact_item, a ,b );
	  
	    contancts_list = getListView();
	    
	    if (adapter.getCount()!=0)
	    contancts_list.setAdapter(adapter);
	    
	    contancts_list.setTextFilterEnabled(true);
	    
	    int i=0;
	    contancts_list.setOnItemClickListener(new OnItemClickListener()
		  {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				 contact = adapter.getItem(arg2);
				 Intent i = new Intent(getActivity(), ContactDialog.class);
					startActivityForResult(i,0);
			}});
	    
	  }
	  
	  public ContactsFragment() {
		super();
		setRetainInstance(true);
		  setHasOptionsMenu(true);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) 
		 {
             // return from Contact Dialog and events handling
			 if (requestCode==0)
			 {
			     if (resultCode == R.drawable.call)
				 {
			    	 // calling
					 Intent callIntent = new Intent(Intent.ACTION_CALL);
				        callIntent.setData(Uri.parse("tel:"+ contact.getNumber()));
				        startActivity(callIntent);
				 }
				 else if(resultCode==R.drawable.sms)
				 {
					 
					 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
		                        + contact.getNumber())));
				 }
				 else if(resultCode == R.drawable.info)
				 {
	            		Intent a = new Intent(getActivity(), InformationDialog.class);
	            		a.putExtra("contact_info", contact);
	            		startActivity(a);
				 }
				 else if(resultCode == R.drawable.archive )
				 {

					 FragmentTransaction transaction = getFragmentManager().beginTransaction();
					 final ContactsArchiveFragment fragment = new ContactsArchiveFragment();
					    transaction.replace(android.R.id.content, fragment, "Tag B");
					    transaction.addToBackStack(null);
					    transaction.commit();
					
				 }
				 else if(resultCode == R.drawable.calendar )
				 {
					 ClipboardManager clipboard = (ClipboardManager)getActivity().
					            getSystemService(Context.CLIPBOARD_SERVICE);
					    getActivity().getContentResolver();
					    ClipData clip = ClipData.newPlainText("number",contact.getNumber());
					    clipboard.setPrimaryClip(clip);
					 SharedPrefs.getInstance().editSharePrefs().putString(Constants.BUFFER_NUMBER, contact.getNumber()).commit();
					 Intent i =  new Intent(getActivity(), CalendarViewActivity.class);
					startActivity(i);
				 }
				 else if(resultCode ==  R.drawable.edit)
				 {
					 Intent i =  new Intent(getActivity(), ContactSaveActivity.class);
					 i.putExtra("contact_edit", contact);
					 startActivityForResult(i,2);
				 }
			 }
			 else if (requestCode==1)
			 {
				 if (resultCode!=0)
				 {
					 Log.i("contact Frag",	"I'm going to filter it");
					 adapter.clear();
					 ArrayList<ContactItem> filtered = new ArrayList<ContactItem>();
					 ArrayList<ContactItem> remain = new ArrayList<ContactItem>();
						int a=0;
						Log.i("contacts size", Integer.toString(adapter.getItems().size()));
						for (ContactItem call : adapter.getItems())
						{
							if(call.getContactGroup()!=null)
							{
								if (call.getContactGroup().getIcon() == resultCode)
								{	
									filtered.add(a, call);
									a++;
								}
								else 
								{
									remain.add(call);
								}
							}
							else 
								remain.add(call);
						}
						filtered.addAll(remain);
						 SparseArray<String> b = FrequentlyUsedMethods.adapterLabels(filtered);
						  ContactAdaper filteredAdapter = new ContactAdaper(getActivity(),
								       R.layout.contact_item,filtered ,b );
						getListView().setAdapter(filteredAdapter);
					 
				 }
			 }
			 else if (requestCode==2)
			 {
				 if (resultCode == 1)
				 {	
					 listener.fragmentsEventsListener(1);
//					 updateContactList();
				 }
			 }
			 else if (requestCode==3)
			 {
				 if (resultCode == 1)
				 {
					 listener.fragmentsEventsListener(1);
				 }
			 }
			 
		 }


	  @Override
		 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		     // TODO Add your menu entries here
		  	menu.clear();
		     inflater.inflate(R.menu.contact_menu, menu);
		     super.onCreateOptionsMenu(menu, inflater);
		 }
	  
		 @Override
		 public boolean onOptionsItemSelected(MenuItem item) {
		     // Handle item selection
		     switch (item.getItemId()) {
		     case R.id.filter:
		    	 Intent i = new Intent(getActivity(), FilterContactDialog.class);
		    	 startActivityForResult(i,1);
		    	 return true;
		     case R.id.archive:
		    	 Intent q = new Intent(getActivity(), ArchiveActivity.class);
		    	 startActivityForResult(q,3);
		    	 return true;
		     case R.id.preferences:
		    	 Intent s = new Intent(getActivity(), PreferencesActivity.class);
		    	 startActivity(s);
		    	 return true;
		     case R.id.calendar:
		    	 Intent n = new Intent(getActivity(), CalendarViewActivity.class);
		    	 startActivity(n);
		    	 return true;
		     case R.id.contacts_import:
		    	 
		    	 listener.fragmentsEventsListener(5);
		    	 return true;
		     default:
		         return super.onOptionsItemSelected(item);
		     }
		
		 }
		 
		 public static interface TaskCallbacks {
			    public void onPreExecute();
			    public void onCancelled();
			    public void onPostExecute();
			  }
		 public void updateContactList()
		 {
			 ArrayList<ContactItem> a = new ArrayList<ContactItem>();
			    SparseArray<String> b = new SparseArray<String>();
			    a  = faq.getDisplayContacts(getActivity());
			    b = FrequentlyUsedMethods.adapterLabels(a);
			    adapter = new ContactAdaper(getActivity(),
					       R.layout.contact_item, a ,b );
             contactFrag.getListView().setAdapter(adapter);
		 }

		@Override
		public void fragmentsEventsListener(int flag) {
		}
}