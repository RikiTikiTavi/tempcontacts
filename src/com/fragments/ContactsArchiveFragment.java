package com.fragments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.arrayadapters.ContactArchiveAdapter;
import com.datamodel.ArchiveItem;
import com.datamodel.ContactItem;
import com.library.Constants;
import com.library.FrequentlyUsedMethods;
import com.singletones.SharedPrefs;
import com.tempcontacts.R;
public class ContactsArchiveFragment extends ListFragment {
	  public static final String TAG = "ContactManager";

	    private Button mAddAccountButton;
	    private ListView mContactList;
	    private boolean mShowInvisible;
	    private CheckBox mShowInvisibleControl;
	    /***  USE this CODE   ******/
	    int PICK_CONTACT=1;
	    FrequentlyUsedMethods faq;
	    ContactArchiveAdapter adapter;
	    Map<ContactItem, Boolean> checkboxCont;

		private RelativeLayout selectAllArchive;
		
		ArrayList<ContactItem> contactsToArchive = new ArrayList<ContactItem>();
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setHasOptionsMenu(true);
	    }
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
		  View view = inflater.inflate(R.layout.archive_fragment, container, false);
		  faq = new FrequentlyUsedMethods(getActivity());

		  selectAllArchive = (RelativeLayout)view.findViewById(R.id.selectAllArchive);  
		  return view;
	 }
	 @Override
	 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	     // TODO Add your menu entries here
	     super.onCreateOptionsMenu(menu, inflater);
	     menu.clear();
	     inflater.inflate(R.menu.contacts_archive_menu, menu);
	     
	 }
	 
	 
	 
	 @Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	     // Handle item selection
	     switch (item.getItemId()) {
	     case R.id.itemDelete:
	    	 ArrayList<ContactItem> forDelete = new ArrayList<ContactItem>();
	    	 faq.putContactsListToArchive(contactsToArchive);
	    	 adapter.getItems().removeAll(contactsToArchive);
	    	 adapter.notifyDataSetChanged();
	    	 return true;
	     default:
	         return super.onOptionsItemSelected(item);
	     }
	 }
	 
	  @Override
	  public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);

//          getActivity().getActionBar().setTitle(getActivity().getString(R.string.contacts_archive));
	    adapter  = new ContactArchiveAdapter(getActivity(),
	    		android.R.layout.simple_list_item_multiple_choice, faq.getDisplayContacts(getActivity()));
		    getListView().setAdapter(adapter);
		  
		    getListView().setOnItemClickListener(new OnItemClickListener()
		  {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if(adapter.getChecks().get(arg2).intValue()==0)
				{
					adapter.getChecks().set(arg2, 1);
					adapter.getItem(arg2).setFlag(true);
					contactsToArchive.add(adapter.getItem(arg2));
				}
				else 
				{
					adapter.getChecks().set(arg2, 0);
					adapter.getItem(arg2).setFlag(false);
					contactsToArchive.remove(adapter.getItem(arg2));
				}
				adapter.notifyDataSetChanged();
			}});
		    
		    selectAllArchive.setOnClickListener(new OnClickListener() {  
		        public void onClick(View v)
	            {
		        	CheckBox checkbox = (CheckBox)v.findViewById(R.id.selectAllArchive_chb);
		        	if (!checkbox.isChecked())
		        	{
		        		checkbox.setChecked(true);
		               for (ContactItem a : adapter.getItems())
		               {
							a.setFlag(true);
							contactsToArchive.add(a);
							adapter.notifyDataSetChanged();
		               }
		        	}
		        	else 
		        	{
		        		checkbox.setChecked(false);
		        		
		        		 for (ContactItem a : adapter.getItems())
			               {
								a.setFlag(false);
								contactsToArchive.remove(a);
								adapter.notifyDataSetChanged();
			               }
		        	}
	            }
	         });
		    Log.i("adapter size",Integer.toString(adapter.getItems().size()));
	  
	  }
	  @Override
	    public void onPause() {
		  super.onPause();
		  Log.i("contact Archive Layout", "onPause");
		  FragmentManager fragmentManager =  getActivity().getFragmentManager();
	        FragmentTransaction fragmentTransaction = fragmentManager
	                .beginTransaction();
	        ContactsFragment frag =new ContactsFragment();
	        fragmentTransaction.replace(android.R.id.content, frag);
	        fragmentTransaction.addToBackStack(null);
	        fragmentTransaction.commit();
		  
	  }
	  @Override
	    public void onDestroyView() {
	        super.onDestroyView();
	        Log.i("contact Archive Layout", "onDestroyView");
	       
	    }   
	  @Override
		public void onStop() {
			super.onStop();	
			Log.i("contact Archive Layout", "on stop");
		}
	  @Override
		public void onResume() {
			super.onResume();	
			Log.i("contact Archive Layout", "on resume");
		}
	 public boolean allowBackPressed()
	 {
//		 ((CallActivity)getActivity()).
		return true;
		 
	 }
}