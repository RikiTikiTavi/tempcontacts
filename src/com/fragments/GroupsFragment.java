package com.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.arrayadapters.GroupAdapter;
import com.customdialogs.GroupDialog;
import com.customdialogs.InformationDialog;
import com.datamodel.GroupItem;
import com.library.Constants;
import com.library.FrequentlyUsedMethods;
import com.tempcontacts.ArchiveActivity;
import com.tempcontacts.CalendarViewActivity;
import com.tempcontacts.GroupEditing;
import com.tempcontacts.PreferencesActivity;
import com.tempcontacts.R;
/* fragment for existing groups setting */
public class GroupsFragment extends ListFragment {

	 private FrequentlyUsedMethods faq;
	 GroupAdapter adapter;
	 int requestCode = 0;
	 int position;
	 GroupItem choosed_group;
	 @Override
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
	   Bundle savedInstanceState) {
		 getActivity().getActionBar().setTitle(R.string.groups);
		  View view = inflater.inflate(R.layout.groups_fragment, container, false);
		  faq = new FrequentlyUsedMethods(getActivity());
		  this.setHasOptionsMenu(true);
		  RelativeLayout layout = (RelativeLayout)view.findViewById(R.id.new_group_adding);
		  layout.setOnClickListener(new OnClickListener() {  
		        public void onClick(View v)
		            {
		        	
		        	  Intent i = new Intent(getActivity(), GroupEditing.class);
		               startActivityForResult(i,requestCode);
		               
		            }
		         });

		  return view;
	 }
	 
	@Override
	 public void onActivityCreated(Bundle savedInstanceState) {
		    super.onActivityCreated(savedInstanceState);
		    faq = new FrequentlyUsedMethods(getActivity());
		    adapter  = new GroupAdapter(getActivity(),
		    		android.R.layout.simple_list_item_multiple_choice, faq.getDisplayGroups(getActivity()));
		    ListView contancts_list = getListView();
		    contancts_list.setAdapter(adapter);
		    contancts_list.setLongClickable(true);
		    
		    contancts_list.setOnItemLongClickListener(new OnItemLongClickListener() {
		        @Override
		        public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
		                final int arg2, long arg3) {
		        	 Intent i = new Intent(getActivity(), GroupDialog.class);
					 position = arg2;
					 choosed_group = adapter.getItem(arg2);
					 startActivityForResult(i, requestCode);
					return false;

			}
			});
		    contancts_list.setOnItemClickListener(new OnItemClickListener()
			  {
				  
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
						if (faq.getGroupContactsListSize(adapter.getItem(arg2))!=0)
						{
							Intent i = new Intent(getActivity(), InformationDialog.class);
							i.putExtra("group_contact_list", adapter.getItem(arg2));
							startActivityForResult(i, 1);
						}
						else 
							Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.group_no_contacts_toast), Toast.LENGTH_SHORT).show();
					
				}});
		    
		  }
	 @Override
		public void onCreate(Bundle savedInstanceState) {
			
				super.onCreate(savedInstanceState);
				setRetainInstance(true);
				setHasOptionsMenu(true);
		}
	 
	  @Override
	  public void onAttach(Activity activity) {
	      super.onAttach(activity);
	      setRetainInstance(true);
			setHasOptionsMenu(true);
	  }
	  
	 public void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		 Log.i("GroupsFrag resultCode", Integer.toString(resultCode));
		 Log.i("GroupsFrag requestCode", Integer.toString(requestCode));
		if (requestCode == 0 )
		{
			if (resultCode == 1)
			 {
				 Intent i = new Intent(getActivity(), GroupEditing.class);
				 i.putExtra("group_editing", choosed_group);
				 startActivityForResult(i, requestCode);
			 }
			 else if(resultCode==2)
			 {
				 faq.cleanGroup(choosed_group);
			 }
			 else if(resultCode == 3)
			 {
				 faq.deleteGroup(adapter.getItem(position));
				 adapter.remove(adapter.getItem(position));
				 adapter.notifyDataSetChanged();
			 }
			 else if(resultCode == 4 )
			 {
				 adapter   = new GroupAdapter(getActivity(),
					       R.layout.groups_fragment, faq.getDisplayGroups(getActivity()));
				 getListView().setAdapter(adapter);
				 
			 }
			 else if(resultCode == 5 )
			 {
				 adapter = new GroupAdapter(getActivity(),
				    		android.R.layout.simple_list_item_multiple_choice, faq.getDisplayGroups(getActivity()));
				    getListView().setAdapter(adapter);
				 
			 }
		}
		if (requestCode == 1 )
		{
			
			
		}
	 }
	 private BroadcastReceiver newGroupBroadcastReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	        	updateUI(intent);   
	        }
	    };
	  @Override
		public void onResume() {
			
			
				super.onResume();
				adapter.notifyDataSetChanged();
				  getActivity().getApplicationContext().registerReceiver(newGroupBroadcastReceiver, new IntentFilter(Constants.NEW_GROUP));
				adapter = new GroupAdapter(getActivity(),
			    		android.R.layout.simple_list_item_multiple_choice, faq.getDisplayGroups(getActivity()));
			    getListView().setAdapter(adapter);
		}
	  @Override
		public void onPause() {
			super.onPause();
			getActivity().getApplicationContext().unregisterReceiver(newGroupBroadcastReceiver);
		}	
	  private void updateUI(Intent intent)
	    {
	    	adapter.notifyDataSetChanged();
	    }
	  
	  @Override
		 public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		     super.onCreateOptionsMenu(menu, inflater);
		     menu.clear();
		     inflater.inflate(R.menu.group_menu, menu);
		 }
	  
		 @Override
		 public boolean onOptionsItemSelected(MenuItem item) {
		     // Handle item selection
		     switch (item.getItemId()) {
		     case R.id.archive:
		    	 Intent q = new Intent(getActivity(), ArchiveActivity.class);
		    	 startActivity(q);
		    	 return true;
		     case R.id.preferences:
		    	 Intent s = new Intent(getActivity(), PreferencesActivity.class);
		    	 startActivity(s);
		    	 return true;
		     case R.id.calendar:
		    	 Intent n = new Intent(getActivity(), CalendarViewActivity.class);
		    	 startActivity(n);
		    	 return true;
		     default:
		         return super.onOptionsItemSelected(item);
		     }
		
		 }

}
