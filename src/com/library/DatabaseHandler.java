
package com.library;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.datamodel.ArchiveItem;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.datamodel.ReminderItem;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
/**
 */
public class DatabaseHandler extends  OrmLiteSqliteOpenHelper{
	 private static final String TAG = DatabaseHandler.class.getSimpleName();
    // All Static variables
    // Database Version
	 /**
	  */
    private static final int DATABASE_VERSION = 2;
 
    // Database Name
    private static final String DATABASE_NAME = "cashdata";
    private Dao<ContactItem, Integer> simpleContactDao;
    private Dao<CallItem, Integer> simpleCallDao;
    private Dao<GroupItem, Integer> simpleGroupDao;
    private Dao<ReminderItem, Integer> simpleReminderDao;
    private Dao<ArchiveItem, Integer> simpleArchiveDao;
    private static final String TABLE_LOGIN = "login";
    private static final AtomicInteger usageCounter = new AtomicInteger(0);
    private RuntimeExceptionDao<ContactItem, Integer> contactRuntimeDao = null;
    private RuntimeExceptionDao<CallItem, Integer> callRuntimeDao = null;
    private RuntimeExceptionDao<GroupItem, Integer> groupRuntimeDao = null;
    private RuntimeExceptionDao<ReminderItem, Integer> remincerRuntimeDao = null;
    private RuntimeExceptionDao<ArchiveItem, Integer> archiveRuntimeDao = null;
    
    // Categories Table Columns names
    private ConnectionSource connectionSource = null;
    private static DatabaseHandler helper;
	private static DatabaseHandler _helperInstance;
	public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        
    }
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) 
    {
    	 try
         {	
            TableUtils.createTable(connectionSource, ContactItem.class);
            TableUtils.createTable(connectionSource, CallItem.class);
            TableUtils.createTable(connectionSource, GroupItem.class);
            TableUtils.createTable(connectionSource, ReminderItem.class);
            TableUtils.createTable(connectionSource, ArchiveItem.class);
 			Log.i(DatabaseHandler.class.getName(), "created new entries in onCreate: " );
         }
         catch (SQLException e){
             Log.e(TAG, "error creating DB " + DATABASE_NAME);
             throw new RuntimeException(e);
         }
	}

	  @Override
	   public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer)
	  {
	       try
	        {	      
	           TableUtils.dropTable(connectionSource, ContactItem.class, true);
	           TableUtils.dropTable(connectionSource, CallItem.class,true);
	           TableUtils.dropTable(connectionSource, GroupItem.class,true);
	           TableUtils.dropTable(connectionSource, ReminderItem.class,true);
	           TableUtils.dropTable(connectionSource, ArchiveItem.class,true);
	           onCreate(db, connectionSource);
	       	}
	       catch (SQLException e){
	           Log.e(TAG,"error upgrading db "+DATABASE_NAME+"from ver "+oldVer);
	           throw new RuntimeException(e);
	       }
   
	   }
	

	  public Dao<ContactItem, Integer> getContactDao() throws SQLException 
	    {
			if (simpleContactDao == null) {
				simpleContactDao = getDao(ContactItem.class);
			}
			return simpleContactDao;
		}
	  public Dao<CallItem, Integer> getCallDao() throws SQLException 
	    {
			if (simpleCallDao == null) {
				simpleCallDao = getDao(CallItem.class);
			}
			return simpleCallDao;
	    }
	  public Dao<GroupItem, Integer> getGroupDao() throws SQLException 
	    {
			if (simpleGroupDao == null) {
				simpleGroupDao = getDao(GroupItem.class);
			}
			return simpleGroupDao;
	    }
	  
	  public Dao<ReminderItem, Integer> getReminderDao() throws SQLException 
	    {
			if (simpleReminderDao == null) {
				simpleReminderDao = getDao(ReminderItem.class);
			}
			return simpleReminderDao;
	    }
	  
	  public Dao<ArchiveItem, Integer> getArchiveDao() throws SQLException 
	    {
			if (simpleArchiveDao == null) {
				simpleArchiveDao = getDao(ArchiveItem.class);
			}
			return simpleArchiveDao;
	    }
	  

		public RuntimeExceptionDao<ContactItem, Integer> getSimpleContactDao() {
			if (contactRuntimeDao == null) {
				contactRuntimeDao = getRuntimeExceptionDao(ContactItem.class);
			}
			return contactRuntimeDao;
		}
		public RuntimeExceptionDao<CallItem, Integer> getSimpleCallDao() {
			if (callRuntimeDao == null) {
				callRuntimeDao = getRuntimeExceptionDao(CallItem.class);
			}
			return callRuntimeDao;
		}
		public RuntimeExceptionDao<GroupItem, Integer> getSimpleGroupDao() {
			if (groupRuntimeDao == null) {
				groupRuntimeDao = getRuntimeExceptionDao(GroupItem.class);
			}
			return groupRuntimeDao;
		}
		public RuntimeExceptionDao<ReminderItem, Integer> getSimpleReminderDao() {
			if (remincerRuntimeDao == null) {
				remincerRuntimeDao = getRuntimeExceptionDao(ReminderItem.class);
			}
			return remincerRuntimeDao;
		}
		public RuntimeExceptionDao<ArchiveItem, Integer> getSimpleArchiveDao() {
			if (archiveRuntimeDao == null) {
				archiveRuntimeDao = getRuntimeExceptionDao(ArchiveItem.class);
			}
			return archiveRuntimeDao;
		}
		
		
	  public static synchronized DatabaseHandler getHelper(Context context) 
	   {
			if (helper == null) 
			{
				helper = new DatabaseHandler(context);
			}
			usageCounter.incrementAndGet();
			return helper;
		}
	  
	  public void open() throws SQLException {
		  helper.getReadableDatabase();
		  helper.getWritableDatabase();
		  Log.i("opening", "It's might to be open");
		  
	  }
	  
	  public static DatabaseHandler getInstance(Context context)
	    {
		  
	        if(_helperInstance==null)
	            _helperInstance=new DatabaseHandler(context);
	        
	        return _helperInstance;
	    }
	  	
    /**
     * Getting user data from database
     * */
 
    /**
     * Getting user login status
     * return true if rows are there in table
     * */
    public int getRowCount() {
        String countQuery = "SELECT  * FROM " + TABLE_LOGIN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        db.close();
        cursor.close();
        // return row count
        return rowCount;
    }
    @Override
    public ConnectionSource getConnectionSource() {
        if (connectionSource == null) {
            connectionSource = super.getConnectionSource();
        }
        return connectionSource;
    }
    public void resetTables(){
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_LOGIN, null, null);
        db.close();
    }
    @Override
    public void close(){
        super.close();
        
      //  helper.close();
        Log.i("database closing", "All DAO's might be destroyed");
        simpleContactDao = null;
        simpleCallDao = null;
        simpleGroupDao=null;
        simpleReminderDao = null;
        simpleArchiveDao = null;
        
        
        callRuntimeDao = null;
        contactRuntimeDao = null;
        groupRuntimeDao = null;
        remincerRuntimeDao = null;
        archiveRuntimeDao = null;
        
        SQLiteDatabase db = this.getWritableDatabase();
        db.close();
    }
	
 
}