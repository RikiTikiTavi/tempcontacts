package com.library;

public class Constants {

	public final static String NEW_CALL = "new_call";
	public final static String OUTCOME_CALL = "outcome_call";
	public final static String NEW_GROUP = "new_group";
	
	public final static String CONTACT_ID = "contact_id";
	public final static String CONTACT_NAME = "contact_name";
	public final static String CONTACT_NUMBER = "contact_number";
	public final static String CONTACT_ICON = "contact_icon";
	public final static String CONTACT_DAYS = "contact_days";
	public final static String CONTACT_GROUP_LIST= "contact_group_list";
	public final static String CONTACT_GROUP =  "contact_group";
	public final static String CONTACT_CHECKFLAG =  "contact_checkFlag";
	public final static String CONTACT_INFO = "contact_info";
	
	
	public final static String CALL_NAME = "call_name";
	public final static String CALL_NUMBER = "call_number";
	public final static String CALL_ICON = "call_icon";
	public final static String CALL_DATE = "call_date";
	public final static String CALL_DURATION= "call_duration";
	public static final String CALL_CONTACT = "call_contact";
	public static final String CALL_SYSTEM_ID = "call_system_id";
	
	
	public static final String GROUP_ID = "group_id";
	public final static String GROUP_NAME = "group_name";
	public final static String GROUP_COUNT = "group_count";
	public final static String GROUP_ICON = "group_icon";
	public final static String GROUP_CONTACTS_LIST = "group_contacts_list";
	public final static String GROUP_CONTACT = "group_contact";
	public final static String GROUP_DAYS= "group_days";
	
	
	public static final String REMINDER_ID = "reminder_id";
	public final static String REMINDER_NAME = "reminder_name";
	public final static String REMINDER_NUMBER = "reminder_number";
	public final static String REMINDER_DESCRIPTION = "reminder_desc";
	public final static String REMINDER_DATE= "reminder_date";
	public final static String REMINDER_ICON= "reminder_icon";
	public final static String REMINDER_CALENDAR_ICON= "reminder_calendar_icon";
	
	
	public final static String ARCHIVE_CONTACT = "archive_contact";
	public final static String ARCHIVE_NAME = "archive_name";
	public final static String ARCHIVE_NUMBER = "archive_number";
	public final static String ARCHIVE_DAYS = "archive_days";
	
	public final static String CALL_FRAGMENT_DIALOG = "call_dialog";
	public final static String CONTACT_FRAGMENT_DIALOG = "contacts_dialog";
	public final static String GROUP_FRAGMENT_DIALOG = "group_dialog";
	
	public final static String CONTEXT_MENU_CALLS = "context_menu_call";
	public final static String CONTEXT_MENU_CONTACTS = "context_menu_contact";
	public final static String CONTEXT_MENU_ARCHIVE_CONTACTS = "context_menu_ar_contacts";
	
	// intent extras
	public final static String NEW_CONTACT_NUMBER = "new_contact_number";
	
	
	public final static String UPDATE_SERVICE = "com.service.UpdateService";
	
	
	public final static String CURRENT_DATE = "current_date";
	
	//SharedPrefs
	public static final String BUFFER_NUMBER = "buffer_number";
	
	public static final int REMINDER_EDIT_INT = 123;
	
	public static final int TEMP_CONTACTS_ID = 6667;
    public static final String TESTFLIGHT_TOKEN =  "60310aee-173a-4179-9ea9-c026b981ab46";


    // checkPoints
    public static final String CHECKPOINT_CALL_INITIALIZE =  "call_initializing";
    public static final String CHECKPOINT_CONTACTS_IMPORT =  "contact_initializing";
    public static final String CHECKPOINT_CONTACT_ADD =  "contact_adding";
    public static final String CHECKPOINT_CONTACT_DELETE =  "contact_delete";
    public static final String CHECKPOINT_CONTACT_GROUP_ADD =  "contact_to_group_add";

    public static final String FILER_INCOMING =  "incoming";
    public static final String FILTER_OUTGOING =  "outgoing";
    public static final String FILTER_MISSED =  "missed";
    public static final String FILTER_ALL =  "all";
}

