package com.library;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;

import com.datamodel.ArchiveItem;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.datamodel.ReminderItem;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.tempcontacts.R;
/**
 *	 Repository for object saving in ORMLite database 
 */
public class ContentRepository {
	  private Context _context;
	    /**
	     */
	    public ContentRepository(ContentResolver contentResolver, Context context) {
	        this._context=context;
	    }

	    public void saveContacts(List<ContactItem> categories) throws SQLException
	    {
	    	try{
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ContactItem.class);
	        for (ContactItem contact : categories) {
	        	 HelperFactory.GetHelper().getContactDao().createIfNotExists(contact);
	        }
	        dbHelper.close();
	    	}catch(Exception e){e.printStackTrace();}
	    }
	    public void saveContact(ContactItem contact) throws SQLException
	    {
	    	
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ContactItem.class);
	        	 HelperFactory.GetHelper().getContactDao().create(contact);
	        dbHelper.close();
	    }
	    public void deleteContact(ContactItem contact) throws SQLException
	    {
	    	
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ContactItem.class);
	        	 HelperFactory.GetHelper().getContactDao().delete(contact);
	        dbHelper.close();
	    }
	    public void saveCallItem(CallItem contact) throws SQLException
	    {
	    	Log.i(" FAQ saveCallItem meth", contact.toString());
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    		dbHelper.getDao(CallItem.class);
	        	 HelperFactory.GetHelper().getCallDao().createIfNotExists(contact);
	    }

	    
	    public void saveGroup(GroupItem group) throws SQLException
	    {
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(GroupItem.class);
	        	 HelperFactory.GetHelper().getGroupDao().createIfNotExists(group);
	        dbHelper.close();
	    }
	    
	    public void saveArchiveItem(ArchiveItem group) throws SQLException
	    {
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ArchiveItem.class);
	        	 HelperFactory.GetHelper().getArchiveDao().createIfNotExists(group);
	        dbHelper.close();
	    }

	    
	    public void deleteGroup(GroupItem contact) throws SQLException
	    {
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(GroupItem.class);
	        	 HelperFactory.GetHelper().getGroupDao().delete(contact);
	        dbHelper.close();
	    }
	    
	    public void saveReminder(ReminderItem reminder) throws SQLException
	    {
	    	Log.i(" reminder for save", reminder.toString());
	    	try{
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ReminderItem.class);
	        	 HelperFactory.GetHelper().getReminderDao().createIfNotExists(reminder);
	        dbHelper.close();
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }
	    
	    public void deleteReminder(ReminderItem reminder) throws SQLException
	    {
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ReminderItem.class);
	        	 HelperFactory.GetHelper().getReminderDao().delete(reminder);
	        dbHelper.close();
	    }
	    public void deleteArchiveItem(ArchiveItem contact) throws SQLException
	    {
	    	
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ArchiveItem.class);
	        	 HelperFactory.GetHelper().getArchiveDao().delete(contact);
	        dbHelper.close();
	    }
	    
	    public void putContactsToArchive(ArrayList<ContactItem> contacts) throws SQLException
	    {
	    	
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ArchiveItem.class);
	    	for (ContactItem f: contacts){
	    		ArchiveItem toArchive = new ArchiveItem(f.getName(), f.getNumber(),f.getDays());
	        	 HelperFactory.GetHelper().getArchiveDao().createIfNotExists(toArchive);
	        	 HelperFactory.GetHelper().getContactDao().delete(f);
	        	 for (CallItem a :  HelperFactory.GetHelper().getCallDao().queryForAll())
	        	 {
	        		 if (a.getNumber().equalsIgnoreCase(f.getNumber()))
	        		 {
	        			 a.setName(a.getName());
	        			 HelperFactory.GetHelper().getCallDao().update(a);
	        		 }
	        	 }
	        	 }
	        dbHelper.close();
	    }
	    
	    public void deleteArchiveItemList(ArrayList<ArchiveItem> archives) throws SQLException
	    {
	    	
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	dbHelper.getDao(ArchiveItem.class);
	    	for (ArchiveItem item: archives){
	        	 HelperFactory.GetHelper().getArchiveDao().delete(item);
	    	}
	        dbHelper.close();
	    }
	    
	    public void restoreContactsFromArchive(ArrayList<ArchiveItem> archives)
	    {
	    	OrmLiteSqliteOpenHelper dbHelper=DatabaseHandler.getInstance(_context);
	    	try{
	    	dbHelper.getDao(ArchiveItem.class);
		    	for (ArchiveItem item : archives)
		    	{
		    		ContactItem restore = new ContactItem(item.getName(),item.getNumber(),R.drawable.user_not_group,item.getDays());
		    		 HelperFactory.GetHelper().getContactDao().createIfNotExists(restore);
		    		 HelperFactory.GetHelper().getArchiveDao().delete(item);
		    	}
	    	}
	    	catch(Exception e){e.printStackTrace();}
	    }
	    
	    
}
