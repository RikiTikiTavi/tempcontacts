package com.library;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.util.Log;
import android.util.Patterns;
import android.util.SparseArray;
import android.widget.Toast;

import com.arrayadapters.CallAdapter;
import com.datamodel.ArchiveItem;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
import com.datamodel.ContactItem;
import com.datamodel.GroupItem;
import com.datamodel.ReminderItem;
import com.fragments.CallsFragment;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.services.ReminderService;
import com.singletones.IntSingletone;
import com.tempcontacts.R;
import com.testflightapp.lib.TestFlight;

public class FrequentlyUsedMethods {
	private static Object databaseHandler;
	private static  Context context;
	ContentRepository _contentRepo;
	private static ContentRepository _contactRepo;
	private static DatabaseHandler db;
	private static SparseArray<String> labels;
	  ArrayList<ContactItem> importContacts = new ArrayList<ContactItem>();
	public FrequentlyUsedMethods(Context context)
	{
		FrequentlyUsedMethods.context= context;
		_contentRepo = new ContentRepository(context.getContentResolver(), context);
	}
	
	  public void  importContacts()
	    {
            TestFlight.log("FAQ importContacts method");
            TestFlight.passCheckpoint(Constants.CHECKPOINT_CONTACTS_IMPORT);
	    	 ContentResolver cr =  context.getContentResolver();
	         Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
	                 null, null, null, null);
	         _contactRepo=new ContentRepository(context.getContentResolver(),context);
	         if (cur.getCount() > 0) {
	             while (cur.moveToNext()) {
	                   String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
	                   String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
	                   if (Integer.parseInt(cur.getString(
	                         cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
	                      Cursor pCur = cr.query(
	                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
	                                null,
	                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
	                                new String[]{id}, null);
	                      while (pCur.moveToNext()) {
	                          String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	                          ContactItem contact = new ContactItem(name, phoneNo,R.drawable.user_not_group,14);
	                          importContacts.add(contact);
	                      }
	                     pCur.close();
	                 }
	             }
	         }
	    		try
	    		{
	    		   		_contactRepo.saveContacts(importContacts);
	    		   	
	    		}catch(Exception e){e.printStackTrace();}
	    }
	  
	  public Context getContext()
	  {
		  return FrequentlyUsedMethods.context;
	  }
	  public ArrayList<ContactItem> getSavedContacts(Context context)
	  {
			ArrayList<ContactItem> contactsList = null;
	    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			 try {
				Dao<ContactItem,Integer> daoSubject = db.getContactDao();
				contactsList = (ArrayList<ContactItem>) daoSubject.queryForAll();
			 }
			 catch(Exception e)
			 {e.printStackTrace();}
			 return contactsList;
	  }
	   public ArrayList<ContactItem> getDisplayContacts(Context context)
	    {	
	    	ArrayList<ContactItem> contactsList = null;
	    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			 try {
				Dao<ContactItem,Integer> daoSubject = db.getContactDao();
				contactsList = (ArrayList<ContactItem>) daoSubject.queryForAll();
			 }
			 catch(Exception e)
			 {e.printStackTrace();}
			 
			 //sorting list
			 Collections.sort(contactsList, new Comparator<ContactItem>() {
					@Override
					public int compare(ContactItem lhs, ContactItem rhs) {
						return lhs.getName().compareTo(rhs.getName());
					}
				});
			 labels = new SparseArray<String>(contactsList.size());
			 for(int i=0;i<labels.size()-1;	++i)
			 {
				 if (!contactsList.get(i).getName().substring(0, 1).equalsIgnoreCase(contactsList.get(i-1).getName().substring(0, 1)))
				 	labels.put(i, contactsList.get(i).getName().substring(0,1));
				 
			 }	
			return contactsList;
			 
	    }
	   
	   public SparseArray<String> getSparseLabelsArray()
	   {
		   return FrequentlyUsedMethods.labels;
	   }

	   public List<CallItem> getDisplayCalls(Context context)
	    {	
	    	List<CallItem> contactsList = new ArrayList<CallItem>();
	    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			 try {
				Dao<CallItem,Integer> daoSubject = db.getCallDao();
                 QueryBuilder<CallItem, Integer> qb = daoSubject.queryBuilder();
                 qb.groupBy(Constants.CALL_DATE);
                 PreparedQuery<CallItem> pq = qb.prepare();
				contactsList =  daoSubject.query(pq);
			 }
			 catch(Exception e)
			 {e.printStackTrace();}

			 if (contactsList.isEmpty())
			 {
				 contactsList = downloadMoreCalls();
			 }
			 Collections.reverse(contactsList);

			 return contactsList;
	    }
    public List<CallItem> getFilteredCalls(Context context)
    {
        List<CallItem> contactsList = new ArrayList<CallItem>();
        DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
        try {
            Dao<CallItem,Integer> daoSubject = db.getCallDao();
            QueryBuilder<CallItem, Integer> qb = daoSubject.queryBuilder();
            qb.where().eq(Constants.CALL_ICON, R.drawable.incoming_call);
            PreparedQuery<CallItem> pq = qb.prepare();
            contactsList =  daoSubject.query(pq);
        }
        catch(Exception e)
        {e.printStackTrace();}

        if (contactsList.isEmpty())
        {
            contactsList = downloadMoreCalls();
        }
        Collections.reverse(contactsList);
        Toast.makeText(getContext(), Integer.toString(contactsList.size()),Toast.LENGTH_SHORT).show();
        return contactsList;
    }
	   
	   public void clearCallLog()
	   {
		   DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			 try {
				Dao<CallItem,Integer> daoSubject = db.getCallDao();
				DeleteBuilder<CallItem, Integer> deleteBuilder =
						daoSubject.deleteBuilder();
						
				deleteBuilder.clear();
				deleteBuilder.delete();
			 
			 }
			 catch(Exception e)
			 {e.printStackTrace();}

	   }
	   public ArrayList<GroupItem> getDisplayGroups(Context context)
	    {	
	    	ArrayList<GroupItem> contactsList = null;
	    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			 try {
				Dao<GroupItem,Integer> daoSubject = db.getGroupDao();
				contactsList = (ArrayList<GroupItem>) daoSubject.queryForAll();
				if (contactsList.isEmpty())
				{
					_contentRepo.saveGroup(new GroupItem(getContext().getResources().getString(R.string.group_friends), R.drawable.user_yellow_group,0));
					_contentRepo.saveGroup(new GroupItem(getContext().getResources().getString(R.string.group_family), R.drawable.user_green_group,0));
					_contentRepo.saveGroup(new GroupItem(getContext().getResources().getString(R.string.group_acquaintance), R.drawable.user_purple_group,0));
					_contentRepo.saveGroup(new GroupItem(getContext().getResources().getString(R.string.group_job), R.drawable.user_red_group,0));
				}
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			return contactsList;
	    }
	   public ArrayList<ReminderItem> getDisplayReminders(Context context)
	    {	
	    	ArrayList<ReminderItem> contactsList = null;
	    	DatabaseHandler db = new DatabaseHandler(getContext().getApplicationContext());
			 try {
				 db.getWritableDatabase();
				Dao<ReminderItem,Integer> daoSubject = db.getReminderDao();
				contactsList = (ArrayList<ReminderItem>) daoSubject.queryForAll();
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			return contactsList;
			 
	    }
	   
	   public ArrayList<ArchiveItem> getDisplayArchiveItems(Context context)
	    {	
	    	ArrayList<ArchiveItem> contactsList = null;
	    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			 try {
				Dao<ArchiveItem,Integer> daoSubject = db.getArchiveDao();
				contactsList = (ArrayList<ArchiveItem>) daoSubject.queryForAll();
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			 Collections.sort(contactsList, new Comparator<ArchiveItem>() {
					@Override
					public int compare(ArchiveItem lhs, ArchiveItem rhs) {
						return lhs.getName().compareTo(rhs.getName());
					}
				});
			return contactsList;
			 
	    }
	   
	   public static DatabaseHandler getHelper1(Context context) {
			if (databaseHandler == null) {
				databaseHandler = DatabaseHandler.getHelper(context.getApplicationContext());
			}
			return (DatabaseHandler) databaseHandler;
		}
	   public void saveGroup(GroupItem item)
	   {
		   
			 try {
				_contentRepo.saveGroup(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   public void deleteGroup(GroupItem item)
	   {
		   
			 try {
				_contentRepo.deleteGroup(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   public void saveContact(ContactItem item)
	   {
		   DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			Dao<CallItem,Integer> daoSubject;
           Dao<ContactItem,Integer> daoContact;
           try {
				 daoSubject= db.getCallDao();
                 daoContact = db.getContactDao();
				 UpdateBuilder<CallItem, Integer> updateBuilder = daoSubject.updateBuilder();
				 if (item.getContactGroup()!=null)
					 item.setIcon(this.returnContactIcon(item.getContactGroup().getIcon()));
				 _contentRepo.saveContact(item);
				for (CallItem call : this.getDisplayCalls(context))
				{
					if (call.getNumber().equalsIgnoreCase(item.getNumber()))
					  {
						updateBuilder.where().eq(Constants.CALL_NUMBER, call.getNumber());
						    updateBuilder.updateColumnValue(  Constants.CALL_CONTACT, item);
						    updateBuilder.update();
					  }
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public void saveCallItem(CallItem item)
	   {
			 try {
                 Log.i("saveCallItem call", item.toString());
				_contentRepo.saveCallItem(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public ArrayList<CallItem> initializeCallLog( )
	   {
           TestFlight.log("logging FAQ initializeCallLog");
           TestFlight.passCheckpoint(Constants.CHECKPOINT_CALL_INITIALIZE);
		   ArrayList<CallItem> contactsList = new ArrayList<CallItem>();
           DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());

			  String[] strFields = {
					  android.provider.CallLog.Calls._ID,
				        android.provider.CallLog.Calls.NUMBER,
				        android.provider.CallLog.Calls.TYPE,
				        android.provider.CallLog.Calls.CACHED_NAME,
				        android.provider.CallLog.Calls.CACHED_NUMBER_TYPE,
				        android.provider.CallLog.Calls.DATE,
				        android.provider.CallLog.Calls.DURATION
				        };
				String strOrder = "" ;// = android.provider.CallLog.Calls.DATE + " DESC";

           int adapterSize = 0;
           Dao<CallItem,Integer > daoCall = null;
           try {
               daoCall   = db.getCallDao();
               adapterSize = daoCall.queryForAll().size();
               strOrder=    String.format("%s limit "+ Integer.toString(adapterSize)+",20",android.provider.CallLog.Calls.DATE + " DESC");
           } catch (SQLException e) {
               e.printStackTrace();
           }
            Toast.makeText(getContext(),Integer.toString(adapterSize),Toast.LENGTH_SHORT).show();
				Cursor mCallCursor = getContext().getContentResolver().query(
				        android.provider.CallLog.Calls.CONTENT_URI,
				        strFields,
				        null,
				        null,
				        strOrder
						);

           while (mCallCursor != null && mCallCursor.moveToNext()) {
               String id = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls._ID));
						  String name = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
		                  String number = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.NUMBER));
		                  String type = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.TYPE));
		                  String date = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.DATE));
		                  String duration = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.DURATION));
		                  if (name==null)
		                	  name = number;

		                  int icon = 0;
		                if (Integer.parseInt(type) == 1)
		  	    		{
		      		   		icon = (R.drawable.incoming_call);
		  	    		}
		                  else if (Integer.parseInt(type) == 2)
		  	    		{
		  	    			icon = (R.drawable.outgoing_call);
		  	    		}
		                  else if (Integer.parseInt(type) == 3)
		  	    		{
		  	    			icon =(R.drawable.misst_call);
		  	    		}
		                ContactItem founndContact = null;
		                CallItem call;
		                Dao<ContactItem, Integer> daoContact;

						try {
							daoContact = db.getContactDao();
		                QueryBuilder<ContactItem, Integer> queryBuilder =
		                		daoContact.queryBuilder();

		                queryBuilder.where().eq(Constants.CONTACT_NUMBER, number);
		                PreparedQuery<ContactItem> preparedQuery = queryBuilder.prepare();

							founndContact = daoContact.queryForFirst(preparedQuery);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}


						if (founndContact != null)
							call = new CallItem(id, founndContact.getName(),number, icon, this.getCallFormatDate(Long.parseLong(date)), duration);
						else
							call = new CallItem(id, name,number, icon, this.getCallFormatDate(Long.parseLong(date)), duration);

		                 contactsList.add(call );
           }
           mCallCursor.close();


		   Collections.reverse(contactsList);
		   for (CallItem item : contactsList)
		   {
				 try {

					_contentRepo.saveCallItem(item);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		   }
		return contactsList;
	   }

        public ArrayList<CallItem> downloadMoreCalls()
        {
            ArrayList<CallItem> contactsList = new ArrayList<CallItem>();
            DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());

            String[] strFields = {
                    android.provider.CallLog.Calls._ID,
                    android.provider.CallLog.Calls.NUMBER,
                    android.provider.CallLog.Calls.TYPE,
                    android.provider.CallLog.Calls.CACHED_NAME,
                    android.provider.CallLog.Calls.CACHED_NUMBER_TYPE,
                    android.provider.CallLog.Calls.DATE,
                    android.provider.CallLog.Calls.DURATION
            };
            String strOrder = "" ;

            int adapterSize = 0;
            Dao<CallItem,Integer > daoCall = null;
            try {
                daoCall   = db.getCallDao();
                adapterSize = daoCall.queryForAll().size();
                strOrder=    String.format("%s limit "+ Integer.toString(adapterSize)+",20",android.provider.CallLog.Calls.DATE + " DESC");
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Cursor mCallCursor = getContext().getContentResolver().query(
                    android.provider.CallLog.Calls.CONTENT_URI,
                    strFields,
                    null,
                    null,
                    strOrder
            );

            while (mCallCursor != null && mCallCursor.moveToNext()) {
                String id = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls._ID));
                String name = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                String number = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.NUMBER));
                String type = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.TYPE));
                String date = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.DATE));
                String duration = mCallCursor.getString(mCallCursor.getColumnIndex(CallLog.Calls.DURATION));
                if (name==null)
                    name = number;

                int icon = 0;
                if (Integer.parseInt(type) == 1)
                {
                    icon = (R.drawable.incoming_call);
                }
                else if (Integer.parseInt(type) == 2)
                {
                    icon = (R.drawable.outgoing_call);
                }
                else if (Integer.parseInt(type) == 3)
                {
                    icon =(R.drawable.misst_call);
                }
                ContactItem founndContact = null;
                CallItem call;
                Dao<ContactItem, Integer> daoContact;

                try {
                    daoContact = db.getContactDao();
                    QueryBuilder<ContactItem, Integer> queryBuilder =
                            daoContact.queryBuilder();

                    queryBuilder.where().eq(Constants.CONTACT_NUMBER, number);
                    PreparedQuery<ContactItem> preparedQuery = queryBuilder.prepare();

                    founndContact = daoContact.queryForFirst(preparedQuery);
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }


                if (founndContact != null)
                    call = new CallItem( founndContact.getName(),number, icon, this.getCallFormatDate(Long.parseLong(date)), duration);
                else
                    call = new CallItem( name,number, icon, this.getCallFormatDate(Long.parseLong(date)), duration);

                contactsList.add(call );
            }
            mCallCursor.close();
            Collections.reverse(contactsList);
            for (CallItem item : contactsList)
            {
                try {

                    _contentRepo.saveCallItem(item);
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            return contactsList;
        }
	   
	   public void deleteContact(ContactItem item)
	   {
           TestFlight.passCheckpoint(Constants.CHECKPOINT_CONTACT_DELETE);
		   DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			Dao<CallItem,Integer> daoCall;
			 try {
				 daoCall= db.getCallDao();
				 UpdateBuilder<CallItem, Integer> updateBuilder = daoCall.updateBuilder();
                List<CallItem> existingCalls = daoCall.queryForAll();
				for (CallItem call : this.getDisplayCalls(getContext()))
				{
					if (call.getNumber().equals(item.getNumber()))
					  {
						updateBuilder.where().eq("id", call.getId());
                        updateBuilder.updateColumnValue(  Constants.CALL_CONTACT ,null);
						updateBuilder.updateColumnValue(  Constants.CALL_NAME, call.getName());
						updateBuilder.update();
					  }
				}
                 _contentRepo.deleteContact(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public void saveReminder(ReminderItem item)
	   {
		   
			 try {
				_contentRepo.saveReminder(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public void saveArchiveItem(ArchiveItem item)
	   {
		   
			 try {
				_contentRepo.saveArchiveItem(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public void deleteArchiveItem(ArchiveItem item)
	   {
		   
			 try {
				_contentRepo.deleteArchiveItem(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public void deleteReminder(ReminderItem item)
	   {
			 try {
				_contentRepo.deleteReminder(item);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public void updateReminder(ReminderItem item)
	   {
			 try {
				 DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
					Dao<ReminderItem, Integer> daoSubject=db.getDao(ReminderItem.class);
				 
					 try {
						UpdateBuilder<ReminderItem, Integer> updateBuilder = daoSubject.updateBuilder();
						updateBuilder.where().eq("id"	, item.getId());
						updateBuilder.updateColumnValue(  Constants.REMINDER_NUMBER	, item.getNumber());
						updateBuilder.updateColumnValue(  Constants.REMINDER_DESCRIPTION	, item.getInfo());
						updateBuilder.updateColumnValue(  Constants.REMINDER_NAME	, item.getMention_name());
						 updateBuilder.update();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   	   
	   public static ArrayList<CallItem> reverse(List<CallItem> orig)
	    {
	       ArrayList<CallItem> reversed = new ArrayList<CallItem>() ;
	       for ( int i = orig.size() - 1 ; i >=0 ; i-- )
	       {
	    	   CallItem obj = orig.get( i ) ;
	           reversed.add( obj ) ;
	       }
	       return reversed ;
	    }
	   
	   public static ArrayList<ContactItem> reverse2(List<ContactItem> orig)
	    {
	       ArrayList<ContactItem> reversed = new ArrayList<ContactItem>() ;
	       for ( int i = orig.size() - 1 ; i >=0 ; i-- )
	       {
	    	   ContactItem obj = orig.get( i ) ;
	           reversed.add( obj ) ;
	       }
	       return reversed ;
	    }
	   

	   public static SparseArray<String> adapterLabels(ArrayList<ContactItem> a)
		  {
		   if (a.size()!=0){
		  labels = new SparseArray<String>(a.size());
		  labels.append(0, a.get(0).getName().substring(0, 1));
			 for(int i=1;i<a.size()-1;	i++)
			 {
				 if (!a.get(i).getName().substring(0, 1).equalsIgnoreCase(a.get(i-1).getName().substring(0, 1)))
				 	{
					 	labels.append(i, a.get(i).getName().substring(0, 1));
				 	}
			 }	
			 return labels;
		   }
		   else 
			   return null;
		  }
		public static Calendar DateToCalendar(Date date){ 
			  Calendar cal = Calendar.getInstance();
			  cal.clear();
			  cal.setTime(date);
//			  cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)-1);
			  return cal;
			}
		public static Calendar DateToCalendarReminder(Date date){ 
			  Calendar cal = Calendar.getInstance();
			  cal.clear();
			  cal.setTime(date);
			  cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)-1);
			  return cal;
			}
		public String getParsedDateDay(Date date)
		{
			Calendar cal = FrequentlyUsedMethods.DateToCalendar(date);
			String currDate = (Integer.toString(cal.get(Calendar.YEAR))+"/"+Integer.toString(cal.get(Calendar.MONTH))+"/"+Integer.toString(cal.get(Calendar.DAY_OF_MONTH)));
			return currDate;
		}
		
		public String getParsedDateMonth(Date date)
		{
			Calendar cal = FrequentlyUsedMethods.DateToCalendar(date);
			String currDate = (Integer.toString(cal.get(Calendar.YEAR))+"/"+Integer.toString(cal.get(Calendar.MONTH)));
			return currDate;
		}
        // detecting call in existing database (whether it was saved as ContactItem or CallItem )
		public CallItem detectCallName( CallItem call)
		{
            CallItem callName = call;
			ArrayList<ContactItem> contacts = getDisplayContacts(getContext());
            DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
            try {
            Dao<CallItem,Integer> daoCall = db.getCallDao();
            Dao<ContactItem,Integer> daoContact = db.getContactDao();
                QueryBuilder<CallItem, Integer> queryCallBuilder = daoCall.queryBuilder();

                UpdateBuilder<CallItem, Integer> updateCallBuilder = daoCall.updateBuilder();

                QueryBuilder<ContactItem, Integer> queryContactBuilder = daoContact.queryBuilder();

                // quering for matched contact in the database
                queryContactBuilder.where().eq(Constants.CONTACT_NUMBER, call.getNumber());
                ContactItem foundContact = queryContactBuilder.queryForFirst();

                // quering for matched call in the database
                queryCallBuilder.where().eq(Constants.CALL_NUMBER, call.getNumber());
                CallItem foundCall = queryCallBuilder.queryForFirst();

                if (foundCall != null)
                {
                    if (foundCall.getCall_contact()!=null)
                    {
                        callName.setCall_contact(foundCall.getCall_contact());
                        callName.setName(foundCall.getName());
                    }
                    else if(foundCall.getName()!=null)
                        callName.setName(foundCall.getName());
                }
            }

            catch (Exception e){e.printStackTrace();}
			return callName;
			
		}
		public void updateGroupCount(GroupItem group)
		{
		    	
		    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
				Dao<GroupItem,Integer> daoGroup = null;
				
				 try {
					 daoGroup = db.getGroupDao();
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
				UpdateBuilder<GroupItem, Integer> updateBuilder = daoGroup.updateBuilder();
				 try {
					updateBuilder.where().eq("id", group.getId());
					updateBuilder.updateColumnValue(  Constants.GROUP_COUNT, group.getCount()+1);
					 updateBuilder.update();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	    	}
		public void cleanGroup(GroupItem group)
		{
		    	
		    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
				Dao<GroupItem,Integer> daoGroup = null;
				Dao<ContactItem,Integer> daoContact = null;
				
				 try {
					 daoGroup = db.getGroupDao();
					 daoContact= db.getContactDao();
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
				 try {
					 for (ContactItem item : group.getContactCollection())
					 {
						if ( item.getContactGroup().equals(group))
							{
								item.setContactGroup(null);
								item.setIcon(R.drawable.user_not_group);
							}
						daoContact.update(item);
					 }
					 group.getContactCollection().clear();
					 daoGroup.update(group);
					 
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	    	}
		public void updateGroupName(GroupItem group)
		{
		    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
				Dao<GroupItem,Integer> daoGroup = null;
				
				 try {
					 daoGroup = db.getGroupDao();
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
				UpdateBuilder<GroupItem, Integer> updateBuilder = daoGroup.updateBuilder();
				 try {
					updateBuilder.where().eq("id", group.getId());
					updateBuilder.updateColumnValue(  Constants.GROUP_NAME, group.getName());
					 updateBuilder.update();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	    	}
		
		public void updateGroupIcon(GroupItem group)
		{
		    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
				Dao<GroupItem,Integer> daoGroup = null;
				
				 try {
					 daoGroup = db.getGroupDao();
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
				UpdateBuilder<GroupItem, Integer> updateBuilder = daoGroup.updateBuilder();
				 try {
					updateBuilder.where().eq("id", group.getId());
					updateBuilder.updateColumnValue(  Constants.GROUP_ICON, group.getIcon());
					 updateBuilder.update();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
	    	}
		public void updateGroupDays(GroupItem group)
		{
			DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			Dao<GroupItem,Integer> daoGroup = null;
			
			 try {
				 daoGroup = db.getGroupDao();
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			UpdateBuilder<GroupItem, Integer> updateBuilder = daoGroup.updateBuilder();
			 try {
				updateBuilder.where().eq("id", group.getId());
				updateBuilder.updateColumnValue(  Constants.GROUP_DAYS, group.getDays());
				 daoGroup.getEmptyForeignCollection(Constants.GROUP_CONTACTS_LIST);
				 updateBuilder.update();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public void updateGroup(GroupItem group)
		{
			DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			Dao<GroupItem,Integer> daoGroup = null;
			
			 try {
				 daoGroup = db.getGroupDao();
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			UpdateBuilder<GroupItem, Integer> updateBuilder = daoGroup.updateBuilder();
			 try {
				updateBuilder.where().eq("id", group.getId());
				updateBuilder.updateColumnValue(  Constants.GROUP_ICON, group.getIcon());
				updateBuilder.updateColumnValue(  Constants.GROUP_NAME, group.getName());
				updateBuilder.updateColumnValue(  Constants.GROUP_DAYS, group.getDays());
				 updateBuilder.update();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public int getGroupContactsListSize(GroupItem group)
		{
			int count = 0;
			DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			Dao<GroupItem,Integer> daoGroup = null;
			GroupItem newGroup = null;
			 try {
				 daoGroup = db.getGroupDao();
				 daoGroup.updateBuilder();
				 newGroup = daoGroup.queryForId(group.getId());
				 count = newGroup.getContactCollection().size();
			 }
			 catch(Exception e)
			 {
				 e.printStackTrace();
			 }
			 return count;
		}
		public void removeContactFromGroup(GroupItem group, ContactItem contact)
		{
			DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
			Dao<GroupItem,Integer> daoGroup = null;
			Dao<ContactItem,Integer> daoContact = null;
			ContactItem contactFromDb = null;
			 try {
				 daoGroup = db.getGroupDao();
				 daoContact = db.getContactDao();
				 daoGroup.updateBuilder();
				 daoGroup.queryForId(group.getId());
				 contactFromDb = daoContact.queryForId(contact.getId());
				 contactFromDb.setContactGroup(null);
				 contactFromDb.setIcon(R.drawable.user_not_group);
				 daoContact.update(contactFromDb);

			 }
			 catch(Exception e)
			 {
				 Log.i(" saveGroupContacts database problem","fuck it is cause problem");
				 e.printStackTrace();
			 }
		}
		
		public void removeArchiveList(ArrayList<ArchiveItem> archives)
		{
			_contactRepo=new ContentRepository(context.getContentResolver(),context);
			try {
				_contactRepo.deleteArchiveItemList(archives);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// method for Contacts in choosed group saving
		public void saveGroupContacts(GroupItem group, ArrayList<ContactItem> contacts)
		{
            TestFlight.passCheckpoint(Constants.CHECKPOINT_CONTACT_GROUP_ADD);
		    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
				Dao<GroupItem,Integer> daoGroup = null;
				Dao<ContactItem,Integer> daoContact = null;
				GroupItem newGroup;
				 try {
					 daoGroup = db.getGroupDao();
					 daoContact= db.getContactDao();
					 daoGroup.updateBuilder();
					 newGroup = daoGroup.queryForId(group.getId());
					 daoGroup.getEmptyForeignCollection(Constants.GROUP_CONTACTS_LIST);
					 for (ContactItem contact: contacts)
					 {
						 contact.setContactGroup(group);
						 if (newGroup.getDays()!=0)
						 contact.setDays(newGroup.getDays());
						 contact.setIcon(this.returnContactIcon(newGroup.getIcon()));
						 daoContact.update(contact);
						 newGroup.setContact(contact);
						 newGroup.setCount(newGroup.getCount()+1);
						 daoGroup.update(newGroup);
					 }
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
			
	    	}
		
		public void saveNonExistingGroupContacts(GroupItem group, ArrayList<ContactItem> contacts)
		{
		    	DatabaseHandler db = new DatabaseHandler(context.getApplicationContext());
				Dao<GroupItem,Integer> daoGroup = null;
				Dao<ContactItem,Integer> daoContact = null;
				GroupItem newGroup;
				 try {
					 daoGroup = db.getGroupDao();
					 daoContact= db.getContactDao();
					 UpdateBuilder<ContactItem, Integer> contactUpdateBuilder = daoContact.updateBuilder();
					 UpdateBuilder<GroupItem, Integer> groupUpdateBuilder = daoGroup.updateBuilder();
					 daoGroup.createIfNotExists(group);
					 newGroup = daoGroup.queryForId(group.getId());
					 daoGroup.getEmptyForeignCollection(Constants.GROUP_CONTACTS_LIST);
					 for (ContactItem contact: contacts)
					 {
						 contact.setContactGroup(group);
						 contact.setIcon(this.returnContactIcon(newGroup.getIcon()));
						 newGroup.setContact(contact);
						 newGroup.setCount(newGroup.getCount()+1);
						 if (newGroup.getDays()!=0)
							 contact.setDays(newGroup.getDays());
						 contactUpdateBuilder.updateColumnValue(Constants.CONTACT_GROUP,newGroup);
						 groupUpdateBuilder.updateColumnValue(Constants.GROUP_CONTACT,contact);
					 }
					 contactUpdateBuilder.update();
					 groupUpdateBuilder.update();
				 }
				 catch(Exception e)
				 {
					 e.printStackTrace();
				 }
	    	}
		
		
		public void sendNotification( ReminderItem reminder)
		{
			getContext();
			AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		    Calendar calendar =  Calendar.getInstance();
		    calendar.clear();
		    calendar.setTime(reminder.getDate());
		    long when = calendar.getTimeInMillis();   
		    Log.i("date ",	calendar.getTime().toString());

		            Intent intent = new Intent(getContext(), ReminderService.class);
		            intent.putExtra("reminder_notification", getContext().getResources().getString(R.string.remind_notification) + " "+reminder.getNumber());
		            intent.putExtra("reminder_when", when);
		            PendingIntent pendingIntent = PendingIntent.getService(getContext(), 0, intent, 0);
		            alarmManager.set(AlarmManager.RTC_WAKEUP, when, pendingIntent);
			
		}
		public void putContactsListToArchive(ArrayList<ContactItem> list)
		{
			_contactRepo=new ContentRepository(context.getContentResolver(),context);
			try {
				_contactRepo.putContactsToArchive(list);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		public void restoreContactsFromArchive(ArrayList<ArchiveItem> contacts)
		{
			_contactRepo=new ContentRepository(context.getContentResolver(),context);
			_contactRepo.restoreContactsFromArchive(contacts);
		}
		
		
		public String getCurrentDate()
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			String yourDate = dateFormat.format(date);
			   return yourDate;
		}
		public String getCurrentDateMinutes()
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String yourDate = dateFormat.format(date);
			   return yourDate;
		}
		public String getDateMMddYY(Date date)
		{
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String yourDate = dateFormat.format(date);
			   return yourDate;
		}
		public String getCallFormatDate(long milisecs)
		{  
			Date currentDate = new Date(milisecs);
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			return df.format(currentDate);
		}
		
		public String getReminderFormatDate(Date date)
		{  
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			String yourDate = dateFormat.format(date);
			   return yourDate;
			
		}
		
		public boolean numberValidate(String email)
		    {
		    	Pattern pattern = (Patterns.PHONE);
				Matcher matcher = pattern.matcher(email);
					boolean matchFound = matcher.matches();
		    	return matchFound;
		    }
		
		public int returnContactIcon(int id)
		{
			return context.getResources().getIdentifier(context.getResources().getResourceEntryName(id).split("_group")[0] , "drawable", context.getPackageName());
		}

}
