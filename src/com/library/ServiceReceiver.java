package com.library;

import com.singletones.SharedPrefs;
import com.singletones.TelephonyManagerSingletone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;
// receiver for incoming calls
public class ServiceReceiver extends BroadcastReceiver {
	 CallStateListener phoneListener;
    @Override
    public void onReceive(final Context context, Intent intent) {
    if (!CallStateListener.noCallListenerYet)
    {
        if (phoneListener == null) {
        	phoneListener = CallStateListener.getCallStateInstance(context);
    	
            TelephonyManager tmanager=(TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
            tmanager.listen(CallStateListener.getCallStateInstance(context), android.telephony.PhoneStateListener.LISTEN_CALL_STATE);
        	SharedPrefs.getInstance().getSharedPrefs().edit().putBoolean("isTelephoneRunning", false).commit();
        }
    }
        else
            CallStateListener.noCallListenerYet = false;


    }
}
