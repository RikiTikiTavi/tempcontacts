package com.library;


import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.services.AlarmReceiver;
import com.singletones.ArchiveViewMapSingletone;
import com.singletones.CallViewMapSingletone;
import com.singletones.ContactsViewMapSingletone;
import com.singletones.GroupsViewMapSingletone;
import com.singletones.IntSingletone;
import com.singletones.SharedPrefs;
import com.singletones.TelephonyManagerSingletone;
import com.singletones.TypeFaceSingletone;
import com.testflightapp.lib.TestFlight;

/**
 *  
 *
 */
public class ApplicationMemory extends Application{
	static ApplicationMemory instance;
	   @Override
	   public void onCreate() {
	       super.onCreate();
	      
	       instance = this;
	       HelperFactory.SetHelper(getApplicationContext());
	       initSingletons();
	       Log.i("app memory class", "i'm initialized");
           TestFlight.takeOff(this, Constants.TESTFLIGHT_TOKEN);
	       Log.i("curr date time", android.text.format.DateFormat.getDateFormat(getApplicationContext()).toString());
	       setRecurringAlarm(this);
	   }
	   @Override
	   public void onTerminate() {
	       super.onTerminate();
	       HelperFactory.ReleaseHelper();
	   }
	   public static ApplicationMemory getInstance(){
	        return instance;
	    }
	   /**
	    * ����� ������������� Signletone
	    */
	   protected void initSingletons()
	   {
		   
		   
	     TypeFaceSingletone.initInstance();
	     SharedPrefs.initInstance(getApplicationContext());
	     ContactsViewMapSingletone.initInstance();
	     CallViewMapSingletone.initInstance();
	     GroupsViewMapSingletone.initInstance();
	     IntSingletone.initInstance();
	     ArchiveViewMapSingletone.initInstance();
	     CallStateListener.initInstance(getApplicationContext());
	     TelephonyManagerSingletone.initInstance(getApplicationContext());
	   }
	   
	   private void setRecurringAlarm(Context context) {
		    // we know mobiletuts updates at right around 1130 GMT.
		    // let's grab new stuff at around 11:45 GMT, inexactly
		   Intent intent = new Intent(AlarmReceiver.ACTION_ALARM);
				//   intent.setAction(AlarmReceiver.ACTION_ALARM);
				   AlarmManager alarms = (AlarmManager) this
						     .getSystemService(Context.ALARM_SERVICE);
				   final PendingIntent pIntent = PendingIntent.getBroadcast(this,
				     1234567, intent, PendingIntent.FLAG_UPDATE_CURRENT);
				  
				   alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				     System.currentTimeMillis(), 
				     AlarmManager.INTERVAL_HALF_DAY, 
				     pIntent);
				    
		}
}