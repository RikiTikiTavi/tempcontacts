package com.library;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.datamodel.CallItem;
import com.tempcontacts.R;
// receiver for outgoing calls
public class OutgoingReceiver extends BroadcastReceiver {
	Context ctx;
	FrequentlyUsedMethods faq;
    public OutgoingReceiver() {
    	
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        faq = new FrequentlyUsedMethods(context);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	Date date = new Date();
        CallItem call = new CallItem(null,number, R.drawable.outgoing_call, dateFormat.format(date));
        faq.saveCallItem(faq.detectCallName(call));

    }
    public Context getContext()
	  {
		  return this.ctx;
	  }
	  public void setContext(Context ctx)
	  {
		  this.ctx = ctx;
	  }
}
