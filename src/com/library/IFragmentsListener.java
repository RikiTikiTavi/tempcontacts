package com.library;
/** * interface for nested fragments in one tab switching */
public interface IFragmentsListener {
	/** * method for Fragment switching handling
	 * @param flag - parameter for fragment switching*/
	public void fragmentsEventsListener(int flag);
}
