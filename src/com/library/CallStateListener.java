package com.library;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.widget.Toast;

import com.datamodel.CallItem;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.singletones.SharedPrefs;
import com.tempcontacts.R;
/* listener for incoming calls */
public class CallStateListener extends PhoneStateListener {
	private static Object databaseHandler;
	Context ctx;
	private DatabaseHandler db;
	private ContentRepository _contactRepo;
	CallItem lastCall;
	FrequentlyUsedMethods faq;
	private static CallStateListener callStateInstance;
    // variable for one Listener registration controlling
    public static boolean noCallListenerYet;
	public CallStateListener(Context ctx)
	{
		this.ctx = ctx;
		faq = new FrequentlyUsedMethods(ctx);
	}
	@SuppressLint("SimpleDateFormat")
	@Override
	public void onCallStateChanged(int state, String incomingNumber) {
	      switch (state) {
	          case TelephonyManager.CALL_STATE_RINGING:
	        	  if (incomingNumber!=null)
	        	  {
                      noCallListenerYet = true;
			          getContext().getSystemService(Context.TELEPHONY_SERVICE);
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date date = new Date();
					CallItem call;
					if (incomingNumber.equalsIgnoreCase("-1"))
						call = new CallItem(null,getContext().getResources().getString(R.string.unknown_call), R.drawable.incoming_call, dateFormat.format(date));
					else 
					    call = new CallItem(null,incomingNumber, R.drawable.incoming_call, dateFormat.format(date));
					if (!SharedPrefs.getInstance().getSharedPrefs().getBoolean("isTelephoneRunning", false))
			       	{
     					  lastCall = call;
                            faq.saveCallItem(faq.detectCallName(call));
						    SharedPrefs.getInstance().getSharedPrefs().edit().putBoolean("isTelephoneRunning", true).commit();
	        	  	}
	        	  }
	        	  else
	        	  {
                      noCallListenerYet =  false;
	        	  }
	       
	          break;

	      }
	      super.onCallStateChanged(state, incomingNumber);
	  }
	  public Context getContext()
	  {
		  return this.ctx;
	  }
	  public void setContext(Context ctx)
	  {
		  this.ctx = ctx;
	  }
	  
	  public static DatabaseHandler getHelper1(Context context) {
			if (databaseHandler == null) {
				databaseHandler = DatabaseHandler.getHelper(context.getApplicationContext());
			}
			return (DatabaseHandler) databaseHandler;
		}
	  
	  public static CallStateListener getCallStateInstance(Context ctx)
	  {
		  return callStateInstance;
	  }
	  public static void initInstance(Context ctx)
	  {
	    if (callStateInstance == null)
	    {
	    	callStateInstance = new CallStateListener(ctx);
	    }
	  }

}