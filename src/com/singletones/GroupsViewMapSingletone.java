package com.singletones;

import java.util.HashMap;
import java.util.Map;

import android.view.View;

import com.datamodel.GroupItem;
/**
 * Singletone for Group object and View binding
 */
public class GroupsViewMapSingletone {
	 Map<GroupItem, View>  arrayList;

	private static GroupsViewMapSingletone instance;

	private GroupsViewMapSingletone()
	{
	    arrayList = new  HashMap<GroupItem,View>();
	}
	/**
	   */
	public static GroupsViewMapSingletone getInstance(){
		
	    return instance;
	}
	/**
	 */
	 public static void initInstance()
	  {
		 if (instance == null)
		 {
		        instance = new GroupsViewMapSingletone();
		 }
	  }

	public Map<GroupItem, View> getOrderViewMap()
	{
	    return arrayList;
	}
	
	@Override
	public String toString()
	{		
	return getOrderViewMap().toString();	
	}
}
