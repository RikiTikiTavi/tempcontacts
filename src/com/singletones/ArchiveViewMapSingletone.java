package com.singletones;

import java.util.HashMap;
import java.util.Map;

import android.view.View;

import com.datamodel.ArchiveItem;
import com.datamodel.CallItem;
import com.datamodel.ContactItem;
/**
 *  Singletone for Archive object and View binding
 */
public class ArchiveViewMapSingletone {
	 Map<ArchiveItem, View>  arrayList;

	private static ArchiveViewMapSingletone instance;

	private ArchiveViewMapSingletone()
	{
	    arrayList = new  HashMap<ArchiveItem,View>();
	}
	/**
	   */
	public static ArchiveViewMapSingletone getInstance(){
		
	    return instance;
	}
	/**
	 */
	 public static void initInstance()
	  {
		 if (instance == null)
		 {
		        instance = new ArchiveViewMapSingletone();
		 }
	  }

	public Map<ArchiveItem, View> getOrderViewMap()
	{
	    return arrayList;
	}
	
	@Override
	public String toString()
	{		
	return getOrderViewMap().toString();	
	}
}
