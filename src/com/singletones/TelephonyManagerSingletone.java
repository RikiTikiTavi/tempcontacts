package com.singletones;

import java.util.Map;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.view.View;

import com.datamodel.GroupItem;

public class TelephonyManagerSingletone {

	private static TelephonyManagerSingletone instance;
	TelephonyManager telephony ;
	private TelephonyManagerSingletone(Context context)
	{
		 telephony = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
	}
	/**
	   */
	public static TelephonyManagerSingletone getInstance(){
	    return instance;
	}
	/**
	 */
	 public static void initInstance(Context ctx)
	  {
		 if (instance == null)
		 {
		        instance = new TelephonyManagerSingletone(ctx);
		 }
	  }

	public TelephonyManager getTelephonyManager()
	{
	    return telephony;
	}
	
}
