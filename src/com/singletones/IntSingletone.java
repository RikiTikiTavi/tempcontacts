package com.singletones;

import java.util.HashMap;
import java.util.Map;

import android.view.View;

import com.datamodel.GroupItem;
/**
 * Singletone for int counter
 */
public class IntSingletone {
	 int counter;

	private static IntSingletone instance;

	private IntSingletone()
	{
		counter = 0;
	}
	Object object = new Object();
	
	
	public static IntSingletone getInstance(){
	    return instance;
	}
	 public static void initInstance()
	  {
		 if (instance == null)
		 {
		        instance = new IntSingletone();
		 }
	  }

	public String getInt()
	{
	     ++counter;
	     return Integer.toString(counter);
	}
    public String getDownloadInt()
    {
        return Integer.toString(counter += 20);
    }

	
}
