package com.singletones;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.library.Constants;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
/**
 * Singletone for sharedPreferences
 */
public class SharedPrefs  extends Application {
    private static  SharedPreferences settings;
    private static SharedPrefs instance;
    //private final SharedPreferences.Editor editor = settings.edit();
    private static Context mContext;
    
    
	private SharedPrefs()
	{
	}
	/**
	   */
	public static SharedPrefs getInstance(){
	    return instance;
	}
	 /**
	   */
	  public void Initialize(Context ctxt){
	       mContext = ctxt;
	       settings = PreferenceManager.getDefaultSharedPreferences(mContext);
	   }
	  /**
	   */
	  public void writeBoolean(String key, boolean value ){
		     Editor e = settings.edit();
		     e.putBoolean(key, value);
		     e.commit();
		}
	  /**
	   */
	  public void writeString(String key, String value ){
		     Editor e = settings.edit();
		     e.putString(key, value);
		     e.commit();
		}
	  /**
	   */
    public SharedPreferences.Editor editSharePrefs() {
        return settings.edit();
    }
    /**
	   */
    public SharedPreferences getSharedPrefs()
    {
    	return settings;
    }
    public static void initInstance(Context context)
	  {
	    if (instance == null)
	    {
	      // Create the instance
	      instance = new SharedPrefs();
	      mContext = context;
	      settings = PreferenceManager.getDefaultSharedPreferences(mContext);
	    }
	  }
    public static String getCurrentDate()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		String yourDate = dateFormat.format(date);
		   return yourDate;
	}
}
