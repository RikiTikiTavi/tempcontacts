package com.singletones;

import java.util.HashMap;
import java.util.Map;

import android.view.View;

import com.datamodel.CallItem;
/**
 *  Singletone for Call object and View binding
 */
public class CallViewMapSingletone {
	 Map<CallItem, View>  arrayList;

	private static CallViewMapSingletone instance;

	private CallViewMapSingletone()
	{
	    arrayList = new  HashMap<CallItem,View>();
	}
	/**
	   */
	public static CallViewMapSingletone getInstance(){
		
	    return instance;
	}
	/**
	 */
	 public static void initInstance()
	  {
		 if (instance == null)
		 {
		        instance = new CallViewMapSingletone();
		 }
	  }

	public Map<CallItem, View> getOrderViewMap()
	{
	    return arrayList;
	}
	
	@Override
	public String toString()
	{		
	return getOrderViewMap().toString();	
	}
}
