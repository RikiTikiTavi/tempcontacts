package com.singletones;

import java.util.HashMap;
import java.util.Map;

import android.view.View;

import com.datamodel.CallItem;
import com.datamodel.ContactItem;
/**
 *  Singletone for Contact object and View binding
 */
public class ContactsViewMapSingletone {
	 Map<ContactItem, View>  arrayList;

	private static ContactsViewMapSingletone instance;

	private ContactsViewMapSingletone()
	{
	    arrayList = new  HashMap<ContactItem,View>();
	}
	/**
	   */
	public static ContactsViewMapSingletone getInstance(){
		
	    return instance;
	}
	/**
	 */
	 public static void initInstance()
	  {
		 if (instance == null)
		 {
		        instance = new ContactsViewMapSingletone();
		 }
	  }

	public Map<ContactItem, View> getOrderViewMap()
	{
	    return arrayList;
	}
	
	@Override
	public String toString()
	{		
	return getOrderViewMap().toString();	
	}
}
