package com.asynctaskbase;
/** *interface which helps handle AsyncTasks results*/
public interface ITaskLoaderListener {
	void onLoadFinished(Object data);
	void onCancelLoad();
}
